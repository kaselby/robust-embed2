import argparse
import os
from transformers import BertModel, BertTokenizer, BertForMaskedLM, DistilBertModel, DistilBertTokenizer, DistilBertForMaskedLM, WEIGHTS_NAME
from tqdm import tqdm
import torch
import numpy as np
import glob

from bayes_lev_embed import RobustEmbeddings, RobustEmbeddingsNaive, VectorEmbeddings
from entailment.entailment_detection import *
from preprocess import *
from bert_model import BertForDensityEstimation, BertForContinuousMaskedLM
from preprocessing.synthetic_noise import process_sentence

from utils import load_sentences

#w2v_file = "./data/reddit_10m_word_level_processed_3_word2vec_model.w2v"
#vocab_file = "./data/reddit_vocab_symspell.txt"
w2v_file = "data/clean_news_word2vec_model.w2v"
base_vocab_file = "data/clean_news_vocab_symspell.txt"
bert_vocab_file = "data/bert_vocab_symspell.txt"
tinybert_vocab_file = "data/tinybert_vocab_symspell.txt"
distilbert_vocab_file = "data/distilbert_vocab_symspell.txt"

#w2v_file = "preprocessing/GoogleNews-vectors-negative300.bin"
#vocab_file = "preprocessing/google_w2v_vocab.txt"
#cost_file = "data/news_dists_matrix.pt"
#neighbour_file = "data/news_neighbours_matrix.pt"

save_folder = "./models"

tinybert_dir = "data/finetuned-tinybert"
minilm_dir = "data/finetuned-minilm"
distilbert_dir = "data/finetuned-distilbert"
tinybert_noisy_dir = "data/finetuned-tinybert-5"
minilm_noisy_dir = "data/finetuned-minilm-5"
distilbert_noisy_dir = "data/finetuned-distilbert-5"
bert_dir = "data/finetuned-bert"
bert_noisy_dir = "data/finetuned-bert-5"
model_classes={
    'base': (BertTokenizer, BertForContinuousMaskedLM, BertForDensityEstimation.from_bfcmlm, bert_dir, bert_noisy_dir, True)
    #'tiny': (BertTokenizer, BertForContinuousMaskedLM, BertForDensityEstimation.from_bfcmlm, tinybert_dir, tinybert_noisy_dir, True),
    #'mini': (BertTokenizer, BertForContinuousMaskedLM, BertForDensityEstimation.from_bfcmlm, minilm_dir, minilm_noisy_dir, True),
    #'distill': (DistilBertTokenizer, DistilBertForMaskedLM, BertForDensityEstimation.from_dbfcmlm, distilbert_dir, distilbert_noisy_dir, True)
}

noise_levels = [0,0.02,0.05,0.1,0.2]
out_dir = "out"

def eval_model(emb, ss, m, n, k, **kwargs):
    seqs = load_sentences(**kwargs)
    counts = 0
    error_probs = []
    for (i, seq, noisy_seq) in tqdm.tqdm(seqs):
        highlight_orig = list(seq)
        highlight_orig[i] = '{' + highlight_orig[i] + '}'
        #print('\nOriginal Sentence: {}'.format(' '.join(highlight_orig)))
        highlight_noisy = list(noisy_seq)
        highlight_noisy[i] = '{' + highlight_noisy[i] + '}'
        #print('   Noisy Sentence: {}'.format(' '.join(highlight_noisy)))

        with torch.no_grad():
            posterior_samples, (prior_knearest, likelihood_knearest, posterior_knearest), (
                (pr_pr, pr_l, pr_po), (l_pr, l_l, l_po), (po_pr, po_l, po_po)) = emb.analyze_seq(noisy_seq, ss, i, m=m,
                                                                                                 n=n, k=k,
                                                                                                 only_oov=False)

        top1_posteriors = posterior_knearest[:, 0]
        wrong_posterior = False
        errors = 0
        for top_posterior in emb.indices_to_words(top1_posteriors):
            if top_posterior != seq[i]:
                wrong_posterior = True
                errors += 1
        if wrong_posterior:
            counts += 1
            error_probs.append(errors / len(top1_posteriors))
    prob_wrong = counts / len(seqs)
    avg_wrong = sum(error_probs) / len(error_probs) if len(error_probs) > 0 else -1

    return prob_wrong, avg_wrong


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--m', help='m', type=int, default=5)
    parser.add_argument('--n', help='n', type=int, default=20)
    parser.add_argument('--k', help='k', type=int, default=10)
    parser.add_argument('--seed', help='the seed for random generation', type=int, default=-1)
    parser.add_argument('--max_vocab', help='max vocab size', type=int, default=-1)
    parser.add_argument('--N_samples', help='N', type=int, default=200)
    parser.add_argument('--dataset', choices=('mtnt', 'snli'), type=str, default='mtnt')
    parser.add_argument('--variance_file', type=str, default=None)
    parser.add_argument('--prefix', help='output file prefix', type=str, default="")
    parser.add_argument('--n_eval', help='number of evaluation runs', type=int, default=5)
    parser.add_argument('--noisy', help='use noisy finetuning', action="store_true")
    parser.add_argument('--eval_all', help="eval all checkpoints", action="store_true")

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    vocab_file = bert_vocab_file

    Sigma = None
    tau = 0.6
    if args.variance_file is not None:
        saved_param = torch.load(args.variance_file)
        Sigma = saved_param['embed.Sigma'].abs()
        tau = float(saved_param['embed.tau'])

    w2v, ss = load_vocab(vocab_file, w2v_file)
    #cost_matrix = torch.load(cost_file)
    #nn_matrix = torch.load(neighbour_file)

    seed = args.seed
    if seed == -1:
        seed = random.randrange(1000000)
    random.seed(seed)
    torch.manual_seed(seed)
    np.random.seed(seed)
    print('seed used is {}\n'.format(seed))

    k = args.k
    n = args.n
    m = args.m

    results = {}
    for model_name, (
    tokenizer_class, pretrained_class, model_fct, base_model_path, noisy_model_path, load_all) in model_classes.items():
        model_path = base_model_path if not args.noisy else noisy_model_path
        if load_all and args.eval_all:
            checkpoints = list(
                os.path.dirname(c) for c in sorted(glob.glob(model_path + "/**/" + WEIGHTS_NAME, recursive=True))
            )
        else:
            checkpoints = [model_path]
        print("Evaluating model:", model_name)
        result = {}
        for checkpoint in checkpoints:
            suffix = checkpoint.split("-")[-1] if checkpoint.find("checkpoint") != -1 else "final"
            print("Checkpoint: ", suffix)
            result[suffix] = {}
            for noise_prob in noise_levels:
                tokenizer = tokenizer_class.from_pretrained(model_path)
                pretrained_model = pretrained_class.from_pretrained(model_path)
                model = model_fct(pretrained_model)
                emb = RobustEmbeddings.from_bert(model, tokenizer)
                prob_wrong, avg_wrong = 0,0
                for i in range(args.n_eval):
                    prob_wrong_i, avg_wrong_i = eval_model(emb, ss, m, n, k, noise_probability=noise_prob, num_sample=args.N_samples, dataset="snli")
                    print(model_name, "@", noise_prob, "% noise:\tprob wrong:", prob_wrong, "avg wrong:", avg_wrong)
                    prob_wrong += prob_wrong_i
                    avg_wrong += avg_wrong_i
                result[suffix][str(noise_prob * 100)] = {"prob wrong": prob_wrong / args.n_eval, "avg wrong": avg_wrong / args.n_eval}

                del tokenizer, pretrained_model, model, emb
        results[model_name] = result

    prefix = args.prefix + "_" if args.prefix is not "" else ""
    output_eval_file = os.path.join(out_dir, prefix + "eval_results.txt")
    with open(output_eval_file, "w") as writer:
        for model_name, model_results in results.items():
            writer.write(model_name + ":\n")
            for checkpoint, checkpoint_results in model_results.items():
                writer.write("\t" + checkpoint + ":\n")
                for noise, noise_results in checkpoint_results.items():
                    writer.write("\t\t" + str(noise) + "% noise:\n")
                    for key, value in noise_results.items():
                        writer.write("\t\t\t" + key + ":\t" + str(value) + "\n")


