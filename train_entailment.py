import argparse
import os
import time
from transformers import BertModel, BertTokenizer, BertForMaskedLM

from bayes_lev_embed import RobustEmbeddings, RobustEmbeddingsNaive, VectorEmbeddings
from entailment.entailment_detection import *
from preprocess import *
from bert_model import BertForContinuousMaskedLM, BertForDensityEstimation

import torch.optim as optim

#torch.backends.cudnn.deterministic=True

#w2v_file = "./data/reddit_10m_word_level_processed_3_word2vec_model.w2v"
#vocab_file = "./data/reddit_vocab_symspell.txt"
w2v_file = "data/clean_news_word2vec_model.w2v"
base_vocab_file = "data/clean_news_vocab_symspell.txt"
bert_vocab_file = "data/bert_vocab_symspell.txt"
#w2v_file = "preprocessing/GoogleNews-vectors-negative300.bin"
#vocab_file = "preprocessing/google_w2v_vocab.txt"
#cost_file = "data/news_dists_matrix.pt"
#neighbour_file = "data/news_neighbours_matrix.pt"
snli_train = "entailment/snli_1.0/snli_1.0_train.jsonl"
snli_val = "entailment/snli_1.0/snli_1.0_dev.jsonl"
snli_test = "entailment/snli_1.0/snli_1.0_test.jsonl"
snli_test_rove = "entailment/snli_1.0/test.txt"
google_w2v_file = "data/GoogleNews-vectors-negative300.bin.gz"

save_folder = "./models"


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--bs', help='batch size', type=int, default=32)
    parser.add_argument('--hs', help='hidden size', type=int, default=256)
    parser.add_argument('--k', help='k', type=int, default=10)
    parser.add_argument('--model_file', type=str, default=None)
    parser.add_argument('--data_type', choices=('snli'), type=str, default='snli')
    parser.add_argument('--model_type', choices=('w2v', 'robust', 'robust-naive', 'fasttext'), type=str, default='w2v')
    parser.add_argument('--likelihood', choices=('w2v', 'bert'), type=str, default='w2v')
    parser.add_argument('--checkpoint_file', type=str, default=None)
    parser.add_argument('--classifier', choices=('cosine', 'lstm'), type=str, default='cosine')
    parser.add_argument('--noise', help = 'level of synthetic noise to inject', type=float, default=0)
    parser.add_argument('--max_vocab', help='max vocab size', type=int, default=-1)
    parser.add_argument('--iters', help='likelihood context iterations', type=int, default=1)
    parser.add_argument('--epochs', help='fine tuning epochs', type=int, default=1)
    parser.add_argument('--lr', help='learning rate', type=float, default=0.001)
    parser.add_argument('--name', help='file prefix', type=str, default=None)
    parser.add_argument('--max_pairs', help='max training pairs to use', type=int, default=50000)
    parser.add_argument('--log', type=str, default=None)


    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    vocab_file = bert_vocab_file if args.likelihood == 'bert' else base_vocab_file
    w2v, ss = load_vocab(vocab_file, w2v_file)
    #cost_matrix = torch.load(cost_file)
    #nn_matrix = torch.load(neighbour_file)

    if args.data_type == 'snli':
        train_pairs, train_labels = load_snli(snli_train, noise_probability=args.noise, max_pairs=args.max_pairs)
        val_pairs, val_labels = load_snli(snli_val, noise_probability=args.noise, max_pairs=args.max_pairs)
        test_pairs, test_labels = load_snli(snli_test, noise_probability=args.noise, max_pairs=args.max_pairs)
        #test_pairs, test_labels = load_snli_rove(snli_test_rove, noise_probability=args.noise)
    else:
        raise NotImplementedError

    """
    if args.model_type == 'robust':
        emb = RobustEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab)
    elif args.model_type == 'fasttext':
        vocab_path = 'data/wiki.en.vec'
        from torchtext import vocab
        voc = vocab.Vectors(vocab_path, max_vectors = 50000)
        emb = VectorEmbeddings.from_fasttext(voc)
    else:
        emb = VectorEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab)
    """

    if args.likelihood == 'w2v':
        if args.model_type == 'w2v':
            emb = VectorEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab)
        elif args.model_type == 'fasttext':
            vocab_path = 'data/wiki.en.vec'
            from torchtext import vocab
            voc = vocab.Vectors(vocab_path, max_vectors = args.max_vocab if args.max_vocab > 0 else 30000)
            emb = VectorEmbeddings.from_fasttext(voc)
        elif args.model_type == 'robust-naive':
            emb = RobustEmbeddingsNaive.from_w2v(w2v, max_vocab=args.max_vocab)
        else:
            emb = RobustEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab)

    elif args.likelihood == 'bert':
        bert_tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
        if args.model_type == 'w2v':
            bert_model = BertForMaskedLM.from_pretrained('bert-base-uncased')
            emb = VectorEmbeddings.from_bert(bert_model, bert_tokenizer)
        if args.model_type == 'robust-naive':
            bert_model = BertForMaskedLM.from_pretrained('bert-base-uncased')
            emb = RobustEmbeddingsNaive.from_bert(bert_model, bert_tokenizer)
        else:
            bert_saved_model = BertForMaskedLM.from_pretrained('bert-base-uncased')
            bert_model = BertForDensityEstimation.from_bfcmlm(bert_saved_model)
            if args.model_type == 'w2v':
                emb = VectorEmbeddings.from_bert(bert_model, bert_tokenizer)
            else:
                emb = RobustEmbeddings.from_bert(bert_model, bert_tokenizer)

    if args.classifier == 'lstm':
        model, optim = make_lstm_classifier(emb.dim, args.hs, emb, 1)
        outfile = os.path.join(save_folder, args.model_file + ".pt")
        model.load_state_dict(torch.load(outfile))
    else:
        model = CosineClassifier(emb)
    opt = optim.Adam(model.parameters(), args.lr)

    '''
    if args.model_type == 'robust' or args.model_type == 'robust-naive':
        train_batches = batch_seqs(train_pairs, train_labels, args.bs, make_batch, emb.voc, ss, args.k)
        val_batches = batch_seqs(val_pairs, val_labels, args.bs, make_batch, emb.voc, ss, args.k)
        test_batches = batch_seqs(test_pairs, test_labels, args.bs, make_batch, emb.voc, ss, args.k)
    else:
        train_batches = batch_seqs(train_pairs, train_labels, args.bs, make_batch_w2v, emb.voc, exclude_oov = True)
        val_batches = batch_seqs(val_pairs, val_labels, args.bs, make_batch_w2v, emb.voc, exclude_oov=True)
        test_batches = batch_seqs(test_pairs, test_labels, args.bs, make_batch_w2v, emb.voc, exclude_oov=True)
    '''
    MAX_LENGTH=35
    start_pregen = time.time()
    if args.model_type == 'robust' or args.model_type == 'robust-naive':
        train_batches = ClassificationBatchGenerator(train_pairs, train_labels, emb.voc, MAX_LENGTH, ss, args.k, n_inputs=2, robust=True).make_batches(args.bs)
        val_batches = ClassificationBatchGenerator(val_pairs, val_labels, emb.voc, MAX_LENGTH, ss, args.k, n_inputs=2,  robust=True).make_batches(args.bs)
        test_batches = ClassificationBatchGenerator(test_pairs, test_labels, emb.voc, MAX_LENGTH, ss, args.k, n_inputs=2,  robust=True).make_batches(args.bs)
    else:
        train_batches = ClassificationBatchGenerator(train_pairs, train_labels, emb.voc, MAX_LENGTH, n_inputs=2,  robust=False, exclude_oov=True).make_batches(args.bs)
        val_batches = ClassificationBatchGenerator(val_pairs, val_labels, emb.voc, MAX_LENGTH, n_inputs=2,  robust=False, exclude_oov=True).make_batches(args.bs)
        test_batches = ClassificationBatchGenerator(test_pairs, test_labels, emb.voc, MAX_LENGTH, n_inputs=2,  robust=False, exclude_oov=True).make_batches(args.bs)
    finish_pregen = time.time()
    print("Creating batches took %f minutes" % ((finish_pregen - start_pregen) / 60.0))

    initial_vecs=emb.voc.vecs.clone()

    initial_val, initial_metrics = eval_entailment(model, test_batches)

    name = args.name if args.name is not None else args.model_type + "_" + args.likelihood + "_" + str(args.noise)
    outfile = name + ".pt"

    best_val_train, best_metrics_train = train_entailment(model, opt, train_batches, val_batches, args.epochs, print_every=200, eval_every=1, save_checkpoint=outfile, log_file=args.log)

    best_val_test, best_metrics_test = eval_entailment(model, test_batches)
    print("Initial AUC:", initial_metrics)
    print("Train AUC:", best_metrics_train)
    print("Test AUC:", best_metrics_test)

    N = initial_vecs.size(0)
    changed=[]
    EPS=1e-10
    for i in range(N):
        if (initial_vecs[i] - emb.voc.vecs[i]).norm() >= EPS:
            changed.append(i)
    if len(changed) > 0:
        print("Vectors changed:", str(len(changed)))

    torch.save(model.state_dict(), outfile)
