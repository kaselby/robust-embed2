from translation.translate_kira import *
from bayes_lev_embed import *
from preprocess import *
import matplotlib.pyplot as plt
from transformers import BertModel, BertTokenizer, BertForMaskedLM
from bert_model import BertForDensityEstimation

mtnt_file = "translation/MTNT/train/train.en-fr.tsv"
wmt_train = "translation/WMT/training/europarl-v7.fr-en"
wmt_test = "translation/WMT/dev/news-test2008"

#en_vocab_file = "data/clean_news_vocab_symspell.txt"
en_vocab_file = "data/bert_vocab_symspell.txt"
en_w2v_file = "data/clean_news_word2vec_model.w2v"
fr_vocab_file = "data/french_vocab_symspell.txt"
fr_w2v_file = "data/french_word2vec_model.w2v"

checkpoint_file = "translation/saved_model.pt"

en_w2v, en_ss = load_vocab(en_vocab_file, en_w2v_file)
fr_w2v, fr_ss = load_vocab(fr_vocab_file, fr_w2v_file)

pretrained_bert = 'bert-base-uncased'  # 'albert-base-v1'
bert_tokenizer = BertTokenizer.from_pretrained(pretrained_bert)
bert_saved_model = BertForMaskedLM.from_pretrained(pretrained_bert)
bert_model = BertForDensityEstimation.from_bfcmlm(bert_saved_model)

en_emb = RobustEmbeddings.from_bert(bert_model, bert_tokenizer, trainable_tokens=(SOS,EOS))
fr_emb = VectorEmbeddings.from_w2v(fr_w2v, trainable_tokens=(SOS,EOS))

train_pairs = load_mtnt(mtnt_file)

batchgen=TranslationBatchGenerator(train_pairs,en_emb.voc,fr_emb.voc,35,en_ss,10,robust=True,max_data_size=100)
batches=batchgen.make_batches(4)
batch1=batches.__next__()

train_pairs, test_pairs = load_WMT(wmt_train, max_pairs=50000), load_WMT(wmt_test)

bs = 256
hs = 1024
lr = 0.0001
epochs = [0,22]

model, optim = create_model(en_emb, fr_emb, hs, lr)

train_batches = batch_seqs(train_pairs, bs, make_batch_w2v, en_emb.voc.t2i, fr_emb.voc.t2i)
test_batches = batch_seqs(test_pairs, bs, make_batch_w2v, en_emb.voc.t2i, fr_emb.voc.t2i)

print("Training...")
best_val, best_metrics, loss_lst = train_translate(model, optim, train_batches, test_batches, epochs, save_checkpoint=checkpoint_file)
print('lowest loss: ' + str(best_val))
print('best BLEU: ' + str(best_metrics))

print("Ploting...")
plt.plot(loss_lst)
plt.xlabel('batch')
plt.ylabel('loss')
plt.savefig('translation/training_plot.png')
print("done!")


'''
(b1,l1),(b2,l2) = train_batches().__next__()
out = model(b1, l1)
#print(out)
'''
