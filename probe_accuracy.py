import argparse
import os
import tqdm
from transformers import BertModel, BertTokenizer, BertForMaskedLM, BertConfig

from bayes_lev_embed import RobustEmbeddings, RobustEmbeddingsNaive, VectorEmbeddings
from entailment.entailment_detection import *
from preprocess import *
from bert_model import BertForContinuousMaskedLM, BertForDensityEstimation, W2VBertForMaskedLM
from preprocessing.synthetic_noise import process_sentence, noise_generator

#torch.backends.cudnn.deterministic=True

#w2v_file = "./data/reddit_10m_word_level_processed_3_word2vec_model.w2v"
#vocab_file = "./data/reddit_vocab_symspell.txt"
base_w2v_file = "data/clean_news_word2vec_model.w2v"
base_vocab_file = "data/clean_news_vocab_symspell.txt"
bert_vocab_file = "data/bert_vocab_symspell.txt"
tinybert_vocab_file = "data/tinybert_vocab_symspell.txt"
tinybert_dir = "data/TinyBERT_4_312"

noisy_snli_dir = "entailment/noisy_snli"

#w2v_file = "data/GoogleNews-vectors-negative300.bin"
#vocab_file = "preprocessing/google_w2v_vocab.txt"
#cost_file = "data/news_dists_matrix.pt"
#neighbour_file = "data/news_neighbours_matrix.pt"
snli_train = "entailment/snli_1.0/snli_1.0_train.jsonl"
snli_test = "entailment/snli_1.0/snli_1.0_test.jsonl"
snli_test_rove = "entailment/snli_1.0/test.txt"
google_w2v_file = "data/GoogleNews-vectors-negative300.bin.gz"
google_w2v_vocab = "data/google_vocab_symspell.txt"

w2v_bert_file='data/w2v-bert'

b2v_path = "data/b2v_model.bin"

save_folder = "./models"

class RobustNaiveModel(nn.Module):
    def __init__(self, emb, ss, k=20):
        super().__init__()
        self.emb = emb
        self.k=k
        self.ss = ss

    def forward(self, seq, posterior_samples=-1, trivial=False, **kwargs):
        b, l = make_batch([seq], self.emb.voc, self.ss, self.k, **kwargs)
        predictions = self.emb(b, return_indices=True, trivial=trivial)
        return [self.emb.voc.i2t[predictions[0,i].item()] for i in range(predictions.size(1))]

import tabulate
def summarize_seq(seq, emb, ss, pivot, k=10, **kwargs):
    neighbours, (prior, likelihood, posterior) = emb.analyze_seq(seq, ss, k=k, **kwargs)
    candidates = emb.indices_to_words(neighbours[0,pivot,:])
    table = [["Prior"] + prior[0,pivot,:].tolist(),
             ["Likelihood"] + likelihood[0,pivot,:].tolist(),
             ["Posterior"] + posterior[0,pivot,:].tolist()]
    headers = [''] + candidates
    print(tabulate.tabulate(table, headers, tablefmt='rst'))


def probe_accuracy(model, base_seqs, noisy_seqs, **kwargs):
    highlighted={}
    for j in range(len(seqs)):
        seq = base_seqs[j]
        noisy_seq = noisy_seqs[j]
        if len(seq) > 0:
            predictions = model(noisy_seq, **kwargs)
            predictions_trivial = model(noisy_seq, trivial=True, **kwargs)
            for i in range(len(seq)):
                if seq[i] == predictions[i] and seq[i] != predictions_trivial[i]:
                    if j in highlighted:
                        highlighted[j].append(i)
                    else:
                        highlighted[j] = [i]

    return highlighted

noise=0.05
pregen=1
max_pairs=25
tau=0.1
bs=16
k=10

vocab_file = bert_vocab_file
w2v_file = google_w2v_file
kv = True
w2v, ss = load_vocab(vocab_file, w2v_file, keyedvectors=kv)

test_pairs, test_labels = load_snli(snli_test, noise_probability=0, max_pairs=max_pairs)
#datasets.append((test_pairs,test_labels))

pretrained_bert = 'bert-base-uncased'
bert_tokenizer = BertTokenizer.from_pretrained(pretrained_bert)
bert_model = BertForMaskedLM.from_pretrained(pretrained_bert)
emb = RobustEmbeddingsNaive.from_w2v_bert(bert_model, bert_tokenizer, w2v, tau=tau)
model = RobustNaiveModel(emb, ss)

voc = emb.voc.t2i
seqs1, seqs2 = zip(*test_pairs)
all_seqs = seqs1 + seqs2
seqs = [[word for word in seq if word in voc] for seq in all_seqs]
noisy_seqs = []
for seq in seqs:
    noisy_seqs.append(process_sentence(seq, noise))

highlighted = probe_accuracy(model, seqs, noisy_seqs)