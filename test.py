import torch
from torch.distributions import Categorical

from bayes_lev_embed import RobustEmbeddings
from paraphrase.paraphrase_detection import *
from preprocess import *
from preprocess import _tensorize


w2v_file = "./data/clean_news_word2vec_model.w2v"
vocab_file = "./data/clean_news_vocab_symspell.txt"

cost_file = "data/news_dists_matrix.pt"
neighbour_file = "data/news_neighbours_matrix.pt"

msr_train = "paraphrase/MSRParaphraseCorpus/msr_paraphrase_train.txt"
msr_test = "paraphrase/MSRParaphraseCorpus/msr_paraphrase_test.txt"
twitter_train = "paraphrase/SemEval-PIT2015/train.data"
twitter_test = "paraphrase/SemEval-PIT2015/test.data"

w2v,ss = load_vocab(vocab_file, w2v_file)
emb1 = RobustEmbeddings.from_w2v(w2v, tau=1, reserved=None)
emb2 = RobustEmbeddings.from_w2v(w2v, tau=1)

batchlist = ["i hink we should go .", "what id you say ?", "i love ats ."]
batchlist = [s.split(" ") for s in batchlist]

b1,l1 = make_batch(batchlist, emb1.voc.t2i, ss, 10, only_oov=False)
b2,l2 = make_batch(batchlist, emb2.voc.t2i, ss, 10, only_oov=False)

test_pairs, test_labels = load_twitter(twitter_test, train=False)

#(oov_pairs, oov_labels), (iv_pairs, iv_labels) = split_oov_pairs(test_pairs, test_labels, emb.voc.t2i)

'''
msr_file = "MSRParaphraseCorpus/msr_paraphrase_train.txt"
pairs, targets = load_msr(msr_file)
batches = batch_seqs(pairs, targets, 4, make_batch, emb.t2i, ss, 10)
(b1,b2),t = batches.__next__()
#n,c,m = b1
#out = model.inference(n,c,mask=m)


batchlist = ["i hink we showld go .", "what ?"]
batchlist = [s.split(" ") for s in batchlist]
neighbours, costs, lengths = make_batch(batchlist, model, ss, 10)
mask = mask_lengths(lengths)
tau = 0.6
normed = F.softmax(-1/tau*costs,dim=-1)
'''


def eval_likelihood(emb, center, context):
    l = len(context)
    contexts = torch.LongTensor([emb.voc.t2i[x] for x in context if x in emb.voc.t2i])
    C = emb.voc.C[contexts]
    w = emb.voc.W[emb.voc.t2i[center]]
    logits = C.matmul(w).sum()
    norm = emb._logcontext(w)
    return logits - norm * l

def maximum_likelihood(emb, context, max_w=5000, k=1):
    with torch.no_grad():
        contexts = torch.LongTensor([emb.voc.t2i[x] for x in context if x in emb.voc.t2i])
        C = emb.voc.C[contexts]
        W = emb.voc.W[:max_w]
        l = contexts.size(0)
        d = emb.dim
        V = max_w if max_w > 0 else len(emb.voc.t2i.keys())
        # V x l x 1 x 1
        logits = C.view(1,l,1,d).expand(V,l,1,d).matmul(W.view(V,1,d,1).expand(V,l,d,1))
        norm = emb._logcontext(W)
        loglik = logits.view(V,l).sum(dim=1) - norm * l
        v,i = loglik.topk(k)
    return [emb.voc.i2t[x.item()] for x in i]


def sample_prior(emb, neighbours, costs, m, n):
    categorical = Categorical(costs.contiguous())
    components = categorical.sample([m,n])
    neighbours_expanded = neighbours.view(1,1,-1).expand(m,n,-1)
    indices = torch.gather(neighbours_expanded, -1, components.unsqueeze(-1)).squeeze(-1)
    means = emb.voc.W[indices, :]
    xi = torch.randn(m,n, emb.dim)
    if use_cuda:
        xi = xi.cuda()
    w = means + xi * emb.Sigma
    return w

def infer_seq(emb, ss, center, context, m, k=5, n=20, tau=1):
    neighbours, dists = _tensorize(center, emb.voc.t2i, ss, k)
    costs = normalize_costs(dists)
    w = sample_prior(emb, neighbours, costs, m, n)
    contexts = torch.LongTensor([emb.voc.t2i[x] for x in context if x in emb.voc.t2i])
    l = contexts.size(-1)
    C = emb.voc.C[contexts]
    wC = w.view(m,n,1,1,emb.dim).matmul(C.view(1,1,-1,emb.dim,1)).squeeze(-1).squeeze(-1)
    logits = wC.sum(dim=-1)
    norm = emb._logcontext(w)
    ll = logits - norm * l
    cat = Categorical(logits=ll / tau)
    indices = cat.sample()
    out = torch.gather(w, 1, indices.view(m, 1, 1).expand(m,1,emb.dim)).squeeze(1)
    I = emb.topk_nn(out, k=k).transpose(0,1)
    tokens = [[emb.voc.i2t[x] for x in I[j,:].tolist()] for j in range(I.size(0))]
    return tokens#, out

def indices_to_words(emb, I):
    if len(I.size()) > 1:
        return [indices_to_words(emb, I[j]) for j in range(I.size(0))]
    else:
        return [emb.voc.i2t[x.item()] for x in I]

def get_nearest(emb, e, k=5):
    I = emb.topk_nn(e, k)
    return indices_to_words(emb, I.t())


#out,p = model.inference(neighbours, normed, n=20, m=10, tau=0.5)

def cosine_dist(a, b):
    if len(b.size()) == 1:
        return a.matmul(b) / a.norm(dim=-1) / b.norm()
    elif len(b.size()) == 2:
        return a.matmul(b.t())/a.norm(dim=-1).unsqueeze(1)/b.norm(dim=-1).unsqueeze(0)
    else:
        raise NotImplementedError

def remove_index(seq, i, dim):
    N = seq.size(dim)
    indices = torch.cat((torch.arange(i), torch.arange(i+1, N)))
    return seq.index_select(dim, indices)

def test_w(model, seq, i=0):
    w = model.W[seq[i],:]
    C = model.C[remove_index(seq, i, -1),:]

    return posterior(model, w, C)


def posterior(model, w, C, wmax=1000):
    l = C.size(0)
    w_trunc = model.W[:wmax,:]

    prior = -1./2 * ((w_trunc - w)**2 / model.Sigma).sum(-1)        # N x 1

    norm = w_trunc.matmul(model.C.t()).logsumexp(dim=-1)            # N
    WC = w_trunc.matmul(C.t())                                      # N x l
    ll = -l * norm + WC.sum(-1)

    return prior + ll


def get_topk(outputs, i, j):
    vecs = outputs[i,j,:]
    dists = cosine_dist(emb.voc.W, vecs)
    return dists.topk(5, dim=0)
