import argparse
import os

from bayes_lev_embed import RobustEmbeddings, VectorEmbeddings, RobustEmbeddingsNaive
from paraphrase.paraphrase_detection import *
from preprocess import *

from transformers import BertModel, BertTokenizer, BertForMaskedLM, BertConfig
from bert_model import BertForContinuousMaskedLM, BertForDensityEstimation, W2VBertForMaskedLM

#torch.backends.cudnn.deterministic=True

#w2v_file = "./data/reddit_10m_word_level_processed_3_word2vec_model.w2v"
#vocab_file = "./data/reddit_vocab_symspell.txt"
base_w2v_file = "data/clean_news_word2vec_model.w2v"
base_vocab_file = "data/clean_news_vocab_symspell.txt"
bert_vocab_file = "data/bert_vocab_symspell.txt"
#w2v_file = "preprocessing/GoogleNews-vectors-negative300.bin"
#vocab_file = "preprocessing/google_w2v_vocab.txt"
cost_file = "data/news_dists_matrix.pt"
neighbour_file = "data/news_neighbours_matrix.pt"
msr_train = "paraphrase/MSRParaphraseCorpus/msr_paraphrase_train.txt"
msr_test = "paraphrase/MSRParaphraseCorpus/msr_paraphrase_test.txt"
twitter_train = "paraphrase/SemEval-PIT2015/train.data"
twitter_test = "paraphrase/SemEval-PIT2015/test.data"

save_folder = "./models"

google_w2v_file = "data/GoogleNews-vectors-negative300.bin.gz"
google_w2v_vocab = "data/google_vocab_symspell.txt"

w2v_bert_file='data/w2v-bert'

nosiy_msr_dir = "entailment/noisy_msr"

def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--bs', help='batch size', type=int, default=32)
    parser.add_argument('--hs', help='hidden size', type=int, default=256)
    parser.add_argument('--k', help='k', type=int, default=10)
    parser.add_argument('--model_file', type=str, default=None)
    parser.add_argument('--data_type', choices=('twitter', 'msr'), type=str, default='msr')
    parser.add_argument('--model_type', choices=('w2v', 'robust', 'robust-naive', 'fasttext'), type=str, default='w2v')
    parser.add_argument('--likelihood', choices=('w2v', 'bert', 'w2v-bert'), type=str, default='w2v')
    parser.add_argument('--classifier', choices=('cosine', 'lstm'), type=str, default='cosine')
    parser.add_argument('--spellcheck', help='level of synthetic noise to inject', choices=('none', 'lev'), type=str,
                        default='none')
    parser.add_argument('--noise', help = 'level of synthetic noise to inject', type=float, default=0)
    parser.add_argument('--max_vocab', help='max vocab size', type=int, default=-1)

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    if args.model_type == 'w2v':
        vocab_file = google_w2v_vocab
        w2v_file = google_w2v_file
        kv = True
    else:
        if args.likelihood == 'w2v':
            kv = False
            w2v_file = base_w2v_file
            vocab_file = base_vocab_file
        else:  # args.likelihood == "bert" or args.likelihood == "w2v-bert":
            vocab_file = bert_vocab_file
            w2v_file = google_w2v_file
            kv = True

    w2v, ss = load_vocab(vocab_file, w2v_file, keyedvectors=kv)
    #cost_matrix = torch.load(cost_file)
    #nn_matrix = torch.load(neighbour_file)
    #train_pairs, train_labels = load_msr(msr_train)
    #test_pairs, test_labels = load_msr(msr_test)

    if args.data_type == 'twitter':
        #train_pairs, train_labels = load_twitter(twitter_train, train=True)
        test_pairs, test_labels = load_twitter(twitter_test, train=False, noise_probability = args.noise)
    elif args.data_type == 'msr':
        #train_pairs, train_labels = load_msr(msr_train)
        test_pairs, test_labels = load_msr(msr_test, noise_probability=args.noise)
    else:
        raise NotImplementedError

    datasets = []
    if len(args.pregen) > 0:
        for n in args.pregen:
            noise_prefix = str(args.noise) if args.noise > 0 else "0"
            msr_test = os.path.join(nosiy_msr_dir, str(n), noise_prefix + "_msr_paraphrase_test.jsonl")
            test_pairs, test_labels = load_msr(msr_test, noise_probability=0, max_pairs=args.max_pairs)
            datasets.append((test_pairs, test_labels))
    else:
        if args.data_type == 'twitter':
            # train_pairs, train_labels = load_twitter(twitter_train, train=True)
            test_pairs, test_labels = load_twitter(twitter_test, train=False, noise_probability=args.noise)
        elif args.data_type == 'msr':
            # train_pairs, train_labels = load_msr(msr_train)
            test_pairs, test_labels = load_msr(msr_test, noise_probability=args.noise)
        else:
            raise NotImplementedError

    if args.likelihood == 'w2v':
        if args.model_type == 'w2v':
            emb = VectorEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab)
        elif args.model_type == 'fasttext':
            vocab_path = 'data/wiki.en.vec'
            from torchtext import vocab
            voc = vocab.Vectors(vocab_path, max_vectors = args.max_vocab)
            emb = VectorEmbeddings.from_fasttext(voc)
        elif args.model_type == 'robust-naive':
            emb = RobustEmbeddingsNaive.from_w2v(w2v, max_vocab=args.max_vocab)
        else:
            emb = RobustEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab)

    elif args.likelihood == 'bert':
        bert_tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
        if args.model_type == 'w2v':
            bert_model = BertForMaskedLM.from_pretrained('bert-base-uncased')
            emb = VectorEmbeddings.from_bert(bert_model, bert_tokenizer)
        elif args.model_type == 'robust-naive':
            bert_model = BertForMaskedLM.from_pretrained('bert-base-uncased')
            emb = RobustEmbeddingsNaive.from_bert(bert_model, bert_tokenizer)
        else:
            bert_saved_model = BertForMaskedLM.from_pretrained('bert-base-uncased')
            bert_model = BertForDensityEstimation.from_bfcmlm(bert_saved_model)
            emb = RobustEmbeddings.from_bert(bert_model, bert_tokenizer)


    elif args.likelihood == 'w2v-bert':
        '''
        w2v = KeyedVectors.load_word2vec_format(google_w2v_file, binary=True, limit = 50000)
        bert_tokenizer = BertTokenizer.from_pretrained(w2v_bert_file)
        bert_config = BertConfig.from_pretrained(w2v_bert_file)
        bert_saved_model = W2VBertForMaskedLM.from_pretrained(w2v_bert_file, torch.Tensor(w2v.wv.vectors), config=bert_config)
        bert_model = BertForDensityEstimation.from_w2vbfmlm(bert_saved_model)
        '''
        #pretrained_bert = tinybert_dir if args.bert_model == 'tiny' else 'bert-base-uncased'
        pretrained_bert = 'bert-base-uncased'
        bert_tokenizer = BertTokenizer.from_pretrained(pretrained_bert)
        bert_model = BertForMaskedLM.from_pretrained(pretrained_bert)
        if args.model_type == 'robust-naive':
            emb = RobustEmbeddingsNaive.from_w2v_bert(bert_model,
                                                      bert_tokenizer, w2v, tau=args.tau)
        else:
            emb = RobustEmbeddings.from_w2v_bert(bert_model, bert_tokenizer, w2v, tau_cost=args.tau)

    (oov_pairs, oov_labels), (iv_pairs, iv_labels) = split_oov_pairs(test_pairs, test_labels, emb.voc.t2i)

    if args.spellcheck == 'lev':
        test_pairs = [(naive_spellcheck(seq, ss, emb.voc.t2i) for seq in pair) for pair in test_pairs]

    if args.classifier == 'lstm':
        model, optim = make_lstm_classifier(emb.dim, args.hs, emb, 1)
        outfile = os.path.join(save_folder, args.model_file + ".pt")
        model.load_state_dict(torch.load(outfile))
    else:
        model = CosineClassifier(emb)

    all_metrics = []
    for pairs, labels in datasets:
        MAX_LENGTH = 100
        if args.model_type == 'robust' or args.model_type == 'robust-naive':
            all_batches = ClassificationBatchGenerator(pairs, labels, emb.voc, MAX_LENGTH, ss, args.k, n_inputs=2,
                                                       robust=True).make_batches(args.bs)
        else:
            all_batches = ClassificationBatchGenerator(pairs, labels, emb.voc, MAX_LENGTH, n_inputs=2, robust=False,
                                                       exclude_oov=True).make_batches(args.bs)

        trivial = args.trivial and ('robust' in args.model_type)
        if args.likelihood == "bert":
            val, metrics = eval_paraphrase(model, all_batches, gibbs_iters=args.iters, trivial=trivial,
                                           ensemble=args.ensemble, m=args.m)
        else:
            val, metrics = eval_paraphrase(model, all_batches, trivial=trivial, ensemble=args.ensemble, m=args.m)
        all_metrics.append(metrics)

        print("AUC:", metrics)

    print("Final Results:", str(all_metrics), "\tAverage:", str(sum(all_metrics) / len(all_metrics)))

    '''
    acc, prec, rec, f1 = all_metrics
    oov_acc, oov_prec, oov_rec, oov_f1 = oov_metrics
    iv_acc, iv_prec, iv_rec, iv_f1 = iv_metrics
    print("All Validation Loss:", all_val)
    print("All Accuracy:", acc, "\tAll Precision:", prec, "\tAll Recall:", rec, "All F1:", f1)
    print("OOV Validation Loss:", oov_val)
    print("OOV Accuracy:", oov_acc, "\tOOV Precision:", oov_prec, "\tOOV Recall:", oov_rec, "OOV F1:", oov_f1)
    print("IV Validation Loss:", iv_val)
    print("IV Accuracy:", iv_acc, "\tIV Precision:", iv_prec, "\tIV Recall:", iv_rec, "IV F1:", iv_f1)
    


    name = args.model_file if args.model_file is not None else "paraphrase"

    score_file = os.path.join(save_folder, name + ".eval.txt")
    f = open(score_file, 'w')
    f.write("All Validation Loss:" + str(all_val))
    f.write("All Accuracy:" + str(acc) + "\tAll Precision:" + str(prec) + "\tAll Recall:" + str(rec) + "All F1:" + str(f1))
    f.write("OOV Validation Loss:" + str(oov_val))
    f.write("OOV Accuracy:" + str(oov_acc) + "\tOOV Precision:" + str(oov_prec) + "\tOOV Recall:" + str(
        oov_rec) + "OOV F1:" + str(oov_f1))
    f.write("IV Validation Loss:" + str(iv_val))
    f.write("IV Accuracy:" + str(iv_acc) + "\tIV Precision:" + str(iv_prec) + "\tIV Recall:" + str(
        iv_rec) + "iv F1:" + str(iv_f1))
    f.close()
    '''

class Net(nn.Module):
    def __init__(self, test_img=None, fc_size=None):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)

        assert test_img is not None or fc_size is not None
        if fc_size is not None:
            l, w, c = fc_size

        else:
            l, w, c = self._determine_size(test_img)
        self.fc_size = l * w * c
        self.fc1 = nn.Linear(self.fc_size, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def _determine_size(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        return x.size()

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, self.fc_size)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x


