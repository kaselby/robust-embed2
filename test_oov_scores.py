import argparse
import os

from bayes_lev_embed import RobustEmbeddings, W2VEmbeddings
#from paraphrase.paraphrase_detection import *
from sentiment.sentiment_analysis import *
from preprocess import *

#torch.backends.cudnn.deterministic=True


w2v_file = "./data/news_word2vec_model.w2v"
vocab_file = "./data/news_vocab_symspell_20000.txt"
cost_file = "data/news_dists_matrix.pt"
neighbour_file = "data/news_neighbours_matrix.pt"

'''
msr_train = "paraphrase/MSRParaphraseCorpus/msr_paraphrase_train.txt"
msr_test = "paraphrase/MSRParaphraseCorpus/msr_paraphrase_test.txt"
twitter_train = "paraphrase/SemEval-PIT2015/train.data"
twitter_test = "paraphrase/SemEval-PIT2015/test.data"
'''

twitter_train = "sentiment/SemEval2017-task4/twitter-2016train-A.txt"
twitter_test = "sentiment/SemEval2017-task4/twitter-2016test-A.txt"



save_folder = "./models/"


model_file = "sentiment_robust_news_mv20k_2"
bs=32
hs=256
k=10


w2v, ss = load_vocab(vocab_file, w2v_file)
emb = RobustEmbeddings.from_w2v(w2v, max_vocab=20000)
cost_matrix = torch.load(cost_file)
nn_matrix = torch.load(neighbour_file)

test_pairs, test_labels = load_twitter(twitter_test)

(oov_pairs, oov_labels), (iv_pairs, iv_labels) = split_oov_pairs(test_pairs, test_labels, emb.t2i)

emb_w2v = W2VEmbeddings.from_w2v(w2v, max_vocab=10000)
model, _ = make_classifier(emb.dim, hs, emb, 1)
#model_w2v, _ = make_classifier(emb.dim, hs, emb_w2v, 1)

outfile = os.path.join(save_folder, model_file + ".pt")
model.load_state_dict(torch.load(outfile))



oov_batches_r = batch_seqs(oov_pairs, oov_labels, bs, make_batch, emb.t2i, ss, k,
                         vocab_tensors=(nn_matrix, cost_matrix))
iv_batches_r = batch_seqs(iv_pairs, iv_labels, bs, make_batch, emb.t2i, ss, k,
                         vocab_tensors=(nn_matrix, cost_matrix))
all_batches_r = batch_seqs(test_pairs, test_labels, bs, make_batch, emb.t2i, ss, k,
                        vocab_tensors=(nn_matrix, cost_matrix))

_, oov_metrics_r = eval_paraphrase(model, oov_batches_r)
_, iv_metrics_r = eval_paraphrase(model, iv_batches_r)
_, all_metrics_r = eval_paraphrase(model, all_batches_r)

model.embed = emb_w2v
oov_batches_w2v = batch_seqs(oov_pairs, oov_labels, bs, make_batch_w2v, emb.t2i)
iv_batches_w2v = batch_seqs(iv_pairs, iv_labels, bs, make_batch_w2v, emb.t2i)
all_batches_w2v = batch_seqs(test_pairs, test_labels, bs, make_batch_w2v, emb.t2i)

_, oov_metrics_w2v = eval_paraphrase(model, oov_batches_w2v)
_, iv_metrics_w2v = eval_paraphrase(model, iv_batches_w2v)
_, all_metrics_w2v = eval_paraphrase(model, all_batches_w2v)

def print_scores(acc, prec, rec, f1):
    print("Accuracy:", acc, "\tPrecision:", prec, "\tRecall:", rec, "F1:", f1)


print("Robust:")
print("All")
print_scores(*all_metrics_r)
print("OOV")
print_scores(*oov_metrics_r)
print("IV")
print_scores(*iv_metrics_r)

print("\nW2V:")
print("All")
print_scores(*all_metrics_w2v)
print("OOV")
print_scores(*oov_metrics_w2v)
print("IV")
print_scores(*iv_metrics_w2v)

