import sys
import os

from preprocess import PAD, UNK, EOS, SOS
from utils import *

class Beam(object):
    def __init__(self, tokens, score, state, max_length, eos_index):
        self.tokens=tokens
        self.score=score
        self.state=state
        self.max_length = max_length
        self.eos_index = eos_index

    def update(self, token, score, state):
        return Beam(self.tokens+[token], self.score*score, state, self.max_length, self.eos_index)

    def stop(self):
        return len(self.tokens) >= self.max_length or self.eos_index in self.tokens


class BeamIterator(object):
    def __init__(self, model, initial_state, encoder_outputs, beam_width, num_beams, max_length, sos_index, eos_index):
        self.model = model
        self.initial_state = initial_state
        self.encoder_outputs = encoder_outputs
        self.bs = encoder_outputs.size(0)
        self.beam_width = beam_width
        self.num_beams = num_beams
        self.max_length = max_length
        self.sos_index = sos_index
        self.eos_index = eos_index

    def print_beams(self,beams):
        for beam in beams:
            print(self.model.wd.to_words(beam.tokens))

    def stop_search(self, beams):
        for beam in beams:
            if not beam.stop():
                return False
        return True

    def search(self):
        stop = False
        beams = [Beam([self.sos_index], 1., self.initial_state, self.max_length, self.eos_index)]
        while not stop:
            beams = self._search_iter(beams)
            if self.stop_search(beams):
                stop = True
        return beams[0].tokens[1:]      # drop the sos token

    def _search_iter(self, beams):
        current_beams = []
        for beam in beams:
            if not beam.stop():
                beam_input = beam.tokens[-1]
                decoder_input = torch.LongTensor([beam_input]).unsqueeze(0)
                if use_cuda:
                    decoder_input = decoder_input.cuda()

                decoder_output, decoder_hidden = self.model._decode_step(decoder_input, beam.state, self.encoder_outputs,
                                                                        lengths=None)
                output_probs = F.softmax(decoder_output, dim=1)

                topv, topi = output_probs.data.topk(self.beam_width)
                for j in range(self.beam_width):
                    current_beams.append(beam.update(int(topi[0,j]), topv[0,j], decoder_hidden))
            else:
                current_beams.append(beam)

        sorted_beams = sorted(current_beams, key=lambda x: x.score, reverse=True)
        top_beams = sorted_beams[:self.num_beams]

        return top_beams

