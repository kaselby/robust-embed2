"""This module generates instances of noisy data for consistent comparison

For specified tasks, this module automatically creates folders ('noisy_sst', 'noisy_snli', 'noisy_msr' respectively)
under the task folder. It generates many instances of data with 5 levels of noise injected. An example of the file
system structure is:
-entailment
    -noisy_snli
        -1
            -0.1_snli_1.0_test.jsonl
            -0.2_snli_1.0_test.jsonl
            -...
        -2
            -...
        -3
            -...
where the first number in the file name corresponds to the level of noise

    Example of usage:
    python noisy_data_gen.py --repeat 5
"""

import os
import shutil
import argparse
from tqdm import tqdm
import json
from nltk.tokenize.treebank import TreebankWordDetokenizer

from preprocess import normalize_string
from preprocessing import synthetic_noise


NOISE_LEVELS = [0, 0.02, 0.05, 0.1, 0.2]

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--repeat', help = 'repetitions of data generation', type = int, default = 1)
    parser.add_argument('--tasks', nargs = '+', help = 'the tasks to generate data for', choices = ['entailment', 'sentiment', 'paraphrase'], default = ['entailment', 'sentiment', 'paraphrase'])
    parser.add_argument('--clean', help = 'clear existing runs if set to true', action = 'store_true', default = False)
    return parser.parse_args()

def add_noise(sent, noise_level):
    words = synthetic_noise.process_sentence(normalize_string(sent).split(' '), noise_level)
    sent = TreebankWordDetokenizer().detokenize(words)
    return sent

def clean_dir(dir):
    if os.path.exists(dir):
        shutil.rmtree(dir)

def print_bar(n):
    if n == 2:
        print('========================')
    else:
        print('------------------------')

def generate(data_file, output_dir, task_fct, args):
    if args.clean:
        print_bar(2)
        print('cleaning existing noisy data for {}'.format(str(task_fct).split(' ')[1]))
        clean_dir(output_dir)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    file_counter = 0

    for subdir in os.listdir(output_dir):
        try:
            file_num = int(subdir)
            if file_num > file_counter:
                file_counter = file_num
        except Exception:
            pass
    for i in range(args.repeat):
        print_bar(1)
        subdir = os.path.join(output_dir, str(i+1+file_counter))
        os.makedirs(subdir)
        for noise_level in NOISE_LEVELS:
            print('task:{}\t\trep:{}\tnoise_level:{}'.format(str(task_fct).split(' ')[1], i+1, noise_level))
            out_file = os.path.join(subdir, '{}_{}'.format(noise_level, os.path.basename(data_file)))
            print('\twriting to {}'.format(out_file))

            task_fct(data_file, out_file, noise_level)

def entailment(data_file, out_file, noise_level):
    input = open(data_file, 'r')
    output = open(out_file, 'w+')

    for line in input.readlines():
        json_obj = json.loads(line)
        json_obj['sentence1'] = add_noise(json_obj['sentence1'], noise_level)
        json_obj['sentence2'] = add_noise(json_obj['sentence2'], noise_level)
        output.write(json.dumps(json_obj))
        output.write('\n')

    input.close()
    output.close()

def sentiment(data_file, out_file, noise_level):
    input = open(data_file.replace('test', 'train'), 'r')
    output = open(out_file.replace('test', 'train'), 'w+')

    header = input.readline()
    output.write(header)
    for line in input.readlines():
        sentence, label = line.strip().rsplit(',', 1)
        sentence = add_noise(sentence, noise_level)
        output.write('{},{}\n'.format(sentence, label))

    input.close()
    output.close()

    input = open(data_file, 'r')
    output = open(out_file, 'w+')

    header = input.readline()
    output.write(header)
    for line in input.readlines():
        sentence, label = line.strip().rsplit(',', 1)
        sentence = add_noise(sentence, noise_level)
        output.write('{},{}\n'.format(sentence, label))

    input.close()
    output.close()

def paraphrase(data_file, out_file, noise_level):
    input = open(data_file, 'r')
    output = open(out_file, 'w+')

    header = input.readline()
    output.write(header)
    for line in input.readlines():
        label, id1, id2, s1, s2 = line.split("\t")
        s1 = add_noise(s1, noise_level)
        s2 = add_noise(s2, noise_level)
        output.write('\t'.join([label, id1, id2, s1, s2]))
        output.write('\n')

    input.close()
    output.close()

if __name__ == '__main__':
    args = parse_args()
    if 'entailment' in args.tasks:
        data_file = 'entailment/snli_1.0/snli_1.0_test.jsonl'
        output_dir = 'entailment/noisy_snli'
        task_fct = entailment
        generate(data_file, output_dir, task_fct, args)
    if 'sentiment' in args.tasks:
        data_file = 'sentiment/stanfordSentimentTreebank/test.txt'
        output_dir = 'sentiment/noisy_sst'
        task_fct = sentiment
        generate(data_file, output_dir, task_fct, args)
    if 'paraphrase' in args.tasks:
        data_file = 'paraphrase/MSRParaphraseCorpus/msr_paraphrase_test.txt'
        output_dir = 'paraphrase/noisy_msr'
        task_fct = paraphrase
        generate(data_file, output_dir, task_fct, args)
