from translation.translate_kira import *
from bayes_lev_embed import *



mtnt_train = "translation/MTNT/train/train.en-fr.tsv"
mtnt_test = "translation/MTNT/test/test.en-fr.tsv"
wmt_train = "translation/WMT/training/europarl-v7.fr-en"
wmt_test = "translation/WMT/dev/news-test2008"

en_vocab_file = "data/clean_news_vocab_symspell.txt"
en_w2v_file = "data/clean_news_word2vec_model.w2v"
fr_vocab_file = "data/french_vocab_symspell.txt"
fr_w2v_file = "data/french_word2vec_model.w2v"

data_type="wmt"

model_file = ""
model_type = "w2v"
hs=1024

en_w2v, en_ss = load_vocab(en_vocab_file, en_w2v_file)
fr_w2v, fr_ss = load_vocab(fr_vocab_file, fr_w2v_file)

if model_type == 'w2v':
    en_emb = W2VEmbeddings.from_w2v(en_w2v)
    fr_emb = W2VEmbeddings.from_w2v(fr_w2v)
else:
    en_emb = RobustEmbeddings.from_w2v(en_w2v)
    fr_emb = RobustEmbeddings.from_w2v(fr_w2v)

model, optim = create_model(en_emb, fr_emb, hs, 1)
checkpoint = torch.load(model_file)
model.load_state_dict(checkpoint['model_state_dict'])

if data_type == 'wmt':
    test_pairs = load_WMT(wmt_test)
else:
    test_pairs = load_mtnt(mtnt_test)
