import argparse
import os
from transformers import BertModel, BertTokenizer, BertForMaskedLM, DistilBertModel, DistilBertTokenizer, DistilBertForMaskedLM
from tqdm import tqdm
import torch
import numpy as np

from bayes_lev_embed import RobustEmbeddings, RobustEmbeddingsNaive, VectorEmbeddings
from entailment.entailment_detection import *
from preprocess import *
from bert_model import BertForDensityEstimation, BertForContinuousMaskedLM
from preprocessing.synthetic_noise import process_sentence

from utils import load_sentences

#w2v_file = "./data/reddit_10m_word_level_processed_3_word2vec_model.w2v"
#vocab_file = "./data/reddit_vocab_symspell.txt"
w2v_file = "data/clean_news_word2vec_model.w2v"
base_vocab_file = "data/clean_news_vocab_symspell.txt"
bert_vocab_file = "data/bert_vocab_symspell.txt"
tinybert_vocab_file = "data/tinybert_vocab_symspell.txt"
distilbert_vocab_file = "data/distilbert_vocab_symspell.txt"
tinybert_dir = "data/TinyBERT_4_312"

#w2v_file = "preprocessing/GoogleNews-vectors-negative300.bin"
#vocab_file = "preprocessing/google_w2v_vocab.txt"
#cost_file = "data/news_dists_matrix.pt"
#neighbour_file = "data/news_neighbours_matrix.pt"

save_folder = "./models"

tinybert_dir = "data/finetuned-tinybert/checkpoint-2000"
minilm_dir = "data/finetuned-minilm/checkpoint-2000"
model_classes={
    'base': (BertTokenizer, BertForContinuousMaskedLM, BertForDensityEstimation.from_bfcmlm, 'bert-base-uncased'),
    'tiny': (BertTokenizer, BertForContinuousMaskedLM, BertForDensityEstimation.from_bfcmlm, tinybert_dir),
    'mini': (BertTokenizer, BertForContinuousMaskedLM, BertForDensityEstimation.from_bfcmlm, minilm_dir),
    'distill': (DistilBertTokenizer, DistilBertForMaskedLM, BertForDensityEstimation.from_dbfcmlm, 'distilbert-base-uncased')
}

noise_levels = [0,0.02,0.05,0.1,0.2]
out_dir = "out"

def eval_model(emb, ss, m, n, k, **kwargs):
    seqs = load_sentences(**kwargs)
    counts = 0
    error_probs = []
    for (i, seq, noisy_seq) in tqdm.tqdm(seqs):
        highlight_orig = list(seq)
        highlight_orig[i] = '{' + highlight_orig[i] + '}'
        #print('\nOriginal Sentence: {}'.format(' '.join(highlight_orig)))
        highlight_noisy = list(noisy_seq)
        highlight_noisy[i] = '{' + highlight_noisy[i] + '}'
        #print('   Noisy Sentence: {}'.format(' '.join(highlight_noisy)))

        with torch.no_grad():
            posterior_samples, (prior_knearest, likelihood_knearest, posterior_knearest), (
                (pr_pr, pr_l, pr_po), (l_pr, l_l, l_po), (po_pr, po_l, po_po)) = emb.analyze_seq(noisy_seq, ss, i, m=m,
                                                                                                 n=n, k=k,
                                                                                                 only_oov=False)

        top1_posteriors = posterior_knearest[:, 0]
        wrong_posterior = False
        errors = 0
        for top_posterior in emb.indices_to_words(top1_posteriors):
            if top_posterior != seq[i]:
                wrong_posterior = True
                errors += 1
        if wrong_posterior:
            counts += 1
            error_probs.append(errors / len(top1_posteriors))
    prob_wrong = counts / len(seqs)
    avg_wrong = sum(error_probs) / len(error_probs) if len(error_probs) > 0 else -1

    return prob_wrong, avg_wrong


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--hs', help='hidden size', type=int, default=256)
    parser.add_argument('--m', help='m', type=int, default=5)
    parser.add_argument('--n', help='n', type=int, default=20)
    parser.add_argument('--k', help='k', type=int, default=10)
    parser.add_argument('--sentence', help='sentence to debug', type=str, default='')
    parser.add_argument('--word', help='a wrong word in the input sentence', type=str, default='')
    parser.add_argument('--seed', help='the seed for random generation', type=int, default=-1)
    parser.add_argument('--model_type', choices=('w2v', 'robust', 'robust-naive', 'fasttext'), type=str, default='w2v')
    parser.add_argument('--likelihood', choices=('w2v', 'bert'), type=str, default='w2v')
    parser.add_argument('--bert_model', choices=('base', 'tiny', 'distill'), type=str, default='base')
    parser.add_argument('--checkpoint_file', type=str, default=None)
    parser.add_argument('--noise', help = 'level of synthetic noise to inject', type=float, default=0)
    parser.add_argument('--max_vocab', help='max vocab size', type=int, default=-1)
    parser.add_argument('--N_samples', help='N', type=int, default=10)
    parser.add_argument('--print_all', help='whether to print correct predictions as well', action='store_true')
    parser.add_argument('--dataset', choices=('mtnt', 'snli'), type=str, default='mtnt')
    parser.add_argument('--variance_file', type=str, default=None)
    parser.add_argument('--prefix', help='output file prefix', type=str, default="")

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    vocab_file = bert_vocab_file

    Sigma = None
    tau = 0.6
    if args.variance_file is not None:
        saved_param = torch.load(args.variance_file)
        Sigma = saved_param['embed.Sigma'].abs()
        tau = float(saved_param['embed.tau'])

    w2v, ss = load_vocab(vocab_file, w2v_file)
    #cost_matrix = torch.load(cost_file)
    #nn_matrix = torch.load(neighbour_file)

    seed = args.seed
    if seed == -1:
        seed = random.randrange(1000000)
    random.seed(seed)
    torch.manual_seed(seed)
    np.random.seed(seed)
    print('seed used is {}\n'.format(seed))

    k = args.k
    n = args.n
    m = args.m

    results = {}
    for noise_prob in noise_levels:
        results[noise_prob]={}
        for model_name, (tokenizer_class, pretrained_class, model_fct, model_path) in model_classes.items():
            tokenizer = tokenizer_class.from_pretrained(model_path)
            pretrained_model = pretrained_class.from_pretrained(model_path)
            model = model_fct(pretrained_model)
            emb = RobustEmbeddings.from_bert(model, tokenizer)
            prob_wrong, avg_wrong = eval_model(emb, ss, m, n, k, noise_probability=noise_prob, num_sample=args.N_samples, dataset="snli")
            print(model_name, "@", noise_prob, "% noise:\tprob wrong:", prob_wrong, "avg wrong:", avg_wrong)
            results[noise_prob][model_name] = {"prob wrong": prob_wrong, "avg wrong": avg_wrong}
            del tokenizer, pretrained_model, model, emb

    output_eval_file = os.path.join(out_dir, args.prefix, "eval_results.txt")
    with open(output_eval_file, "w") as writer:
        for noise, noise_results in results.items():
            writer.write(str(noise*100) + ":\n")
            for model_name, model_results in noise_results.items():
                writer.write("\t" + model_name + ":\n")
                for key, value in model_results.items():
                    writer.write("\t\t"+key+":\t"+str(value)+"\n")


