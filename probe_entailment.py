import argparse
import os
from transformers import BertModel, BertTokenizer, BertForMaskedLM, BertConfig

from bayes_lev_embed import RobustEmbeddings, RobustEmbeddingsNaive, VectorEmbeddings
from entailment.entailment_detection import *
from preprocess import *
from bert_model import BertForContinuousMaskedLM, BertForDensityEstimation, W2VBertForMaskedLM


class ProbeBatchGenerator(BatchGenerator):
    def __init__(self, data, labels, vocab, max_length, *args, robust=False, **kwargs):
        super().__init__(data, vocab, max_length, *args, robust=robust, **kwargs)
        self.labels = torch.FloatTensor(labels)
        if use_cuda:
            self.labels = self.labels.cuda()

    def build_tensors(self, *args, robust=False, **kwargs):
        self.tensor_class = NaiveTensors if not robust else RobustTensors
        if self.n_inputs == 1:
            self.tensors = self.tensor_class(self.data[:self.data_size], self.max_length, self.vocab, *args, **kwargs)
            self.tensors.build_tensors()
        else:
            split_data = list(zip(*self.data[:self.data_size]))
            assert self.n_inputs == len(split_data)
            self.tensors = [self.tensor_class(split_data[i], self.max_length, self.vocab, *args, **kwargs) for i in
                            range(self.n_inputs)]
            for T in self.tensors:
                T.build_tensors()

    def _make_batches(self, batch_size):
        n_batches = math.ceil(self.data_size / batch_size)
        p = torch.randperm(self.data_size)

        for i in range(n_batches):
            min_j = i * batch_size
            max_j = min((i + 1) * batch_size, self.data_size)
            jrange = p[min_j:max_j]
            if self.n_inputs == 1:
                batch = self.tensors._make_batch(jrange)
            else:
                batch = [T._make_batch(jrange) for T in self.tensors]
            yield batch, self.labels[jrange], jrange


def probe_entailment(classifier, batch_gen, print_every = -1, **kwargs):
    predicted_robust = []
    predicted_trivial = []
    true = []
    highlighted = []
    total = 0
    total_loss = 0
    with torch.no_grad():
        # batches = batch_seqs(pairs, targets, bs, batch_fct, *args, **kwargs)
        for (((b1, l1), (b2, l2)), t, J) in tqdm.tqdm(batch_gen()):
            probs_robust = classifier(b1, b2, l1, l2, **kwargs)
            probs_trivial = classifier(b1, b2, l1, l2, trivial=True, **kwargs)
            # loss = loss_fct(probs, t)
            # total_loss += loss.item()

            for i in range(t.size(0)):
                if t[i].item() == True:
                    if probs_robust[i].item() < probs_trivial[i].item():
                        highlighted.append(J[i].item())
                else:
                    if probs_robust[i].item() > probs_trivial[i].item():
                        highlighted.append(J[i].item())

            predicted_robust += probs_robust.tolist()
            predicted_trivial += probs_trivial.tolist()
            true += t.tolist()
            total += 1

            if print_every > 0 and total % print_every == 0:
                print("Eval Batches Processed:", total, "\n")
    predicted_robust = torch.FloatTensor(predicted_robust)
    predicted_trivial = torch.FloatTensor(predicted_trivial)
    true = torch.LongTensor(true)
    # acc, prec, rec, f1 = get_class_scores(predicted, true)
    auc_robust = roc_auc_score(true, predicted_robust)
    auc_trivial = roc_auc_score(true, predicted_trivial)

    return auc_robust, auc_trivial, highlighted

#torch.backends.cudnn.deterministic=True

#w2v_file = "./data/reddit_10m_word_level_processed_3_word2vec_model.w2v"
#vocab_file = "./data/reddit_vocab_symspell.txt"
base_w2v_file = "data/clean_news_word2vec_model.w2v"
base_vocab_file = "data/clean_news_vocab_symspell.txt"
bert_vocab_file = "data/bert_vocab_symspell.txt"
tinybert_vocab_file = "data/tinybert_vocab_symspell.txt"
tinybert_dir = "data/TinyBERT_4_312"

#w2v_file = "data/GoogleNews-vectors-negative300.bin"
#vocab_file = "preprocessing/google_w2v_vocab.txt"
#cost_file = "data/news_dists_matrix.pt"
#neighbour_file = "data/news_neighbours_matrix.pt"
noisy_snli_dir = "entailment/noisy_snli"

snli_train = "entailment/snli_1.0/snli_1.0_train.jsonl"
snli_test = "entailment/snli_1.0/snli_1.0_test.jsonl"
snli_test_rove = "entailment/snli_1.0/test.txt"
google_w2v_file = "data/GoogleNews-vectors-negative300.bin.gz"
google_w2v_vocab = "data/google_vocab_symspell.txt"

w2v_bert_file='data/w2v-bert'

b2v_path = "data/b2v_model.bin"

save_folder = "./models"

noise=0.05
pregen=1
max_pairs=100
tau=1
bs=16
k=10

vocab_file = bert_vocab_file
w2v_file = google_w2v_file
kv = True
w2v, ss = load_vocab(vocab_file, w2v_file, keyedvectors=kv)

noise_prefix = str(noise) if noise > 0 else "0"
snli_test = os.path.join(noisy_snli_dir, str(pregen), noise_prefix+"_snli_1.0_test.jsonl")
test_pairs, test_labels = load_snli(snli_test, noise_probability=0, max_pairs=max_pairs)
#datasets.append((test_pairs,test_labels))

pretrained_bert = 'bert-base-uncased'
bert_tokenizer = BertTokenizer.from_pretrained(pretrained_bert)
bert_model = BertForMaskedLM.from_pretrained(pretrained_bert)
emb = RobustEmbeddingsNaive.from_w2v_bert(bert_model, bert_tokenizer, w2v, tau=tau)

model = CosineClassifier(emb)

MAX_LENGTH=100
all_batches = ProbeBatchGenerator(test_pairs, test_labels, emb.voc, MAX_LENGTH, ss, k, n_inputs=2, robust=True).make_batches(bs)

auc_robust, auc_trivial, highlighted = probe_entailment(model, all_batches)


def probe_pair(pair, label, emb, ss, k):
    seq1, seq2 = pair
