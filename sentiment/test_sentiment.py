from bayes_lev_embed import RobustEmbeddings
from sentiment.sentiment_analysis import *
from preprocess import *

torch.backends.cudnn.deterministic=True

bs = 4
k = 10
n = 20
hs = 128
lr = 0.0001

w2v_file = "./data/reddit_10m_word_level_processed_3_word2vec_model.w2v"
vocab_file = "./data/reddit_vocab_symspell.txt"
cost_file = "data/dists_matrix.pt"
neighbour_file = "data/neighbours_matrix.pt"

twitter_train = "sentiment/SemEval2017-task4/twitter-2016train-A.txt"
twitter_test = "sentiment/SemEval2017-task4/twitter-2016test-A.txt"

w2v, ss = load_vocab(vocab_file, w2v_file)
emb = RobustEmbeddings.from_w2v(w2v)
cost_matrix = torch.load(cost_file)
nn_matrix = torch.load(neighbour_file)

train_pairs, train_labels = load_twitter(twitter_train)
#test_pairs, test_labels = load_twitter(twitter_test)

emb_fct = w2v_model(emb.W)
model, optim = make_classifier(emb.dim, hs, emb, emb.W.size(0), lr)

#train_batches = batch_seqs(train_pairs, train_labels, bs, make_batch_w2v, emb.t2i)
train_batches = batch_seqs(train_pairs, train_labels, bs, make_batch, emb.t2i, ss, k, vocab_tensors=(nn_matrix, cost_matrix))

(b,l),t = train_batches().__next__()