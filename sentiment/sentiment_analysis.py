import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
import nltk
import numpy as np
from numpy import dot
from numpy.linalg import norm
import re
from string import punctuation
import math
from sklearn.naive_bayes import BernoulliNB

from utils import *
from preprocessing import synthetic_noise

# preprocess string
def normalize_string(s):
    s = s.lower().strip()
    s = re.sub('\!+', '!', s)
    s = re.sub('\,+', ',', s)
    s = re.sub('\?+', '?', s)
    s = re.sub('\.+', '.', s)
    #s = re.sub("[^0-9a-zA-Z.!?,'']+", ' ', s)
    for p in punctuation:
      if p != "'":
        s = s.replace(p, " " + p + " ")
    s = re.sub(' +', ' ', s)
    s = s.strip()
    return s


# load sst with neutral entries removed and binary classification
def load_sst(filename, noise_probability = 0):
    sentences = []
    labels = []
    infile = open(filename, 'r')
    infile.readline()
    for line in infile:
        sentence, label = line.strip().rsplit(',', 1)
        label = int(label)
        sentence = nltk.tokenize.word_tokenize(sentence)
        if noise_probability > 0:
            sentence = synthetic_noise.process_sentence(sentence, noise_probability)
        if label == 0:
            label = -1
        sentences.append(sentence)
        labels.append(label)
    infile.close()
    return sentences, labels


def load_twitter(filename, noise_probability = 0):
    tweets = []
    labels = []
    infile = open(filename, 'r')
    for line in infile:
        try:
            _, label, tweet = line.strip().split("\t")
        except:
            _, label, tweet, _ = line.strip().split("\t")
        tweet_clean = normalize_string(tweet).split(" ")
        # synthetic noise injection
        if noise_probability > 0:
            tweet_clean = synthetic_noise.process_sentence(tweet_clean, noise_probability)
        tweets.append(tweet_clean)
        if label.strip() == 'neutral':
            labels.append(0)        
        elif label.strip() == 'positive':
            labels.append(1)
        else:
            labels.append(2)
    infile.close()
    return tweets, labels


def split_oov_pairs(pairs, labels, t2i):
    oov_pairs = []
    oov_labels = []
    iv_pairs = []
    iv_labels = []
    for pair, label in zip(pairs, labels):
        oov=False
        for word in pair:
            if not word in t2i:
                oov = True
                break
        if oov:
            oov_pairs.append(pair)
            oov_labels.append(label)
        else:
            iv_pairs.append(pair)
            iv_labels.append(label)
    return (oov_pairs, oov_labels), (iv_pairs, iv_labels)


def batch_seqs(seq_list, labels, batch_size, batch_fct, *args, **kwargs):
    def generate():
        N = len(seq_list)
        n_batches = math.ceil(N / batch_size)
        p = torch.randperm(N)

        for i in range(n_batches):
            min_j = i*batch_size
            max_j = min((i+1)*batch_size, N)
            seqs = [seq_list[p[j]] for j in range(min_j, max_j)]
            targets = torch.LongTensor([labels[p[j]] for j in range(min_j, max_j)])

            # Sort
            #lengths = torch.LongTensor([len(s) for s in seqs])
            #targets = torch.LongTensor([labels[p[j]] for j in range(min_j, max_j)])
            #lengths, perm_idx = lengths.sort(0, descending=True)
            #seqs = [seqs[perm_idx[i]] for i in range(len(seqs))]
            #targets = targets[perm_idx]

            batch, lengths = batch_fct(seqs, *args, **kwargs)

            if use_cuda:
                targets = targets.cuda()
            #batches.append(((b1,b2),targets))
            yield (batch,lengths), targets
    return generate


class LSTMClassifier(nn.Module):
    def __init__(self, input_size, hidden_size, embed, n_layers=1):
        super().__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.embed = embed
        #self.embed = nn.Embedding(vocab_size, input_size, padding_idx=-1)
        self.n_layers = n_layers
        self.lstm = nn.LSTM(input_size, hidden_size, num_layers=1, batch_first=True)
        self.out = nn.Sequential(
            #nn.Linear(hidden_size, hidden_size),
            #nn.Tanh(),
            nn.Linear(hidden_size, 3)
        )

        if use_cuda:
            #self.embed = self.embed.cuda()
            self.lstm = self.lstm.cuda()
            self.out = self.out.cuda()

    def forward(self, batch, lengths, **kwargs):
        embedded = self.embed(batch)                        # bs x l x d

        sorted_lengths, perm_idx = lengths.sort(0, descending=True)
        sorted_embed = embedded[perm_idx, :, :]

        packed = pack_padded_sequence(sorted_embed, sorted_lengths, batch_first=True)
        _, (h, c) = self.lstm(packed)                       # bs x l x hs
        output = self.out(h.squeeze(0))                     # bs x c

        _, unperm_idx = perm_idx.sort(0)
        unsorted_output = output[unperm_idx,:]

        return unsorted_output#F.log_softmax(output, dim=-1)


def make_classifier(input_size, hidden_size, embed, lr):
    model = LSTMClassifier(input_size, hidden_size, embed)
    opt = optim.Adam(model.parameters(), lr=lr)
    return model, opt


def train_sentiment(model, optim, train_data, test_data, epochs, print_every=50, eval_every=3, clip=50, save_checkpoint=None, **kwargs):
    loss_fct = nn.CrossEntropyLoss()
    best_val = float('inf')
    best_acc = 0
    for i in range(epochs):
        total = 0
        epoch_loss = 0
        #batches =  batch_seqs(pairs, targets, bs, batch_fct, *args, **kwargs)
        for ((batch,lengths), targets) in train_data():
            optim.zero_grad()
            bs = targets.size(0)
            #e1, e2 = embed(s1, **kwargs), embed(s2, **kwargs)  # bs x l x d
            probs = model(batch, lengths, **kwargs)
            loss = loss_fct(probs, targets)
            #loss = -(torch.log(probs).dot(t) + torch.log(1-probs).dot(1-t)) / bs
            loss.backward()
            nn.utils.clip_grad_norm(model.parameters(), clip)
            optim.step()
            epoch_loss += loss.item()
            total += 1
            if total % print_every == 0:
                print("Batches Processed:", total, "\nBatch Loss:", loss.item(), "\n")
        print("Epoch:", i, "\nAverage Loss:", epoch_loss/total, "\n")
        if i % eval_every == 0:
            val_loss, acc = eval_sentiment(model, test_data, **kwargs)
            print("Validation Loss:", val_loss,"Accuracy:", acc)
            if val_loss < best_val:
                best_val = val_loss
                best_acc = acc
                if save_checkpoint is not None:
                    torch.save(model.state_dict(), save_checkpoint)
            #acc, prec, rec, f1 = get_class_scores(predicted, true)
            #print("Accuracy:", acc, "\nPrecision:", prec, "\nRecall:", rec, "\nF1 Score:", f1, "\n")
    return best_val, best_acc


def train_sentiment_NB(model, optim, train_data, test_data, epochs, print_every=50, eval_every=3, clip=50, save_checkpoint=None, **kwargs):
    loss_fct = nn.CrossEntropyLoss()
    best_val = float('inf')
    best_acc = 0
    for i in range(epochs):
        total = 0
        epoch_loss = 0
        #batches =  batch_seqs(pairs, targets, bs, batch_fct, *args, **kwargs)
        for ((batch,lengths), targets) in train_data():
            optim.zero_grad()
            bs = targets.size(0)
            #e1, e2 = embed(s1, **kwargs), embed(s2, **kwargs)  # bs x l x d
            probs = model(batch, lengths, **kwargs)
            loss = loss_fct(probs, targets)
            #loss = -(torch.log(probs).dot(t) + torch.log(1-probs).dot(1-t)) / bs
            loss.backward()
            nn.utils.clip_grad_norm(model.parameters(), clip)
            optim.step()
            epoch_loss += loss.item()
            total += 1
            if total % print_every == 0:
                print("Batches Processed:", total, "\nBatch Loss:", loss.item(), "\n")
        print("Epoch:", i, "\nAverage Loss:", epoch_loss/total, "\n")
        if i % eval_every == 0:
            val_loss, acc = eval_sentiment(model, test_data, **kwargs)
            print("Validation Loss:", val_loss,"Accuracy:", acc)
            if val_loss < best_val:
                best_val = val_loss
                best_acc = acc
                if save_checkpoint is not None:
                    torch.save(model.state_dict(), save_checkpoint)
            #acc, prec, rec, f1 = get_class_scores(predicted, true)
            #print("Accuracy:", acc, "\nPrecision:", prec, "\nRecall:", rec, "\nF1 Score:", f1, "\n")
    return best_val, best_acc


def eval_sentiment(classifier, batch_gen, print_every=-1, **kwargs):
    predicted = []
    true = []
    total = 0
    total_loss = 0
    loss_fct = nn.CrossEntropyLoss()
    with torch.no_grad():
        #batches = batch_seqs(pairs, targets, bs, batch_fct, *args, **kwargs)
        for ((batch,lengths), targets) in batch_gen():
            probs = classifier(batch, lengths, **kwargs)
            loss = loss_fct(probs, targets)
            total_loss += loss.item()
            labels = probs.argmax(dim=-1)
            predicted += labels.tolist()
            true += targets.tolist()
            total += 1
            if print_every > 0 and total % print_every == 0:
                print("Eval Batches Processed:", total, "\n")
    predicted = torch.LongTensor(predicted)
    true = torch.LongTensor(true)
    accuracy = (predicted==true).sum().float()/true.size(0)
    '''
    print("Neutral Predictions:", (predicted == 0).sum(),
          "Positive Predictions:", (predicted == 1).sum(),
          "Negative Predictions:", (predicted == 2).sum())
    '''

    return total_loss / total, accuracy