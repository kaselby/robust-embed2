import argparse
import os
import time

from translation.translate_kira import *
from bayes_lev_embed import *
from preprocess import *
import matplotlib.pyplot as plt

from torch.nn import DataParallel

from transformers import BertModel, BertTokenizer, BertForMaskedLM
from bert_model import BertForContinuousMaskedLM, BertForDensityEstimation

mtnt_train = "translation/MTNT/train/train.en-fr.tsv"
mtnt_test = "translation/MTNT/test/test.en-fr.tsv"
wmt_train = "translation/WMT/training/europarl-v7.fr-en"
wmt_test = "translation/WMT/dev/news-test2008"
ted_train = "translation/tedTalk/en_fr/train"
ted_test = "translation/tedTalk/en_fr/test"

base_vocab_file = "data/clean_news_vocab_symspell.txt"
en_w2v_file = "data/clean_news_word2vec_model.w2v"
fr_vocab_file = "data/french_vocab_symspell.txt"
fr_w2v_file = "data/french_word2vec_model.w2v"

bert_vocab_file = "data/bert_vocab_symspell.txt"
tinybert_vocab_file = "data/tinybert_vocab_symspell.txt"
tinybert_dir = "data/TinyBERT_4_312"

checkpoint_file = "translation/models/saved_model.pt"
log_file = "translation/logs/log.txt"


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--bs', help='training batch size', type=int, default=256)
    parser.add_argument('--tbs', help='test batch size', type=int, default=32)
    parser.add_argument('--hs', help='hidden size', type=int, default=2048)
    parser.add_argument('--n', help='n', type=int, default=20)
    parser.add_argument('--k', help='k', type=int, default=10)
    parser.add_argument('--lr', help='learning rate', type=float, default=0.0003)
    parser.add_argument('--tf', help='teacher forcing ratio', type=float, default=0.5)
    parser.add_argument('--heads', help='attn heads', type=int, default=5)
    parser.add_argument('--epochs', help='num epochs', type=int, default=125)
    parser.add_argument('--saved_model', help='model location', type = str, default = '')
    parser.add_argument('--data_type', choices = ('wmt', 'mtnt', 'ted'), type = str, default = 'ted')
    parser.add_argument('--model_type', choices = ('w2v', 'robust'), type = str, default = 'w2v')
    parser.add_argument('--likelihood', choices=('w2v', 'bert'), type=str, default='w2v')
    parser.add_argument('--bert_model', choices=('base', 'tiny'), type=str, default='base')
    parser.add_argument('--max_vocab', help='max vocab size', type=int, default=-1)
    parser.add_argument('--max_pairs', help='number of training pairs', type=int, default=200000)
    parser.add_argument('--noise', help = 'level of synthetic noise to inject', type = float, default = 0)
    parser.add_argument('--parallel', action="store_const", default=False, const=True)

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    bs = args.bs
    hs = args.hs
    lr = args.lr
    best_bleu = [0,0,0,0]
    epochs = [0, args.epochs]
    name = '_'+str(args.model_type)+'_'+str(args.likelihood)+'_'+str(args.data_type)+'_'+str(args.noise)+'_'+str(bs)+'_'+str(hs)+'_'+str(lr)
    checkpoint_file = checkpoint_file[:-3] + name + checkpoint_file[-3:]
    print('model suffix: '+name)
    if len(args.saved_model) > 0:
        checkpoint_file = args.saved_model

    if args.likelihood == 'bert':
        if args.bert_model == 'tiny':
            en_vocab_file = tinybert_vocab_file
        else:
            en_vocab_file = bert_vocab_file
    else:
        en_vocab_file = base_vocab_file
    en_w2v, en_ss = load_vocab(en_vocab_file, en_w2v_file)
    fr_w2v, fr_ss = load_vocab(fr_vocab_file, fr_w2v_file)

    if args.likelihood == 'w2v':
        if args.model_type == 'w2v':
            print('embedding: w2v')
            en_emb = VectorEmbeddings.from_w2v(en_w2v, trainable_tokens=(SOS, EOS))
        else:
            print('embedding: robust')
            en_emb = RobustEmbeddings.from_w2v(en_w2v, trainable_tokens=(SOS, EOS))
        fr_emb = VectorEmbeddings.from_w2v(fr_w2v, trainable_tokens=(SOS, EOS))

    elif args.likelihood == 'bert':
        pretrained_bert = tinybert_dir if args.bert_model == 'tiny' else 'bert-base-uncased'
        bert_tokenizer = BertTokenizer.from_pretrained(pretrained_bert)
        bert_saved_model = BertForMaskedLM.from_pretrained(pretrained_bert)
        bert_model = BertForDensityEstimation.from_bfcmlm(bert_saved_model)
        if args.model_type == 'w2v':
            print('embedding: w2v')
            en_emb = VectorEmbeddings.from_bert(bert_model, bert_tokenizer)
        else:
            print('embedding: robust')
            en_emb = RobustEmbeddings.from_bert(bert_model, bert_tokenizer)
        bert_multi_tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-uncased')
        bert_multi_model = BertModel.from_pre3trained('bert-base-multilingual-uncased')
        fr_emb = VectorEmbeddings.from_bert(bert_multi_model, bert_multi_tokenizer)

    model, optim = create_model(en_emb, fr_emb, hs, lr, attn_heads=args.heads)
    if os.path.exists(checkpoint_file):
        checkpoint = torch.load(checkpoint_file)
        try:
            model.load_state_dict(checkpoint['model_state_dict'])
            epochs[0] += checkpoint['epoch']
            epochs[1] += checkpoint['epoch']
            best_bleu = checkpoint['best_bleu']
        except:
            model.load_state_dict(checkpoint)
        print('load saved model from '+checkpoint_file)
        log_file = checkpoint_file.replace('models','logs').replace('saved_model','log').replace('.pt','.txt')
        print('record log to '+log_file)
        log = open(log_file, 'a')
    else:
        print('save new model to '+checkpoint_file)
        log_file = log_file[:-4]+name+log_file[-4:]
        print('record log to '+log_file)
        log = open(log_file, 'w')
    log.write(name+'\n\n')

    if args.data_type == 'mtnt':
        train_pairs, test_pairs = load_mtnt(mtnt_train, noise_probability = args.noise), \
                                  load_mtnt(mtnt_test, noise_probability = args.noise)
    elif args.data_type == 'wmt':
        train_pairs, test_pairs = load_WMT(wmt_train, max_pairs=args.max_pairs, noise_probability = args.noise), \
                                  load_WMT(wmt_test, noise_probability = args.noise)
    else:
        print('load ted talk dataset')
        train_pairs, test_pairs = load_ted(ted_train, max_pairs = args.max_pairs, noise_probability = args.noise), \
                                  load_ted(ted_test, noise_probability = args.noise)

    if args.model_type == 'w2v':
        train_batches = batch_seqs(train_pairs, args.bs, make_batch_w2v, en_emb.voc, fr_emb.voc)
        test_batches = batch_seqs(test_pairs, args.tbs, make_batch_w2v, en_emb.voc, fr_emb.voc)
    else:
        train_batches = batch_seqs(train_pairs, args.bs, make_batch, en_emb.voc, fr_emb.voc, en_ss, args.k)
        test_batches = batch_seqs(test_pairs, args.tbs, make_batch, en_emb.voc, fr_emb.voc, en_ss, args.k)

    gpu_count = torch.cuda.device_count()
    if args.parallel and gpu_count > 1:
        model = DataParallel(model, device_ids=range(gpu_count))

    print("Training...")
    best_metrics, loss_lst = train_translate(model, optim, train_batches, test_batches, epochs,
                                             save_checkpoint=checkpoint_file, log_file=log, best_bleu=best_bleu,
                                             parallel=args.parallel)
    #print('lowest loss: ' + str(best_val))
    print('best BLEU: ' + str(best_metrics))
    log.write('Training Done\nBest BLEU score is:{}'.format(str(best_metrics)))
    log.close()

    print("Ploting...")
    plt.plot(loss_lst)
    plt.xlabel('batch')
    plt.ylabel('loss')
    plt.savefig('translation/training_plot'+name+'.png')
    print("done!")

