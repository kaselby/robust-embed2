import torch
import torch.nn as nn
from torch.distributions import Categorical, Distribution, MultivariateNormal

import numpy as np

import itertools
from collections import Counter

from utils import *
from preprocess import vocab_dicts, cluster_contexts, Vocab, voc_from_bert, make_batch, voc_from_b2v, normalize_costs
from likelihood import W2VLikelihood, BertLikelihood, BertLikelihoodNaive, BertLikelihood2, W2VBertLikelihood, W2VLikelihoodNaive, W2VBertLikelihoodNaive

#from levenshtein import Graph


UNK = "<UNK>"

'''
class Vocab:
    @classmethod
    def from_corpus(cls, corpus, vocab_size=-1):
        w2c = Counter(itertools.chain.from_iterable(corpus))
        return cls.from_dict(w2c, vocab_size=vocab_size)

    @classmethod
    def from_dict(cls, w2c, vocab_size=-1):
        sorted_list = sorted(w2c, reverse=True)
        if vocab_size > 0:
            w2c = {k: w2c[k] for k in sorted_list[:vocab_size]}
        total_count = sum(list(w2c.values()))
        w2p = {word: w2c[word] / float(total_count) for word in w2c}

        w2i = {w: idx for (idx, w) in enumerate(w2c)}
        i2w = {idx: w for (idx, w) in enumerate(w2c)}
        return cls(w2c, w2i, i2w, w2p)

    def __init__(self, w2c, w2i, i2w, w2p):
        self.w2c = w2c
        self.w2i = w2i
        self.i2w = i2w
        self.w2p = w2p

    def to_index(self, w):
        return self.w2i[w] if w in self.w2i else -1

    def to_word(self, i):
        return self.i2w[i] if i in self.i2w else UNK

    def __contains__(self, w):
        return w in self.w2i
'''



def softmax_fct(tau):
    def sigma(x, dim=None):
        xmax = torch.max(x, dim=dim)
        scaled = (x - xmax) / tau
        exps = torch.exp(scaled)
        return exps/torch.sum(exps, dim=dim)
    return sigma

'''
class GaussianMixtureModel():
    def __init__(self, weights, means, Sigma):
        assert means.size(1) == Sigma.size(0)
        self.means = means
        self.Sigma = Sigma
        self.dim = Sigma.size(0)

        self.weights = weights
        self.mixing_distribution = Categorical(self.weights)

    def sample(self, *size):
        components = self.mixing_distribution.sample(*size)
        sampled_means = self.means[components,:]
        xi = torch.randn((*size, self.dim))
        return sampled_means + xi * self.Sigma
'''


class BootstrapSampler():
    def __init__(self, sample_fct, loglik, tau):
        self.sample_fct = sample_fct
        self.loglik = loglik
        self.tau=tau

    def sample(self, x, n=10, m=1, iters=1, train=False, **kwargs):
        theta = self.sample_fct((m,n))              # m x n x bs x l x d
        theta_perm = theta.permute(0, 2, 3, 4, 1)  # m x bs x l x d x n
        x_curr = x

        for i in range(iters):
            log_probs = self.loglik(x_curr, theta, **kwargs)
            log_probs_perm = log_probs.permute(0,2,3,4,1)    # m x bs x l x 1 x n
            #TEST:
            log_probs_norm = log_probs_perm - torch.logsumexp(log_probs_perm, dim=-1, keepdim=True)

            if train:
                indices = gumbel_softmax(log_probs_norm, self.tau)
                theta_out = theta_perm.view(*theta_perm.size()[:-1], 1, -1).matmul(indices.unsqueeze(-1)).squeeze(-1).squeeze(-1)
            else:
                dist = Categorical(logits=log_probs_norm)
                indices = dist.sample()
                theta_out = torch.gather(theta_perm, -1, indices.unsqueeze(-1).expand(*(theta_perm.size()[:-1]), 1)).squeeze(-1)
            x_curr = theta_out[0]
        return x_curr





class RobustEmbeddings(nn.Module):
    @staticmethod
    def covariance(X):
        X_centered = X - X.mean(dim=1, keepdim=True)
        return X_centered.t().mm(X_centered).diag() / X.size(0)

    @classmethod
    def from_w2v(cls, w2v, ss=None, S=None, tau_sample=0.6, tau_cost=1, max_vocab=-1, **kwargs):
        w2i, i2w = vocab_dicts(w2v, max_vocab=max_vocab)
        if S is None:
            S = cls.covariance(torch.Tensor(w2v.wv.vectors)) / 10   # just testing to see what reasonable values are
        W, C = torch.Tensor(w2v.wv.vectors), torch.Tensor(w2v.trainables.syn1neg)
        if max_vocab > 0:
            W, C = W[:max_vocab, :], C[:max_vocab, :]
        voc = Vocab(i2w, w2i, vecs=W, C=C)
        loglik = W2VLikelihood(voc.C, voc.vecs)
        return cls(voc, loglik, S, torch.Tensor([tau_sample]), torch.Tensor([tau_cost]), **kwargs)

    @classmethod
    def from_bert(cls, bert_model, bert_tokenizer, S=None, tau_sample=0.6, tau_cost=1, max_vocab=-1, **kwargs):
        emb = bert_model.bert.embeddings.word_embeddings.weight
        if S is None:
            S = cls.covariance(emb) / 10
        voc = voc_from_bert(bert_model.bert, bert_tokenizer)
        loglik = BertLikelihood(bert_model, bert_tokenizer)
        return cls(voc, loglik, S, torch.Tensor([tau_sample]), torch.Tensor([tau_cost]), **kwargs)

    @classmethod
    def from_w2v_bert(cls, bert_model, bert_tokenizer, w2v, S=None, tau_sample=0.6, tau_cost=1, max_vocab=-1, **kwargs):
        w2i, i2w = vocab_dicts(w2v, max_vocab=max_vocab)
        emb = torch.Tensor(w2v.wv.vectors)
        if S is None:
            S = cls.covariance(emb) / 10
        voc = Vocab(i2w, w2i, vecs=emb)
        loglik = W2VBertLikelihood(bert_model, bert_tokenizer, w2v)
        return cls(voc, loglik, S, torch.Tensor([tau_sample]), torch.Tensor([tau_cost]), **kwargs)

    '''
    @classmethod
    def from_vocab(cls, W, C, vocab, k):
        g = Graph(vocab)
        neighbours, dists = g.build_topk(k)
        return cls(W, C, g.i2t, g.t2i, neighbours, dists)
    '''

    def __init__(self, vocab, likelihood, Sigma, tau_sample, tau_cost, **kwargs):
        super().__init__()
        '''
        if clusters > 0:
            indices, centers = cluster_contexts(C.numpy(), clusters)
            _, self.cluster_weights = torch.Tensor(indices).unique(return_counts=True)
            self.cluster_centers = torch.Tensor(centers)
        '''

        self.voc = vocab
        self.dim = vocab.vecs.size(1)
        self.likelihood = likelihood
        if use_cuda:
            # self.W = self.W.cuda()
            # self.C = self.C.cuda()
            self.Sigma = nn.Parameter(torch.FloatTensor(self.dim).cuda())
            self.tau_sample = nn.Parameter(torch.FloatTensor(1).cuda())
            self.tau_cost = nn.Parameter(torch.FloatTensor(1).cuda())
            with torch.no_grad():
                self.Sigma.copy_(Sigma.cuda() * 0)  # change back later
                self.tau_sample.copy_(tau_sample.cuda())
                self.tau_cost.copy_(tau_cost.cuda())
        else:
            self.Sigma = nn.Parameter(torch.FloatTensor(self.dim))
            self.tau_sample = nn.Parameter(torch.FloatTensor(1))
            self.tau_cost = nn.Parameter(torch.FloatTensor(1))
            with torch.no_grad():
                self.Sigma.copy_(Sigma * 0)  # change back later
                self.tau_sample.copy_(tau_sample)
                self.tau_cost.copy_(tau_cost)

        self.tau_sample.requires_grad = False
        self.tau_cost.requires_grad = False

    def _sample_prior(self, neighbours, costs, oov_mask, sizes, mask=None):
        assert neighbours.size() == costs.size()
        bs, l, k = neighbours.size()
        categorical = Categorical(costs.contiguous())
        components = categorical.sample(sizes)                                                      # m x n x bs x l
        neighbours_expanded = neighbours.view(1, 1, bs, l, k).expand(*sizes, bs, l, k)              # m x n x bs x l x k
        indices = torch.gather(neighbours_expanded, -1, components.unsqueeze(-1)).squeeze(-1)       # m x n x bs x l
        means = self.voc.vecs[indices, :]                                                           # m x n x bs x l x d
        xi = torch.randn(*sizes, bs, l, self.dim)
        if use_cuda:
            xi = xi.cuda()
        w = means + xi * self.Sigma * oov_mask.view(1,1,bs,l,1).expand_as(xi)                       # m x n x bs x l x d
        if mask is not None:
            w *= mask.view(1,1,bs,l,1).expand_as(w).float()
        return w

    def _eval_prior(self, vectors, neighbours, costs):
        # vectors should be n x d, neighbours and costs should be n x k
        assert neighbours.size() == costs.size()
        n, k = neighbours.size()

        means = self.voc.vecs[neighbours]   # n x k x d
        normals = MultivariateNormal(means, torch.diag(self.Sigma))
        mvn_logprobs = normals.log_prob(vectors.unsqueeze(1))   # n x k
        mixture_logprobs = weighted_logsumexp(mvn_logprobs, costs)

        return mixture_logprobs

    def topk_nn(self, v, k=5):
        dists = cosine_dist(v, self.voc.vecs)
        #dists = (v / v.norm(dim=-1)).matmul((self.voc.vecs / self.voc.vecs.norm(dim=-1, keepdim=True)).t())
        _, i = dists.topk(k, dim=-1)
        return i

    def indices_to_words(self, I):
        if len(I.size()) > 1:
            return [self.indices_to_words(I[j]) for j in range(I.size(0))]
        else:
            return [self.voc.i2t[x.item()] for x in I]

    def get_nearest(self, e, k=5):
        I = self.topk_nn(e, k)
        return self.indices_to_words(I.t())

    '''
    def forward(self, batch, n=20, m=1, mask=None):
        neighbours, costs = batch   # both bs x l x k
        def sample_prior(sizes):
            return self._sample_prior(neighbours, costs, sizes, mask=mask)
        sampler = BootstrapSampler(sample_prior, self._loglikelihood, self.tau)

        contexts = self.C[neighbours[:, :, 0], :].contiguous()       # bs x l x d

        #import pdb;
        #pdb.set_trace()
        return sampler.sample(contexts, n=n, m=m).squeeze(0)       # 1 x bs x l x d
    '''

    def forward(self, batch, n=20, m=1, mask=None, gibbs_iters=1, trivial=False, train=False):
        neighbours, costs, oov_mask = batch   # all bs x l x k
        if trivial:
            costs = torch.zeros_like(costs)
            costs[:,:,0] = 1
        else:
            costs = normalize_costs(costs, tau=self.tau_cost)
        def sample_prior(sizes):
            return self._sample_prior(neighbours, costs, oov_mask, sizes, mask=mask)
        sampler = BootstrapSampler(sample_prior, self.likelihood, self.tau_sample)

        contexts = neighbours[:, :, 0]       # bs x l
        vector_context = False
        if gibbs_iters > 1:
            contexts = self.voc.vecs[contexts]
            vector_context = True

        #import pdb;
        #pdb.set_trace()
        samples = sampler.sample(contexts, n=n, m=m, oov_mask=oov_mask, iters=gibbs_iters, vector_input=vector_context, train=train).squeeze(0)      # bs x l x d
        return samples

    def embed(self, seq, n=20, k=5):
        # embed single sequence
        b, l = make_batch([seq], self.voc, self.ss, k)
        return self.forward(b, n=n)

    def analyze_seq(self, seq, ss, pivot, n=20, m=1, k=10, width=5,  trivial=False, **kwargs):
        b, l = make_batch([seq], self.voc, ss, k, **kwargs)
        neighbours, costs, oov_mask = b
        if trivial:
            costs = torch.zeros_like(costs)
            costs[:, :, 0] = 1
        else:
            costs = normalize_costs(costs, tau=self.tau_cost)
        contexts = neighbours[:, :, 0]

        prior_samples = self._sample_prior(neighbours, costs, oov_mask, (m,n))      # m x n x bs x l x d
        prior_knearest = self.topk_nn(prior_samples, k)

        likelihood_logprobs, likelihood_knearest = self.likelihood.maximum_likelihood(contexts, k=k, return_logprobs=True)

        log_probs = self.likelihood(contexts, prior_samples)
        permuted_samples, permuted_logprobs = prior_samples.permute(0, 2, 3, 4, 1), log_probs.permute(0, 2, 3, 4, 1)

        indices = gumbel_softmax(permuted_logprobs, self.tau_sample)
        posterior_samples = permuted_samples.view(*permuted_samples.size()[:-1], 1, -1).matmul(indices.unsqueeze(-1)).squeeze(-1).squeeze(-1)   # m x bs x l x d

        posterior_knearest = self.topk_nn(posterior_samples, k)

        prior_vecs = prior_samples[:,0,0,pivot,:]       # m x d
        lik_vecs = self.voc.vecs[likelihood_knearest][0,pivot,:,:]  # k x d
        post_vecs = posterior_samples[:,0,pivot,:]
        pivot_neighbours, pivot_costs = neighbours[:, pivot, :], costs[:, pivot, :]
        prior_prior = self._eval_prior(prior_vecs, pivot_neighbours, pivot_costs)      # m
        prior_lik = log_probs[:,0,0,pivot,0]
        prior_post = prior_prior + prior_lik
        lik_prior = self._eval_prior(lik_vecs, pivot_neighbours, pivot_costs)
        lik_lik = likelihood_logprobs[0,pivot,:]
        lik_post = lik_prior + lik_lik
        post_prior = self._eval_prior(post_vecs, pivot_neighbours, pivot_costs)
        post_lik = torch.gather(permuted_logprobs, -1, indices.argmax(dim=-1, keepdim=True))[:,0,pivot,0,0]
        post_post = post_prior + post_lik

        return posterior_samples, (prior_knearest[:,0,0,pivot,:], likelihood_knearest[0,pivot,:], posterior_knearest[:,0,pivot,:]), \
               ((prior_prior, prior_lik, prior_post),(lik_prior, lik_lik, lik_post),(post_prior, post_lik, post_post))




class RobustEmbeddingsNaive(nn.Module):
    @staticmethod
    def covariance(X):
        X_centered = X - X.mean(dim=1, keepdim=True)
        return X_centered.t().mm(X_centered).diag() / X.size(0)

    @classmethod
    def from_w2v(cls, w2v, tau=1, max_vocab=-1, **kwargs):
        w2i, i2w = vocab_dicts(w2v, max_vocab=max_vocab)
        W, C = torch.Tensor(w2v.wv.vectors), torch.Tensor(w2v.trainables.syn1neg)
        if max_vocab > 0:
            W, C = W[:max_vocab, :], C[:max_vocab, :]
        voc = Vocab(i2w, w2i, vecs=W, C=C)
        loglik = W2VLikelihoodNaive(voc.C, voc.vecs)
        return cls(voc, loglik, torch.Tensor([tau]), **kwargs)

    @classmethod
    def from_bert(cls, bert_model, bert_tokenizer, tau=1, max_vocab=-1, **kwargs):
        voc = voc_from_bert(bert_model.bert, bert_tokenizer)
        loglik = BertLikelihoodNaive(bert_model.bert, bert_model.cls, bert_tokenizer)
        return cls(voc, loglik,  torch.Tensor([tau]), **kwargs)

    @classmethod
    def from_w2v_bert(cls, bert_model, bert_tokenizer, w2v, tau=1, max_vocab=-1, **kwargs):
        w2i, i2w = vocab_dicts(w2v, max_vocab=max_vocab)
        emb = torch.Tensor(w2v.wv.vectors)
        in_voc = voc_from_bert(bert_model.bert, bert_tokenizer)
        out_voc = Vocab(i2w, w2i, vecs=emb)
        loglik = BertLikelihoodNaive(bert_model.bert, bert_model.cls, bert_tokenizer)
        index_dict = make_perm_from_dict(bert_tokenizer.ids_to_tokens, w2i, out_voc.unk_index)
        return cls(in_voc, loglik, torch.Tensor([tau]), out_voc=out_voc, index_dict=index_dict, **kwargs)

    def __init__(self, vocab, likelihood, tau, out_voc=None, index_dict=None, **kwargs):
        super().__init__()

        self.voc = vocab
        self.dim = vocab.vecs.size(1)
        self.likelihood = likelihood
        if use_cuda:
            # self.W = self.W.cuda()
            # self.C = self.C.cuda()
            self.tau_cost = nn.Parameter(torch.FloatTensor(1).cuda())
            with torch.no_grad():
                self.tau_cost.copy_(tau.cuda())
        else:
            self.tau_cost = nn.Parameter(torch.FloatTensor(1))
            with torch.no_grad():
                self.tau_cost.copy_(tau)

        self.tau_cost.requires_grad = False

        self.out_voc = out_voc
        self.index_dict = index_dict

    def forward(self, batch, gibbs_iters=1, N=20, m=None, exact_posterior=True, max_posterior=True, return_indices=False, trivial=False):
        if m is not None:
            max_posterior = False

        neighbours, costs, _ = batch                                               # bs x l x k
        costs = normalize_costs(costs, tau=self.tau_cost)
        neighbours_permuted, costs_permuted = neighbours.permute(2,0,1), costs.permute(2,0,1)
        contexts = neighbours[:, :, 0]
        if not trivial:
            gibbs_iters = 1 if gibbs_iters <= 0 else gibbs_iters
            for i in range(gibbs_iters):
                if not exact_posterior:
                    prior = Categorical(probs=costs)
                    prior_indices = prior.sample([N])       # should be bs x l x N
                    prior_samples = torch.gather(neighbours_permuted, 0, prior_indices)
                    logliks = self.likelihood(contexts, prior_samples)  # N x bs x l x 1
                    logliks_unpermuted = logliks.permute(1,2,0)
                    dist = Categorical(logits=logliks_unpermuted)
                    posterior_indices = dist.sample()
                    vocab_indices = torch.gather(prior_samples, 0, posterior_indices.unsqueeze(0)).squeeze(0)
                else:
                    logliks = self.likelihood(contexts, neighbours_permuted)    # k x bs x l x 1
                    posterior_logits = logliks + torch.log(costs_permuted)
                    posterior_logits_unpermuted = posterior_logits.permute(1, 2, 0)  # bs x l x k
                    if max_posterior:
                        indices = posterior_logits_unpermuted.argmax(dim=-1)
                        vocab_indices = torch.gather(neighbours, -1, indices.unsqueeze(-1)).squeeze(-1)
                    else:
                        posterior = Categorical(logits=posterior_logits_unpermuted)
                        if m is not None:
                            indices = posterior.sample([m])  # m x bs x l
                            vocab_indices = torch.gather(neighbours_permuted, 0, indices)
                        else:
                            indices = posterior.sample()  # bs x l
                            vocab_indices = torch.gather(neighbours, -1, indices.unsqueeze(-1)).squeeze(-1)
                contexts = vocab_indices
        else:
            vocab_indices = contexts
        if return_indices:
            return vocab_indices
        else:
            if self.out_voc is not None:
                converted_indices = self.index_dict[vocab_indices]
                return self.out_voc.vecs[converted_indices]
            else:
                return self.voc.vecs[vocab_indices]

    def analyze_seq(self, seq, ss, k=10, **kwargs):
        b, l = make_batch([seq], self.voc, ss, k, **kwargs)
        neighbours, costs, oov_mask = b
        costs = normalize_costs(costs, tau=self.tau_cost)
        neighbours_permuted, costs_permuted = neighbours.permute(2, 0, 1), costs.permute(2, 0, 1)
        contexts = neighbours[:, :, 0]

        logliks = self.likelihood(contexts, neighbours_permuted)  # k x bs x l
        logliks_unpermuted = logliks.permute(1,2,0)
        posterior_logits = logliks_unpermuted + torch.log(costs)

        return neighbours, (torch.log(costs), logliks_unpermuted, posterior_logits)

    def indices_to_words(self, I):
        if len(I.size()) > 1:
            return [self.indices_to_words(I[j]) for j in range(I.size(0))]
        else:
            return [self.voc.i2t[x.item()] for x in I]

class VectorEmbeddings(nn.Module):
    @classmethod
    def from_bert(cls, bert_model, bert_tokenizer, **kwargs):
        voc = voc_from_bert(bert_model, bert_tokenizer)
        return cls(voc)

    @classmethod
    def from_w2v(cls, w2v, max_vocab=-1, **kwargs):
        w2i, i2w = vocab_dicts(w2v, max_vocab=max_vocab)
        W = torch.Tensor(w2v.wv.vectors)
        if max_vocab > 0:
            W = W[:max_vocab,:]
        voc = Vocab(i2w, w2i, vecs=W, **kwargs)
        return cls(voc)

    @classmethod
    def from_fasttext(cls, voc, **kwargs):
        v = Vocab(dict(enumerate(voc.itos)), voc.stoi, vecs = voc.vectors)
        return cls(v, **kwargs)

    @classmethod
    def from_bridge2vec(cls, b2v, max_vocab=-1, **kwargs):
        voc = voc_from_b2v(b2v, max_vocab=max_vocab, **kwargs)
        return cls(voc)

    def __init__(self, vocab):
        super().__init__()
        self.voc = vocab
        self.dim = vocab.vecs.size(1)

    def forward(self, indices):
        return self.voc.vecs[indices, :]


