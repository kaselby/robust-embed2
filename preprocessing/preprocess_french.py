#Code to preprocess the news dataset

import itertools
import re
import nltk
import io
import os
from collections import Counter
import random, math
import numpy as np
from string import punctuation
import unicodedata
import operator
import collections
from unidecode import unidecode
import sys
import csv

csv.field_size_limit(sys.maxsize)


# Turn a Unicode string to ASCII
def unicodeToAscii(s):
    return ''.join(
        c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn')


def process_text(s):
    s = unicodeToAscii(s.lower().strip())
    s = re.sub(' +', ' ', s)
    s = s.strip()
    return s


def normalize_string_csv(s, ignore_apost = False):
    s = unidecode(s)
    s = s.lower().strip()
    s = re.sub('\!+', '!', s)
    s = re.sub('\,+', ',', s)
    s = re.sub('\?+', '?', s)
    s = re.sub('\.+', '.', s)
    # put spaces around punctuation
    for p in punctuation:
        if p in s and p != "'":
            s = s.replace(p, " " + p + " ")
    s = re.sub(' +', ' ', s)

    # separate number and following word
    match = re.search('\d , \d', s)
    while match is not None:
        s = s[:match.start() + 1] + ',' + s[match.end() - 1:]
        match = re.search('\d , \d', s)
    match = re.search('\d . \d', s)
    while match is not None:
        s = s[:match.start() + 1] + ',' + s[match.end() - 1:]
        match = re.search('\d . \d', s)
    match = re.search('\d[^\d\,\.\ ]', s)
    while match is not None:
        s = s[:match.start()+1] + ' ' + s[match.end():]
        match = re.search('\d[^\d\,\.\ ]', s)
    # process apostrophes in French (chop before)
    if ignore_apost:
        match = re.search('\w+\'+', s)
        while match is not None:
            s = s[:match.start()] + s[match.end():]
            match = re.search('\w+\'+', s)
    s = s.strip()
    return s


def filter_csv(input_paths, output_path):
    total_count = 0
    with open(output_path, 'w', encoding = 'utf-8') as out_f:
        for input_path in input_paths:
            with open(input_path, 'r', encoding = 'utf-8') as in_f:
                csv_reader = csv.reader(in_f, delimiter=',')
                print("Currently filtering and processing text from {}".format(input_path))
                header = next(csv_reader)
                print(f'Column names are {", ".join(header)}')
                col_num = len(header)
                line_count = 0
                for line in csv_reader:
                    empty_line = True
                    if line_count % 10000 == 0:
                        print("Read in {%d} lines" % line_count)
                        print("Current line: ", line[2])
                    for i in range(11, col_num):
                        if line[i] != '':
                            empty_line = False
                            clean_line = re.sub('\s+', ' ', line[i]).strip()
                            clean_line = normalize_string_csv(clean_line)
                            if clean_line != '':
                                out_f.write(clean_line + '\n')
                    if not empty_line:
                        line_count += 1
                total_count += line_count
            in_f.close()
    out_f.close()
    return total_count


def get_vocab(text_file, vocab_file, modified_output_file, cap):
    counter = 0
    word_dict = {}
    with open(text_file, 'r') as in_f:
        print("reading lines from file ...")
        for line in in_f:
            if counter % 10000 == 0:
                print("Read in {%d} lines" % counter)
                print("Current line: ", line)
            words = line.split()
            for word in words:
                if word not in word_dict:
                    word_dict[word] = 1
                else:
                    word_dict[word] += 1
            counter += 1
    print("sorting dictionary ...")
    sorted_word_list = sorted(word_dict.items(), key=operator.itemgetter(1), reverse = True)
    sorted_word_list = sorted_word_list[:cap]
    sorted_word_dict = collections.OrderedDict(sorted_word_list)
    keys = sorted_word_dict.keys()
    with open(vocab_file, 'w+', encoding = 'utf-8') as out_f:
        print("writing lines to file ...")
        for word in keys:
            out_f.write(word + ' ' + str(sorted_word_dict[word]) + '\n')
    print("lines written to file")

    # cut off vocab at cap and substitute using the unknown token
    counter = 0
    unknownToken = '<ukt>'
    with open(text_file, 'r') as text_f:
        print("reading lines from file ...")
        with open(modified_output_file, 'w+') as output_f:
            for line in text_f:
                if counter % 10000 == 0:
                    print("Processing in {%d} lines" % counter)
                    print("Current line: ", line)
                newLine = ''
                words = line.split()
                for word in words:
                    if word not in sorted_word_dict:
                        newLine += unknownToken
                    else:
                        newLine += word
                    newLine += ' '
                newLine = newLine[:-1] + '\n'
                output_f.write(newLine)
                counter += 1
        output_f.close()
    text_f.close()
    return sorted_word_dict


input_paths = ['../data/DEMOCRATIE_ET_CITOYENNETE.csv', '../data/LA_TRANSITION_ECOLOGIQUE.csv', '../data/LA_FISCALITE_ET_LES_DEPENSES_PUBLIQUES.csv', '../data/ORGANISATION_DE_LETAT_ET_DES_SERVICES_PUBLICS.csv']
output_path = 'grand_debate.txt'
total_count = filter_csv(input_paths, output_path)
print("Final total count of news articles: ", total_count)

vocab = get_vocab('grand_debate.txt', 'french_vocab_symspell.txt', 'grand_debate_modified.txt', 50000)
print(len(vocab))
#normalize_string_csv("that's 300euros")
