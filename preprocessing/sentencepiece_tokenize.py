# sentencepiece tokenization code (ended up not using this tokenization)

import io, json, os, collections, pprint, time
import re
from string import punctuation
import unicodedata
import random
import linecache
import heapq

#% pip install sentencepiece
import sentencepiece as spm


def read_vocab(file):
  with open(file) as vocab_f:
    vocab_list = []
    for line in vocab_f:
      word = line.split('\t')[0]
      vocab_list.append(word)
  return vocab_list


def encode_text(input_path, output_path):
    counter = 0
    encoded_list = []
    with open(input_path, 'r') as in_f, open(output_path, 'w+', encoding = 'utf-8') as out_f:
        print("Currently encoding lines from file ...")
        for line in in_f:
            if counter % 100000 == 0:
                print("Read in {%d} lines" % counter)
                print("Current line: ", line)
            bpe_line = sp_bpe.encode_as_pieces(line)
            encoded_line = ' '.join(bpe_line) + '\n'
            out_f.write(encoded_line)
            encoded_list.append(encoded_line)
            counter += 1
        print("Final line count: ", counter)
        out_f.close()
    out_f.close()
    return encoded_list


spm.SentencePieceTrainer.train('--input=reddit_dataset_10m.txt --model_prefix=reddit_10m_bpe12k --vocab_size=12000 --model_type=bpe')

sp_bpe = spm.SentencePieceProcessor()
sp_bpe.load('reddit_10m_bpe12k.model')


vocab_list = read_vocab('reddit_10m_bpe12k.vocab')
print("Vocab list: ", vocab_list)
print("Length of vocab list: ", len(vocab_list))

print('\n*** BPE ***')
print(sp_bpe.encode_as_pieces('thisisatesthelloworld'))
print(sp_bpe.encode_as_pieces('this is a test'))
print(sp_bpe.nbest_encode_as_pieces('hello world', 5))  # returns an empty list.
print(' '.join(sp_bpe.encode_as_pieces('thisisatesthelloworld')) + '\n')


input_path = 'reddit_dataset_10m.txt'
output_path = 'reddit_dataset_10m_bpe_12k.txt'

encoded_reddit_lines = encode_text(input_path, output_path)
print(len(encoded_reddit_lines))
print(encoded_reddit_lines[:100])