# Code to train word2vec embeddings

from string import punctuation
import gensim
from gensim.models.word2vec import Word2Vec
import nltk, gensim
from itertools import islice
import io, json, os, collections, pprint, time
import re
from string import punctuation
import unicodedata
import random
import heapq
import random


class FileToSent(object):    
    def __init__(self, filename, start, end):
        self.filename = filename
        self.start = start
        self.end = end

    def __iter__(self):
        print("Reading lines...")
        counter = self.start
        for line in islice(open(self.filename, 'r'), self.start, self.end):
            if counter % 10000 == 0:
                print("Read in {%d} lines" % counter)
                print(line)
            ll = [i for i in line.split()]
            counter += 1
            yield ll
      
      
def build_model(sentences, path):
    model = Word2Vec(
      sentences,
      size=100,  #50, 20
      window=8,  #5, 10
      min_count=10, #20
      workers=3,
      sg=1,
      negative=8)
    model.save(path)
    return model


news_sentences = FileToSent('news_articles_modified.txt', 0, 1000000000)
print("\n\nTraining Word2Vec model: ")
news_w2v_model = build_model(news_sentences, "news_word2vec_model.w2v")
