

def make_vocab_file(tokenizer, vocab_file):
    words = [v for v in tokenizer.ids_to_tokens.values()]
    with open(vocab_file, 'w+', encoding = 'utf-8') as out_f:
        print("writing lines to file ...")
        for word in words[999:]:
            out_f.write(word + ' ' + str(1) + '\n')
    print("lines written to file")