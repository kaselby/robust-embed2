"""
from gensim.models import Word2Vec, KeyedVectors

w2v = KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True, limit = 50000)
vocab_file = 'google_w2v_vocab.txt'
f = open(vocab_file, 'w+')
for word, vocab in w2v.vocab.items():
    f.write(word + ' ' + str(vocab.count) + '\n')
f.close()
"""

from transformers import BertModel, BertTokenizer
from bayes_lev_embed import VectorEmbeddings

file = "../data/TinyBERT_4_312"

bert_tokenizer = BertTokenizer.from_pretrained(file)
bert_model = BertModel.from_pretrained(file)
emb = VectorEmbeddings.from_bert(bert_model, bert_tokenizer)

vocab_file = '../data/tinybert_vocab_symspell.txt'
f = open(vocab_file, 'w+')
for key in emb.voc.t2i.keys():
    f.write(key + ' 1\n')
f.close()


