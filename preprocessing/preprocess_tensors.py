# Code to precalculate the levenshtein distance tensors beforehand

import torch
import gensim
from gensim.models.word2vec import Word2Vec

from symspellpy import SymSpell, Verbosity
import torch
import torch.nn.functional as F


def read_vocab_file(file):
    with open(file) as vocab_f:
        vocab_list = {}
        for line in vocab_f:
            word, count = line.split()
            vocab_list[word] = count
    return vocab_list


def load_vocab(vocab_file, w2v_file, max_edit_dist=3):
    w2v = Word2Vec.load(w2v_file)

    sym_spell = SymSpell(max_edit_dist)
    sym_spell.load_dictionary(vocab_file, 0, 1)

    return w2v, sym_spell


def _tensorize(word, vocab, tree, k, **kwargs):
    neighbours_ij = tree.lookup(word, Verbosity.ALL, **kwargs)
    #for suggestion in neighbours_ij:
    #  print("{}, {}, {}".format(suggestion.term, suggestion.distance,
    #                            suggestion.count))
    indices_ij = torch.LongTensor([w2v.wv.vocab[item.term].index for item in neighbours_ij])[:k]
    dists_ij = torch.FloatTensor([item.distance for item in neighbours_ij])[:k]
    return indices_ij, dists_ij


def _initialize(vocab_len, k):
    neighbours_matrix = (torch.ones(vocab_len, k) * -1).long()
    dists_matrix = (torch.ones(vocab_len, k) * -1).long()
    return neighbours_matrix, dists_matrix


w2v_file = "french_word2vec_model.w2v"
vocab_file = "french_vocab_symspell.txt"
w2v, sym_spell = load_vocab(vocab_file, w2v_file)
vocab = w2v.wv.vocab
vocab_len = len(vocab)
print(vocab_len)

neighbours_matrix, dists_matrix = _initialize(vocab_len, 10)
print(neighbours_matrix)
print(dists_matrix)

for i in range(0,vocab_len):
    print("current word: ", w2v.wv.index2entity[i])
    if i % 1000 == 0:
        print("Currently on word ", i)
    indices_ij, dists_ij = _tensorize(w2v.wv.index2entity[i], vocab, sym_spell, 10)
    dists_ij = dists_ij.long()
    print(indices_ij, dists_ij)
    #idx = torch.LongTensor([0,1,2,3,4,5,6,7,8,9])
    idx = torch.arange(0,len(indices_ij),1)
    neighbours_matrix[i,idx] = indices_ij
    #print(neighbours_matrix)
    dists_matrix[i,idx] = dists_ij
    #print(dists_matrix)

print(neighbours_matrix)
print(dists_matrix)

torch.save(neighbours_matrix, 'neighbours_matrix.pt')
torch.save(dists_matrix, 'dists_matrix.pt')


# Load saved tensors
neighbours_matrix = torch.load('neighbours_matrix.pt')
dists_matrix = torch.load('dists_matrix.pt')    

print(neighbours_matrix)
print(dists_matrix)