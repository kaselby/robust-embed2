# Code to extract random 10 million lines from original Reddit corpus

import io, json, os, collections, pprint, time
import re
from string import punctuation
import unicodedata
import random
import heapq


def random_lines(input_path, output_path, output_size, seed=None):
    if seed is not None:
        print("seed: ", seed)
        random.seed(seed)
    with open(input_path) as in_f:
        sample = heapq.nlargest(output_size, in_f, key=lambda L: random.random())
    with open(output_path, 'w+', encoding = 'utf-8') as out_f:
        out_f.writelines(sample)
        out_f.close()
    out_f.close()
    return sample


input_path = 'reddit_dataset_processed.txt'
output_path = 'reddit_dataset_10m.txt'
#input_size = 46626673
output_size = 10000000

output_lines = random_lines(input_path, output_path, output_size, 3)
print(len(output_lines))