# Code to preprocess reddit dataset

import itertools
import re
import nltk
import io
import os
from collections import Counter
import random, math
import numpy as np
from string import punctuation
import unicodedata
import operator


def isEnglish(s):
    try:
        s.encode(encoding='utf-8').decode('ascii')
    except UnicodeDecodeError:
        return False
    else:
        return True


# Turn a Unicode string to ASCII
def unicodeToAscii(s):
    return ''.join(
        c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn')


def process_text(s):
    s = unicodeToAscii(s.lower().strip())    
    s = re.sub(' +', ' ', s)
    s = s.strip()
    return s


def normalize_string_2(s):
    s = s.lower().strip()
    s = re.sub('\!+', '!', s)
    s = re.sub('\,+', ',', s)
    s = re.sub('\?+', '?', s)
    s = re.sub('\.+', '.', s)    
    #s = re.sub("[^0-9a-zA-Z.!?,'']+", ' ', s)
    for p in punctuation:
      if p != "'":
        s = s.replace(p, " " + p + " ")       
    s = re.sub(' +', ' ', s)
    s = s.strip()
    return s


def filter_text(input_path, output_path):
    in_counter = 0
    out_counter = 0
    with open(input_path, 'r') as in_f, open(output_path, 'w+', encoding = 'utf-8') as out_f:
        print("Currently filtering and processing text ...")
        for line in in_f:
            if in_counter % 100000 == 0:
                print("Read in {%d} lines" % in_counter)
                print("Current line: ", line)
            if line != '' and isEnglish(line) and 'http' not in line and line.strip() != '[deleted]':
                clean_line = re.sub('\s+', ' ', line).strip()
                clean_line_final = normalize_string_2(clean_line)
                if clean_line_final != '':
                    out_f.write(clean_line_final + '\n')
                    out_counter += 1
            in_counter += 1
        print("Final input line count: ", in_counter)
        print("Final output line count: ", out_counter)
        out_f.close()
    out_f.close()
    

def get_vocab(text_file, vocab_file, modified_output_file, cap):
    counter = 0
    word_dict = {}
    with open(text_file, 'r') as in_f:
        print("reading lines from file ...")
        for line in in_f:
            if counter % 10000 == 0:
                print("Read in {%d} lines" % counter)
                print("Current line: ", line)
            words = line.split()
            for word in words:
                if word not in word_dict:
                    word_dict[word] = 1
                else:
                    word_dict[word] += 1
            counter += 1
    print("sorting dictionary ...")
    sorted_word_list = sorted(word_dict.items(), key=operator.itemgetter(1), reverse = True)
    sorted_word_list = sorted_word_list[:cap]
    sorted_word_dict = collections.OrderedDict(sorted_word_list)
    keys = sorted_word_dict.keys()
    with open(vocab_file, 'w+', encoding = 'utf-8') as out_f:
        print("writing lines to file ...")
        for word in keys:
            out_f.write(word + ' ' + str(sorted_word_dict[word]) + '\n')
    print("lines written to file")

    # cut off vocab at cap and substitute using the unknown token
    counter = 0
    unknownToken = '<ukt>'
    with open(text_file, 'r') as text_f:
        print("reading lines from file ...")
        with open(modified_output_file, 'w+') as output_f:
            for line in text_f:
                if counter % 10000 == 0:
                    print("Processing in {%d} lines" % counter)
                    print("Current line: ", line)
                newLine = ''
                words = line.split()
                for word in words:
                    if word not in sorted_word_dict:
                        newLine += unknownToken
                    else:
                        newLine += word
                    newLine += ' '
                newLine = newLine[:-1] + '\n'
                output_f.write(newLine)
                counter += 1
        output_f.close()
    text_f.close()
    return sorted_word_dict


filter_text('reddit_dataset_10m.txt','reddit_dataset_10m_processed_3.txt')

vocab = get_vocab('reddit_dataset_10m_processed_2.txt','reddit_dataset_10m_processed_2_vocab.txt')
print(len(vocab))