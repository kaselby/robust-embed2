# Code to add synthetic noise to text data
# Note: WIP, not completed, probably more efficient ways of doing this

import random
import string
import re

'''
def add_letters(word, probability):
    k = len(word)
    i = 0
    while i < k:
        if random.random() < probability:
            word = word[:i] + random.choice(string.ascii_lowercase) + word[i:]
            i += 1
            k += 1
        i += 1
    if random.random() < probability:
        word = word[:len(word)] + random.choice(string.ascii_lowercase)
    return word


def remove_letters(word, probability):
    for i in range(len(word)-1):
        if random.random() < probability:
            word = word[:i] + " " + word[i+1:]
    if random.random() < probability:
        word = word[:len(word)-1]
    word = re.sub(" ", "", word)
    return word
'''



def add_letter(word):
    k = len(word)
    pos = random.randint(0,k)
    return word[:pos] + random.choice(string.ascii_lowercase) + word[pos:]


def remove_letter(word):
    k = len(word)
    pos = random.randint(0, k-1)
    return word[:pos] + word[pos+1:]


def swap_letters(word, probability):
    k = len(word)
    if k < 2:
        return word
    else:
        i = 0
        skip = False
        for i in range(k-2):
            if skip == False and random.random() < probability:
                word = word[:i] + word[i+1] + word[i] + word[i+2:]
                skip = True
            else:
                skip = False
        if skip == False and random.random() < probability:
            word = word[:k-2] + word[k-1] + word[k-2]
    return word

def add_noise_by_letter(word, probability):
    k = len(word)
    new_word = ""
    for i in range(k):
        if random.random() < probability:
            new_word = new_word + random.choice(string.ascii_lowercase)
        if random.random() > probability:
            new_word = new_word + word[i]
    if random.random() < probability:
        new_word = new_word + random.choice(string.ascii_lowercase)
    return new_word

def add_noise(word, probability):
    if random.random() < probability:
        word = remove_letter(word)
    if random.random() < probability:
        word = add_letter(word)
    return word

def add_noise_swaps(word, probability):
    word = swap_letters(word, probability)
    word = remove_letters(word, probability)
    word = add_letters(word, probability)
    return word

# copy of ROVE noise generator
def noise_generator(str, noise_level):
    noised = ""
    for c in str:
        if random.random() > noise_level:
            noised += c
        if random.random() < noise_level:
            noised += random.choice(string.ascii_lowercase)
    return noised

def process_sentence(lst: list, noise_probability, noise_fct=noise_generator, noise_loc=None) -> list:
    if noise_loc is not None:
        noisy = lst[:noise_loc] + [noise_fct(lst[noise_loc], noise_probability)] + lst[(noise_loc+1):]
    else:
        noisy = []
        for i, word in enumerate(lst):
            noisy.append(noise_fct(word, noise_probability))
    return noisy
