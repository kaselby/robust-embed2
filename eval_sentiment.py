import argparse
import os

from bayes_lev_embed import RobustEmbeddings
from sentiment.sentiment_analysis import *
from preprocess import *

#torch.backends.cudnn.deterministic=True

"""
w2v_file = "./data/reddit_10m_word_level_processed_3_word2vec_model.w2v"
vocab_file = "./data/reddit_vocab_symspell.txt"
cost_file = "data/dists_matrix.pt"
neighbour_file = "data/neighbours_matrix.pt"
msr_train = "paraphrase/MSRParaphraseCorpus/msr_paraphrase_train.txt"
msr_test = "paraphrase/MSRParaphraseCorpus/msr_paraphrase_test.txt"
twitter_train = "paraphrase/SemEval-PIT2015/train.data"
twitter_test = "paraphrase/SemEval-PIT2015/test.data"
"""
w2v_file = "./data/news_word2vec_model.w2v"
vocab_file = "./data/news_vocab_symspell.txt"
cost_file = "data/news_dists_matrix.pt"
neighbour_file = "data/news_neighbours_matrix.pt"
msr_train = "paraphrase/MSRParaphraseCorpus/msr_paraphrase_train.txt"
msr_test = "paraphrase/MSRParaphraseCorpus/msr_paraphrase_test.txt"
twitter_train = "sentiment/SemEval2017-task4/twitter-2016train-A.txt"
twitter_test = "sentiment/SemEval2017-task4/twitter-2016test-A.txt"


save_folder = "./sentiment/models/"


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--bs', help='batch size', type=int, default=32)
    parser.add_argument('--hs', help='hidden size', type=int, default=256)
    parser.add_argument('--k', help='k', type=int, default=10)
    parser.add_argument('--model_file', type=str, default=None)
    parser.add_argument('--data_type', choices=('twitter', 'msr'), type=str, default='twitter')
    parser.add_argument('--model_type', choices=('w2v', 'robust'), type=str, default='w2v')
    parser.add_argument('--noise', help = 'level of synthetic noise to inject', type = float, default = 0)

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    w2v, ss = load_vocab(vocab_file, w2v_file)
    emb = RobustEmbeddings.from_w2v(w2v)
    cost_matrix = torch.load(cost_file)
    nn_matrix = torch.load(neighbour_file)
    #train_pairs, train_labels = load_msr(msr_train)
    #test_pairs, test_labels = load_msr(msr_test)

    if args.data_type == 'twitter':
        #train_pairs, train_labels = load_twitter(twitter_train)
        test_pairs, test_labels = load_twitter(twitter_test, noise_probability = args.noise)
    else:
        raise NotImplementedError

    (oov_pairs, oov_labels), (iv_pairs, iv_labels) = split_oov_pairs(test_pairs, test_labels, emb.voc.t2i)

    emb_fct = W2VEmbeddings(emb.W) if args.model_type == 'w2v' else emb
    model, optim = make_classifier(emb.dim, args.hs, emb_fct, 1)

    outfile = os.path.join(save_folder, args.model_file + ".pt")

    model.load_state_dict(torch.load(outfile))

    if args.model_type == 'w2v':
        oov_batches = batch_seqs(oov_pairs, oov_labels, args.bs, make_batch_w2v, emb.t2i)
        iv_batches = batch_seqs(iv_pairs, iv_labels, args.bs, make_batch_w2v, emb.t2i)
        all_batches = batch_seqs(test_pairs, test_labels, args.bs, make_batch_w2v, emb.t2i)
    else:
        oov_batches = batch_seqs(oov_pairs, oov_labels, args.bs, make_batch, emb.t2i, ss, args.k,
                                 vocab_tensors=(nn_matrix, cost_matrix))
        iv_batches = batch_seqs(iv_pairs, iv_labels, args.bs, make_batch, emb.t2i, ss, args.k,
                                 vocab_tensors=(nn_matrix, cost_matrix))
        all_batches = batch_seqs(test_pairs, test_labels, args.bs, make_batch, emb.t2i, ss, args.k,
                                vocab_tensors=(nn_matrix, cost_matrix))

    oov_val, oov_acc = eval_sentiment(model, oov_batches)
    iv_val, iv_acc = eval_sentiment(model, iv_batches)
    all_val, all_acc = eval_sentiment(model, all_batches)

    print("All Validation Loss:", all_val, "All Validation Accuracy:", all_acc)
    print("OOV Validation Loss:", oov_val, "OOV Validation Accuracy:", oov_acc)
    print("IV Validation Loss:", iv_val, "IV Validation Accuracy:", iv_acc)


    score_file = os.path.join(save_folder, args.model_file + ".eval.txt")
    f = open(score_file, 'w')
    f.write("All Validation Loss:" + str(all_val) + "\tAll Validation Accuracy:" + str(all_acc))
    f.write("OOV Validation Loss:" + str(oov_val) + "\tOOV Validation Accuracy:" + str(oov_acc))
    f.write("IV Validation Loss:" + str(iv_val) + "\tIV Validation Accuracy:" + str(iv_acc))
    f.close()



