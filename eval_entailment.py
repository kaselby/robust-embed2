import argparse
import os
from transformers import BertModel, BertTokenizer, BertForMaskedLM, BertConfig

from bayes_lev_embed import RobustEmbeddings, RobustEmbeddingsNaive, VectorEmbeddings
from entailment.entailment_detection import *
from preprocess import *
from bert_model import BertForContinuousMaskedLM, BertForDensityEstimation, W2VBertForMaskedLM

#torch.backends.cudnn.deterministic=True

#w2v_file = "./data/reddit_10m_word_level_processed_3_word2vec_model.w2v"
#vocab_file = "./data/reddit_vocab_symspell.txt"
base_w2v_file = "data/clean_news_word2vec_model.w2v"
base_vocab_file = "data/clean_news_vocab_symspell.txt"
bert_vocab_file = "data/bert_vocab_symspell.txt"
tinybert_vocab_file = "data/tinybert_vocab_symspell.txt"
tinybert_dir = "data/TinyBERT_4_312"

#w2v_file = "data/GoogleNews-vectors-negative300.bin"
#vocab_file = "preprocessing/google_w2v_vocab.txt"
#cost_file = "data/news_dists_matrix.pt"
#neighbour_file = "data/news_neighbours_matrix.pt"
noisy_snli_dir = "entailment/noisy_snli"

snli_train = "entailment/snli_1.0/snli_1.0_train.jsonl"
snli_test = "entailment/snli_1.0/snli_1.0_test.jsonl"
snli_test_rove = "entailment/snli_1.0/test.txt"
google_w2v_file = "data/GoogleNews-vectors-negative300.bin.gz"
google_w2v_vocab = "data/google_vocab_symspell.txt"

w2v_bert_file='data/w2v-bert'

b2v_path = "data/b2v_model.bin"

save_folder = "./models"


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--bs', help='batch size', type=int, default=32)
    parser.add_argument('--hs', help='hidden size', type=int, default=256)
    parser.add_argument('--k', help='k', type=int, default=10)
    parser.add_argument('--m', help='m', type=int, default=5)
    parser.add_argument('--model_file', type=str, default=None)
    parser.add_argument('--data_type', choices=('snli'), type=str, default='snli')
    parser.add_argument('--model_type', choices=('w2v', 'robust', 'robust-naive', 'fasttext', 'b2v'), type=str, default='w2v')
    parser.add_argument('--likelihood', choices=('w2v', 'bert', 'w2v-bert'), type=str, default='w2v')
    parser.add_argument('--bert_model', choices=('base', 'tiny'), type=str, default='base')
    parser.add_argument('--checkpoint_file', type=str, default=None)
    parser.add_argument('--classifier', choices=('cosine', 'lstm'), type=str, default='cosine')
    parser.add_argument('--noise', help = 'level of synthetic noise to inject', type=float, default=0)
    parser.add_argument('--spellcheck', help='level of synthetic noise to inject', choices=('none', 'lev'), type=str, default='none')
    parser.add_argument('--max_vocab', help='max vocab size', type=int, default=-1)
    parser.add_argument('--max_pairs', help='max vocab size', type=int, default=-1)
    parser.add_argument('--iters', help='likelihood context iterations', type=int, default=1)
    parser.add_argument('--tau', help='cost softmax temperature', type=float, default=1)
    parser.add_argument('--variance_file', type=str, default=None)
    parser.add_argument('--train_data', type=str, default=None)
    parser.add_argument('--test_data', type=str, default=None)
    parser.add_argument('--trivial', action='store_true')
    parser.add_argument('--pregen', type=int, nargs='*', default=[])
    parser.add_argument('--ensemble', action='store_true')


    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    if args.model_type == 'w2v':
        vocab_file = google_w2v_vocab
        w2v_file = google_w2v_file
        kv = True
    else:
        if args.likelihood == 'w2v':
            kv = False
            w2v_file = base_w2v_file
            vocab_file = base_vocab_file
        else:  # args.likelihood == "bert" or args.likelihood == "w2v-bert":
            vocab_file = bert_vocab_file
            w2v_file = google_w2v_file
            kv = True

    w2v, ss = load_vocab(vocab_file, w2v_file, keyedvectors=kv)
    #cost_matrix = torch.load(cost_file)
    #nn_matrix = torch.load(neighbour_file)

    Sigma = None
    if args.variance_file is not None:
        saved_param = torch.load(args.variance_file)
        Sigma = saved_param['embed.Sigma'].abs()

    datasets=[]
    if len(args.pregen) > 0:
        for n in args.pregen:
            noise_prefix = str(args.noise) if args.noise > 0 else "0"
            snli_test = os.path.join(noisy_snli_dir, str(n), noise_prefix+"_snli_1.0_test.jsonl")
            test_pairs, test_labels = load_snli(snli_test, noise_probability=0, max_pairs=args.max_pairs)
            datasets.append((test_pairs,test_labels))
    else:
        if args.train_data is not None:
            snli_train = args.train_data
        if args.test_data is not None:
            snli_test = args.test_data
        if args.data_type == 'snli':
            #train_pairs, train_labels = load_snli(snli_train)
            test_pairs, test_labels = load_snli(snli_test, noise_probability = args.noise, max_pairs=args.max_pairs)
            #test_pairs, test_labels = load_snli_rove(snli_test_rove, noise_probability=args.noise)
            datasets.append((test_pairs,test_labels))
        else:
            raise NotImplementedError


    """
    if args.model_type == 'robust':
        emb = RobustEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab)
    elif args.model_type == 'fasttext':
        vocab_path = 'data/wiki.en.vec'
        from torchtext import vocab
        voc = vocab.Vectors(vocab_path, max_vectors = 50000)
        emb = VectorEmbeddings.from_fasttext(voc)
    else:
        emb = VectorEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab)
    """

    if args.likelihood == 'w2v':
        if args.model_type == 'w2v':
            emb = VectorEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab)
        elif args.model_type == 'fasttext':
            vocab_path = 'data/wiki.en.vec'
            from torchtext import vocab
            voc = vocab.Vectors(vocab_path, max_vectors = args.max_vocab if args.max_vocab > 0 else 30000)
            emb = VectorEmbeddings.from_fasttext(voc)
        elif args.model_type == 'b2v':
            import fasttext
            b2v = fasttext.load_model(b2v_path)
            emb = VectorEmbeddings.from_bridge2vec(b2v, max_vocab=args.max_vocab)
        elif args.model_type == 'robust-naive':
            emb = RobustEmbeddingsNaive.from_w2v(w2v, max_vocab=args.max_vocab, tau=args.tau)
        else:
            emb = RobustEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab, S=Sigma, tau_cost=args.tau)

    elif args.likelihood == 'bert':
        pretrained_bert = tinybert_dir if args.bert_model == 'tiny' else 'bert-base-uncased'
        bert_tokenizer = BertTokenizer.from_pretrained(pretrained_bert)
        if args.model_type == 'w2v':
            bert_model = BertForMaskedLM.from_pretrained(pretrained_bert)
            emb = VectorEmbeddings.from_bert(bert_model, bert_tokenizer)
        if args.model_type == 'robust-naive':
            bert_model = BertForMaskedLM.from_pretrained(pretrained_bert)
            emb = RobustEmbeddingsNaive.from_bert(bert_model, bert_tokenizer, tau=args.tau)
        else:
            bert_saved_model = BertForMaskedLM.from_pretrained(pretrained_bert)
            bert_model = BertForDensityEstimation.from_bfcmlm(bert_saved_model)
            if args.model_type == 'w2v':
                emb = VectorEmbeddings.from_bert(bert_model, bert_tokenizer)
            else:
                emb = RobustEmbeddings.from_bert(bert_model, bert_tokenizer, S=Sigma, tau_cost=args.tau)


    elif args.likelihood == 'w2v-bert':
        '''
        w2v = KeyedVectors.load_word2vec_format(google_w2v_file, binary=True, limit = 50000)
        bert_tokenizer = BertTokenizer.from_pretrained(w2v_bert_file)
        bert_config = BertConfig.from_pretrained(w2v_bert_file)
        bert_saved_model = W2VBertForMaskedLM.from_pretrained(w2v_bert_file, torch.Tensor(w2v.wv.vectors), config=bert_config)
        bert_model = BertForDensityEstimation.from_w2vbfmlm(bert_saved_model)
        '''
        pretrained_bert = tinybert_dir if args.bert_model == 'tiny' else 'bert-base-uncased'
        bert_tokenizer = BertTokenizer.from_pretrained(pretrained_bert)
        bert_model = BertForMaskedLM.from_pretrained(pretrained_bert)
        if args.model_type == 'robust-naive':
            emb = RobustEmbeddingsNaive.from_w2v_bert(bert_model,
                                                      bert_tokenizer, w2v, tau=args.tau)
        else:
            emb = RobustEmbeddings.from_w2v_bert(bert_model, bert_tokenizer, w2v, S=Sigma, tau_cost=args.tau)

    if args.spellcheck == 'lev':
        test_pairs = [(naive_spellcheck(seq, ss, emb.voc.t2i) for seq in pair) for pair in test_pairs]

    if args.classifier == 'lstm':
        model, optim = make_lstm_classifier(emb.dim, args.hs, emb, 1)
        outfile = os.path.join(save_folder, args.model_file + ".pt")
        model.load_state_dict(torch.load(outfile))
    else:
        model = CosineClassifier(emb)


    all_metrics = []
    for pairs, labels in datasets:
        MAX_LENGTH = 100
        if args.model_type == 'robust' or args.model_type == 'robust-naive':
            all_batches = ClassificationBatchGenerator(pairs, labels, emb.voc, MAX_LENGTH, ss, args.k, n_inputs=2, robust=True).make_batches(args.bs)
        else:
            all_batches = ClassificationBatchGenerator(pairs, labels, emb.voc, MAX_LENGTH, n_inputs=2,  robust=False, exclude_oov=True).make_batches(args.bs)

        trivial = args.trivial and ('robust' in args.model_type)
        if args.likelihood == "bert":
            val, metrics = eval_entailment(model, all_batches, gibbs_iters=args.iters, trivial=trivial, ensemble=args.ensemble, m=args.m)
        else:
            val, metrics = eval_entailment(model, all_batches, trivial=trivial, ensemble=args.ensemble, m=args.m)
        all_metrics.append(metrics)

        print("AUC:", metrics)

    print("Final Results:", str(all_metrics), "\tAverage:", str(sum(all_metrics)/len(all_metrics)))
