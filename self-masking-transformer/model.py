from transformers import BertModel
from transformers.modeling_bert import BertEncoder, BertLayer, BertEmbeddings, BertPooler
import torch
import torch.nn as nn

class BertSelfMaskingEncoder(BertEncoder):
    def __init__(self, config):
        super().__init__(config)

    def forward(
        self,
        hidden_states,
        attention_mask=None,
        head_mask=None,
        encoder_hidden_states=None,
        encoder_attention_mask=None,
    ):
        bs, l = hidden_states.size()[:2]
        self_mask = 1 - torch.eye(l, device=hidden_states.device)
        self_mask = self_mask[None, None, :, :]
        self_mask = self_mask.to(dtype=next(self.parameters()).dtype)  # fp16 compatibility
        self_mask = (1.0 - self_mask) * -10000.0
        modified_mask = attention_mask + self_mask

        return super().forward(
            hidden_states,
            attention_mask=modified_mask,
            head_mask=head_mask,
            encoder_hidden_states=encoder_hidden_states,
            encoder_attention_mask=encoder_attention_mask
        )

class BertSelfMaskingModel(BertModel):
    def __init__(self, config):
        super(BertModel, self).__init__(config)
        self.config = config

        self.embeddings = BertEmbeddings(config)
        self.encoder = BertSelfMaskingEncoder(config)
        self.pooler = BertPooler(config)

        self.init_weights()