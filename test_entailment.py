import argparse
import os
from transformers import BertModel, BertTokenizer, BertForMaskedLM

from bayes_lev_embed import RobustEmbeddings, VectorEmbeddings, RobustEmbeddingsNaive
from entailment.entailment_detection import *
from preprocess import *

from bert_model import BertForDensityEstimation

print("hello")

w2v_file = "data/clean_news_word2vec_model.w2v"
#vocab_file = "./data/clean_news_vocab_symspell.txt"
vocab_file = "data/bert_vocab_symspell.txt"

w2v, ss = load_vocab(vocab_file, w2v_file)


bert_tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
bert_saved_model = BertForMaskedLM.from_pretrained('bert-base-uncased')
bert_model = BertForDensityEstimation.from_bfcmlm(bert_saved_model)
emb = RobustEmbeddings.from_bert(bert_model, bert_tokenizer)


#emb = RobustEmbeddings.from_w2v(w2v, reserved=None)

model = CosineClassifier(emb)

#from preprocessing import synthetic_noise
#lambd = 0.05
#sent = "the man holding the sign of a rainbow elephant"
#normed = normalize_string(sent).split(" ")
#noisy = synthetic_noise.process_sentence(normed, 0.05)
#b1, l1 = make_batch([noisy], emb.voc, ss, 10, only_oov=False)
#sent="the dog barked loudly at the cat"
#sent="Someone I know recently combined Maple Syrup & buttered Popcorn thinking it would taste like caramel popcorn. It didn’t and they don’t recommend anyone else do it either."
#normed = normalize_string(sent).split(" ")
#out, (prior,likelihood,posterior), (prior_probs, likelihood_probs) = emb.analyze_seq(normed, ss)


snli_train = "entailment/snli_1.0/snli_1.0_train.jsonl"
snli_val = "entailment/snli_1.0/snli_1.0_dev.jsonl"
snli_test = "entailment/snli_1.0/snli_1.0_test.jsonl"
train_pairs, train_labels = load_snli(snli_train, noise_probability=0)

#test_batch_gen = EntailmentBatchGenerator(make_batch, train_pairs, train_labels, emb.voc, ss, 10, max_data_size=5000)
#test_batch_gen = EntailmentBatchGenerator.batch_robust(train_pairs, train_labels, emb.voc, ss, 35, 10, max_data_size=5000)
test_batch_gen = BatchGenerator(train_pairs, train_labels, 35, emb.voc, ss, 10, max_data_size=8, n_inputs=2, robust=True)
batches = test_batch_gen.make_batches(4)
batch1 = batches.__next__()

import tabulate
def summarize_seq(seq, pivot, emb, ss, m=5, n=20, k=5):
    posterior_samples, (prior_knearest, likelihood_knearest, posterior_knearest), (
    (pr_pr, pr_l, pr_po), (l_pr, l_l, l_po), (po_pr, po_l, po_po)) = emb.analyze_seq(seq, ss, pivot, m=m, n=n, k=k,
                                                                                     only_oov=False)
    prior_headers = ["K-Nearest Words", "Prior Probability", "Likelihood Probability", "Posterior Probability"]
    prior_candidates = emb.indices_to_words(prior_knearest)
    prior_table = [[' '.join(prior_candidates[i]), pr_pr[i].item(), pr_l[i].item(), pr_po[i].item()] for i in range(m)]

    likelihood_headers = ["Word", "Prior Probability", "Likelihood Probability", "Posterior Probability"]
    likelihood_candidates = emb.indices_to_words(likelihood_knearest)
    likelihood_table = [[likelihood_candidates[i], l_pr[i].item(), l_l[i].item(), l_po[i].item()] for i in range(m)]

    posterior_headers = ["K-Nearest Words", "Prior Probability", "Likelihood Probability", "Posterior Probability"]
    posterior_candidates = emb.indices_to_words(posterior_knearest)
    posterior_table = [[posterior_candidates[i], po_pr[i].item(), po_l[i].item(), po_po[i].item()] for i in range(m)]

    print(tabulate.tabulate(prior_table, prior_headers, tablefmt="fancy_grid"))
    print(tabulate.tabulate(likelihood_table, likelihood_headers, tablefmt="fancy_grid"))
    print(tabulate.tabulate(posterior_table, posterior_headers, tablefmt="fancy_grid"))

