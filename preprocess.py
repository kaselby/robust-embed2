import gensim
from gensim.models.word2vec import Word2Vec
from gensim.models import KeyedVectors

from symspellpy import SymSpell, Verbosity
import torch
import torch.nn.functional as F
import torch.nn as nn
import numpy as np

import tqdm

import re
from string import punctuation
from unidecode import unidecode

import copy

from sklearn.cluster import KMeans, MiniBatchKMeans

from utils import *

w2v_file = "./data/reddit_10m_word_level_processed_3_word2vec_model.w2v"
vocab_file = "./data/reddit_vocab_symspell.txt"

def read_vocab_file(file):
    with open(file) as vocab_f:
        vocab_list = {}
        for line in vocab_f:
            word, count = line.split('\t')[0]
            vocab_list[word] = count
    return vocab_list

def load_vocab(vocab_file, w2v_file, max_edit_dist=3, max_vocab=-1, keyedvectors=False):
    if keyedvectors:
        w2v = KeyedVectors.load_word2vec_format(w2v_file, binary=True, limit=50000)
    else:
        w2v = Word2Vec.load(w2v_file)
    sym_spell = SymSpell(max_edit_dist)
    sym_spell.load_dictionary(vocab_file, 0, 1)

    return w2v, sym_spell

def vocab_dicts(w2v, max_vocab=-1):
    voc = w2v.wv.vocab

    w2i = {w: voc[w].index for w in voc.keys() if voc[w].index < max_vocab or max_vocab <= 0}
    i2w = {i: w2v.wv.index2entity[i] for i in range(len(w2v.wv.index2entity)) if i < max_vocab or max_vocab <= 0}
    #i2w = w2v.wv.index2entity if max_vocab <= 0 else w2v.wv.index2entity[:max_vocab]

    #w2i = lambda w: voc[w].index if w in voc else -1
    #i2w = lambda i: w2v.wv.index2entity[i]
    return w2i, i2w


def normalize_string(s):
    s = s.lower().strip()
    s = re.sub('\!+', '!', s)
    s = re.sub('\,+', ',', s)
    s = re.sub('\?+', '?', s)
    s = re.sub('\.+', '.', s)
    #s = re.sub("[^0-9a-zA-Z.!?,'']+", ' ', s)
    for p in punctuation:
      if p != "'":
        s = s.replace(p, " " + p + " ")
    s = re.sub(' +', ' ', s)
    s = s.strip()
    return s

def normalize_string_en(s):
    s = unidecode(s)
    s = s.lower().strip()
    s = re.sub('\!+', '!', s)
    s = re.sub('\,+', ',', s)
    s = re.sub('\?+', '?', s)
    s = re.sub('\.+', '.', s)
    #s = re.sub("[^0-9a-zA-Z.!?,'']+", ' ', s)
    for p in punctuation:
      if p != "'":
        s = s.replace(p, " " + p + " ")
    s = re.sub(' +', ' ', s)
    s = s.strip()
    return s

def normalize_string_fr(s, ignore_apost = False):
    s = unidecode(s)
    s = s.lower().strip()
    s = re.sub('\!+', '!', s)
    s = re.sub('\,+', ',', s)
    s = re.sub('\?+', '?', s)
    s = re.sub('\.+', '.', s)
    # put spaces around punctuation
    for p in punctuation:
        if p in s:
            if p != "'":
                s = s.replace(p, " " + p + " ")
            else:
                s = s.replace(p, p + " ")
    s = re.sub(' +', ' ', s)

    # separate number and following word
    match = re.search('\d , \d', s)
    while match is not None:
        s = s[:match.start() + 1] + ',' + s[match.end() - 1:]
        match = re.search('\d , \d', s)
    match = re.search('\d . \d', s)
    while match is not None:
        s = s[:match.start() + 1] + ',' + s[match.end() - 1:]
        match = re.search('\d . \d', s)
    match = re.search('\d[^\d\,\.\ ]', s)
    while match is not None:
        s = s[:match.start()+1] + ' ' + s[match.end():]
        match = re.search('\d[^\d\,\.\ ]', s)
    # process apostrophes in French (chop before)
    if ignore_apost:
        match = re.search('\w+\'+', s)
        while match is not None:
            s = s[:match.start()] + s[match.end():]
            match = re.search('\w+\'+', s)
    s = s.strip()
    return s

"""
PAD = "<pad>"
UNK = "<ukt>"
SOS = "<sos>"
EOS = "<eos>"
"""
PAD = "[PAD]" # 0
UNK = "[UNK]" # 100
SOS = "[CLS]" # 101
EOS = "[SEP]" # 102
TOKENS = [PAD, UNK, SOS, EOS]

class Vocab(nn.Module):
    def __init__(self, i2t, t2i, reserved=TOKENS, trainable_tokens=(), sos_token=SOS, eos_token=EOS, unk_token=UNK, pad_token=PAD, **tensors):
        super().__init__()

        if reserved is None:
            self.i2t = copy.deepcopy(i2t)
            self.t2i = copy.deepcopy(t2i)
            for name, tensor in tensors.items():
                if use_cuda:
                    p = nn.Parameter(torch.FloatTensor(*tensor.size()).cuda())
                    with torch.no_grad():
                        p.copy_(tensor.cuda())
                    self.register_parameter(name, p)
                    #setattr(self, key, value.cuda())
                else:
                    p = nn.Parameter(torch.FloatTensor(*tensor.size()))
                    with torch.no_grad():
                        p.copy_(tensor)
                    self.register_parameter(name, p)
                    #setattr(self, key, value)

        else:
            pruned = [t for t in reserved if t not in t2i]
            n = len(pruned)

            self.i2t = {i:pruned[i] for i in range(n)}
            self.t2i = {pruned[i]:i for i in range(n)}
            for k,v in i2t.items():
                self.i2t[k+n] = v
            for k,v in t2i.items():
                self.t2i[k] = v+n
            for name, tensor in tensors.items():
                m = tensor.size(0)
                expanded = torch.zeros(m + n, *tensor.size()[1:])
                expanded[n:] = tensor.clone()

                if use_cuda:
                    p = nn.Parameter(torch.FloatTensor(*expanded.size()).cuda())
                    with torch.no_grad():
                        p.copy_(expanded.cuda())
                    self.register_parameter(name, p)
                    #setattr(self, key, value.cuda())
                else:
                    p = nn.Parameter(torch.FloatTensor(*expanded.size()))
                    with torch.no_grad():
                        p.copy_(expanded)
                    self.register_parameter(name, p)
        self.voc_size = len(self.t2i.keys())

        if sos_token is not None:
            self.sos_token = sos_token
            self.sos_index = self.t2i[sos_token]
        if eos_token is not None:
            self.eos_token = eos_token
            self.eos_index = self.t2i[eos_token]
        if unk_token is not None:
            self.unk_token = unk_token
            self.unk_index = self.t2i[unk_token]
        if pad_token is not None:
            self.pad_token = pad_token
            self.pad_index = self.t2i[pad_token]

        for p in self.parameters():
            p.requires_grad = False

        '''
        #self.trainable_indices = [self.i2t[x] for x in trainable_tokens]
        self.gradient_mask = torch.zeros(self.voc_size,1)
        if use_cuda:
            self.gradient_mask = self.gradient_mask.cuda()
        for t in trainable_tokens:
            self.gradient_mask[self.t2i[t]] = 1
        def filter_grad(grad):
            return self.gradient_mask * grad
        for p in self.parameters():
            p.register_hook(filter_grad)
        '''


def voc_from_bert(bert_model, bert_tokenizer):
    #sos_token, eos_token, unk_token, pad_token = bert_tokenizer.bos_token, bert_tokenizer.eos_token, \
    #                                             bert_tokenizer.unk_token, bert_tokenizer.
    sos_token, eos_token, unk_token, pad_token = bert_tokenizer.cls_token, bert_tokenizer.sep_token, \
                                                 bert_tokenizer.unk_token, bert_tokenizer.pad_token
    return Vocab(bert_tokenizer.ids_to_tokens, bert_tokenizer.vocab, vecs=bert_model.embeddings.word_embeddings.weight,
                reserved=None, sos_token=sos_token, eos_token=eos_token, unk_token=unk_token, pad_token=pad_token)


def voc_from_b2v(b2v, max_vocab=-1):
    all_words = b2v.words
    N = len(all_words)

    i2w = {i: all_words[i] for i in range(N) if i < max_vocab or max_vocab <= 0}
    w2i = {all_words[i]: i for i in range(N) if i < max_vocab or max_vocab <= 0}

    vector_list = [b2v.get_word_vector(i2w[i]) for i in range(N)]
    vectors = torch.FloatTensor(np.stack(vector_list, axis=0))
    if use_cuda:
        vectors = vectors.cuda()

    return Vocab(i2w, w2i, vecs=vectors)



'''
def make_batch(seqs, vocab, tree, k, tau=1, vocab_tensors=None, PAD_INDEX=-1,  **kwargs):
    bs = len(seqs)
    #sorted_seqs = sorted(seqs, key=len, reverse=True)
    max_length = max([len(s) for s in seqs])

    if vocab_tensors is not None:
        vocab_neighbours, vocab_costs = vocab_tensors

    neighbours = PAD_INDEX*torch.ones(bs, max_length, k).long()
    costs = PAD_INDEX*torch.ones(bs, max_length, k).float()
    lengths = torch.zeros(bs).long()
    for i in range(bs):
        words = seqs[i]
        l = len(words)
        lengths[i] = l
        for j in range(l):
            word = words[j]
            index = vocab(word)
            if vocab_tensors is not None and index != -1:
                indices_ij = vocab_neighbours[index,:]
                dists_ij = vocab_costs[index, :]
            else:
                indices_ij, dists_ij = _tensorize(word, vocab, tree, k, **kwargs)
            k_ij = indices_ij.size(0)
            neighbours[i,j,:k_ij] = indices_ij[:k]
            costs[i,j,:k_ij] = dists_ij[:k]
    costs = normalize_costs(costs, tau=tau)
    #mask = mask_lengths(lengths,max_l=max_length)
    if use_cuda:
        neighbours, costs = neighbours.cuda(), costs.cuda()#, mask.cuda()
    return (neighbours, costs), lengths
    '''

def make_batch(seqs, vocab, tree, k, tau=1, vocab_tensors=None, PAD_INDEX=-1, only_oov=True, max_length=-1, force_max_len=False, **kwargs):
    bs = len(seqs)
    #sorted_seqs = sorted(seqs, key=len, reverse=True)
    max_seq_length = max([len(s) for s in seqs])
    if max_length > 0:
        if not force_max_len:
            batch_max_length = min(max_length, max_seq_length)
        else:
            batch_max_length = max_length
    else:
        batch_max_length = max_seq_length
    seqs = list(map(lambda seq: seq[:batch_max_length], seqs))

    if vocab_tensors is not None:
        vocab_neighbours, vocab_costs = vocab_tensors

    neighbours = vocab.pad_index * torch.ones(bs, batch_max_length, k).long()
    costs = -1 * torch.ones(bs, batch_max_length, k).float()
    oov = torch.ones(bs, batch_max_length).float()
    lengths = torch.zeros(bs).long()
    for i in range(bs):
        words = seqs[i]
        l = len(words)
        lengths[i] = l
        for j in range(l):
            word = words[j]
            if word in vocab.t2i:
                index = vocab.t2i[word]
                if only_oov:
                    indices_ij = torch.Tensor([index])
                    dists_ij = torch.Tensor([0])
                    oov[i,j] = 0
                elif vocab_tensors is not None:
                    indices_ij = vocab_neighbours[index,:]
                    dists_ij = vocab_costs[index, :]
                else:
                    indices_ij, dists_ij = ss_tensorize(word, vocab.t2i, tree, k, **kwargs)
            else:
                indices_ij, dists_ij = ss_tensorize(word, vocab.t2i, tree, k, **kwargs)
            k_ij = indices_ij.size(0)
            neighbours[i,j,:k_ij] = indices_ij[:k]
            costs[i,j,:k_ij] = dists_ij[:k]
    #costs = normalize_costs(costs, tau=tau)
    #mask = mask_lengths(lengths,max_l=max_length)
    if use_cuda:
        neighbours, costs, oov = neighbours.cuda(), costs.cuda(), oov.cuda()#, mask.cuda()
        lengths = lengths.cuda()
    return (neighbours, costs, oov), lengths

'''
def make_batch_w2v(seqs, vocab, PAD_INDEX=-1):
    bs = len(seqs)
    #sorted_seqs = sorted(seqs, key=len, reverse=True)
    max_length = max([len(s) for s in seqs])

    batch = PAD_INDEX*torch.ones(bs, max_length).long()
    lengths = torch.zeros(bs).long()

    for i in range(bs):
        words = seqs[i]
        l = len(words)
        oov=0
        for j in range(l):
            word = words[j]
            #
            if word in vocab:
                index = vocab[word]
                batch[i,j-oov] = index
            else:
                oov += 1
        lengths[i] = l - oov
    #mask = mask_lengths(lengths, max_l=max_length)
    if use_cuda:
        batch, lengths = batch.cuda(), lengths.cuda()#, mask.cuda()
    return batch, lengths
'''

def make_batch_w2v(seqs, vocab, max_length=-1, exclude_oov=False, force_max_len=False):
    bs = len(seqs)
    #sorted_seqs = sorted(seqs, key=len, reverse=True)
    max_seq_length = max([len(s) for s in seqs])
    if max_length > 0:
        if not force_max_len:
            batch_max_length = min(max_length, max_seq_length)
        else:
            batch_max_length = max_length
    else:
        batch_max_length = max_seq_length
    #max_length = min(max_length, max_seq_length) if max_length > 0 else max_seq_length
    seqs = list(map(lambda seq: seq[:batch_max_length], seqs))

    #pad_index = vocab[PAD]
    #sunk_index = vocab[UNK]

    batch = vocab.pad_index*torch.ones(bs, batch_max_length).long()
    lengths = torch.zeros(bs).long()

    for i in range(bs):
        words = seqs[i]
        l = len(words)
        oov=0
        for j in range(l):
            word = words[j]
            if word in vocab.t2i:
                index = vocab.t2i[word]
                batch[i,j-oov] = index
            else:
                if not exclude_oov:
                    batch[i,j-oov] = vocab.unk_index
                else:
                    oov += 1
        lengths[i] = l - oov
    #mask = mask_lengths(lengths, max_l=max_length)
    if use_cuda:
        batch, lengths = batch.cuda(), lengths.cuda()#, mask.cuda()
    return batch, lengths

def ss_tensorize(word, vocab, tree, k, **kwargs):
    neighbours_ij = tree.lookup(word, Verbosity.ALL, **kwargs)
    indices_ij = torch.LongTensor([vocab[item.term] for item in neighbours_ij])[:k]
    dists_ij = torch.FloatTensor([item.distance for item in neighbours_ij])[:k]
    return indices_ij, dists_ij

def normalize_costs(costs, tau=1, eps=1e-8):
    cost_mask = negative_mask(costs)
    normed = masked_softmax(-costs, cost_mask, dim=-1, tau=tau) + eps
    return normed


def cluster_contexts(C, k):
    kmeans = MiniBatchKMeans(n_clusters=k)
    kmeans.fit(C)
    indices = kmeans.predict(C)
    return indices, kmeans.cluster_centers_


def naive_spellcheck(text, ss, vocab, **kwargs):
    checked_text = []
    for word in text:
        if word in vocab:
            checked_text.append(word)
        else:
            neighbours = ss.lookup(word, Verbosity.ALL, **kwargs)
            corrected_word = neighbours[0].term if len(neighbours) > 0 else word
            checked_text.append(corrected_word)
    return checked_text




class BatchGenerator():
    def __init__(self, data, vocab, max_length, *args, max_data_size=-1, n_inputs=1, robust=False, **kwargs):
        self.data = data
        self.vocab = vocab
        self.data_size = len(data) if max_data_size <= 0 else min(max_data_size, len(data))
        self.max_length = max_length
        self.args = args
        self.kwargs = kwargs

        self.n_inputs = n_inputs
        self.build_tensors(*args, robust=robust, **kwargs)

    def make_batches(self, batch_size):
        def gen_fct():
            return self._make_batches(batch_size)
        return gen_fct

    def build_tensors(self, *args, robust=False, **kwargs):
        pass

class ClassificationBatchGenerator(BatchGenerator):
    def __init__(self, data, labels, vocab, max_length, *args, robust=False, **kwargs):
        super().__init__(data, vocab, max_length, *args, robust=robust, **kwargs)
        self.labels = torch.FloatTensor(labels)
        if use_cuda:
            self.labels = self.labels.cuda()

    def build_tensors(self, *args, robust=False, **kwargs):
        self.tensor_class = NaiveTensors if not robust else RobustTensors
        if self.n_inputs == 1:
            self.tensors = self.tensor_class(self.data[:self.data_size], self.max_length, self.vocab, *args, **kwargs)
            self.tensors.build_tensors()
        else:
            split_data = list(zip(*self.data[:self.data_size]))
            assert self.n_inputs == len(split_data)
            self.tensors = [self.tensor_class(split_data[i], self.max_length, self.vocab, *args, **kwargs) for i in
                            range(self.n_inputs)]
            for T in self.tensors:
                T.build_tensors()

    def _make_batches(self, batch_size):
        n_batches = math.ceil(self.data_size / batch_size)
        p = torch.randperm(self.data_size)

        for i in range(n_batches):
            min_j = i * batch_size
            max_j = min((i + 1) * batch_size, self.data_size)
            jrange = p[min_j:max_j]
            if self.n_inputs == 1:
                batch = self.tensors._make_batch(jrange)
            else:
                batch = [T._make_batch(jrange) for T in self.tensors]
            yield batch, self.labels[jrange]

class TranslationBatchGenerator(BatchGenerator):
    def __init__(self, data, src_vocab, tgt_vocab, max_length, *args, **kwargs):
        super().__init__(data, [src_vocab,tgt_vocab], max_length, *args, **kwargs)

    def build_tensors(self, *args, robust=False, **kwargs):
        src_data, tgt_data = zip(*self.data[:self.data_size])
        if robust:
            src_tensors = RobustTensors(src_data, self.max_length, self.vocab[0], *args, **kwargs)
        else:
            src_tensors = NaiveTensors(src_data, self.max_length, self.vocab[0], *args, **kwargs)
        tgt_tensors = NaiveTensors(tgt_data, self.max_length, self.vocab[1], *args, **kwargs)
        src_tensors.build_tensors()
        tgt_tensors.build_tensors()
        self.tensors=[src_tensors,tgt_tensors]

    def _make_batches(self, batch_size):
        n_batches = math.ceil(self.data_size / batch_size)
        p = torch.randperm(self.data_size)

        for i in range(n_batches):
            min_j = i * batch_size
            max_j = min((i + 1) * batch_size, self.data_size)
            jrange = p[min_j:max_j]
            batch = [T._make_batch(jrange) for T in self.tensors]
            yield batch

class LMBatchGenerator(BatchGenerator):
    def __init__(self, data, vocab, max_length, *args, robust=False, **kwargs):
        super().__init__(data, vocab, max_length, *args, robust=robust, **kwargs)

    def build_tensors(self, *args, robust=False, **kwargs):
        self.tensor_class = NaiveTensors if not robust else RobustTensors
        if self.n_inputs == 1:
            self.tensors = self.tensor_class(self.data[:self.data_size], self.max_length, self.vocab, *args, **kwargs)
            self.tensors.build_tensors()
        else:
            split_data = list(zip(*self.data[:self.data_size]))
            assert self.n_inputs == len(split_data)
            self.tensors = [self.tensor_class(split_data[i], self.max_length, self.vocab, *args, **kwargs) for i in
                            range(self.n_inputs)]
            for T in self.tensors:
                T.build_tensors()

    def _make_batches(self, batch_size):
        n_batches = math.ceil(self.data_size / batch_size)
        p = torch.randperm(self.data_size)

        for i in range(n_batches):
            min_j = i * batch_size
            max_j = min((i + 1) * batch_size, self.data_size)
            jrange = p[min_j:max_j]
            if self.n_inputs == 1:
                batch = self.tensors._make_batch(jrange)
            else:
                batch = [T._make_batch(jrange) for T in self.tensors]
            yield batch

class BatchTensors():
    def __init__(self, data, max_length, vocab, *args, **kwargs):
        self.data = data
        self.data_size = len(data)
        self.max_length = max_length
        self.vocab = vocab

    def build_tensors(self):
        self._build_tensors()

        for j in tqdm.tqdm(range(self.data_size)):
            outs = self._tensorize(self.data[j])
            self._insert_tensor(outs, j)

    def _build_tensors(self):
        pass

    def _tensorize(self, record, *args, **kwargs):
        pass

    def _insert_tensor(self, record, j):
        pass


class NaiveTensors(BatchTensors):
    def __init__(self, data, max_length, vocab, *args, exclude_oov=False, **kwargs):
        super().__init__(data, max_length, vocab)
        self.exclude_oov = exclude_oov

    def _build_tensors(self):
        self.tensors = torch.ones(self.data_size, self.max_length, dtype=torch.long) * self.vocab.pad_index
        self.lengths = torch.zeros(self.data_size, dtype=torch.long)

    def _insert_tensor(self, record, j):
        batch, length = record
        self.tensors[j] = batch
        self.lengths[j] = length

    def _tensorize(self, record, *args, **kwargs):
        tensor = self.vocab.pad_index * torch.ones(self.max_length).long()

        words = record[:self.max_length]
        l = len(words)
        oov = 0
        for j in range(l):
            word = words[j]
            if word in self.vocab.t2i:
                index = self.vocab.t2i[word]
                tensor[j - oov] = index
            else:
                if not self.exclude_oov:
                    tensor[j - oov] = self.vocab.unk_index
                else:
                    oov += 1
        lengths = l - oov

        return tensor, lengths

    def _make_batch(self, indices):
        batch_lengths = self.lengths[indices]
        batch_max_length = batch_lengths.max()
        batch_tensors = self.tensors[indices, :batch_max_length]

        if use_cuda:
            batch_tensors = batch_tensors.cuda()
            batch_lengths = batch_lengths.cuda()
        return batch_tensors, batch_lengths

class RobustTensors(BatchTensors):
    def __init__(self, data, max_length, vocab, ss, k, vocab_tensors=None, tau=1, only_oov=True):
        super().__init__(data, max_length, vocab)
        self.ss = ss
        self.k = k

        if vocab_tensors is not None:
            self.vocab_neighbours, self.vocab_costs = vocab_tensors
            self.vocab_tensors = True
        else:
            self.vocab_tensors = False

        self.tau = tau
        self.only_oov = only_oov

    def _tensorize(self, record, **kwargs):
        neighbours = self.vocab.pad_index * torch.ones(self.max_length, self.k).long()
        costs = -1 * torch.ones(self.max_length, self.k).float()
        oov = torch.ones(self.max_length).float()

        words = record[:self.max_length]
        l = len(words)
        for j in range(l):
            word = words[j]
            if word in self.vocab.t2i:
                index = self.vocab.t2i[word]
                if self.only_oov:
                    indices_j = torch.Tensor([index])
                    dists_j = torch.Tensor([0])
                    oov[j] = 0
                elif self.vocab_tensors:
                    indices_j = self.vocab_neighbours[index, :]
                    dists_j = self.vocab_costs[index, :]
                else:
                    indices_j, dists_j = ss_tensorize(word, self.vocab.t2i, self.ss, self.k, **kwargs)
            else:
                indices_j, dists_j = ss_tensorize(word, self.vocab.t2i, self.ss, self.k, **kwargs)
            k_j = indices_j.size(0)
            neighbours[j, :k_j] = indices_j[:self.k]
            costs[j, :k_j] = dists_j[:self.k]

        #costs = normalize_costs(costs, tau=self.tau)
        return (neighbours, costs, oov), l

    def _build_tensors(self):
        self.neighbours = torch.ones(self.data_size, self.max_length, self.k, dtype=torch.long) * self.vocab.pad_index
        self.costs = torch.ones(self.data_size, self.max_length, self.k) * self.vocab.pad_index
        self.mask = torch.ones(self.data_size, self.max_length, dtype=torch.long) * self.vocab.pad_index
        self.lengths = torch.zeros(self.data_size, dtype=torch.long)

    def _insert_tensor(self, record, j):
        (neighbours, costs, mask), length = record
        self.neighbours[j] = neighbours
        self.costs[j] = costs
        self.mask[j] = mask
        self.lengths[j] = length

    def _make_batch(self, indices):
        batch_lengths = self.lengths[indices]
        batch_max_length = batch_lengths.max()
        batch_neighbours = self.neighbours[indices, :batch_max_length]
        batch_costs = self.costs[indices, :batch_max_length]
        batch_mask = self.mask[indices, :batch_max_length]

        if use_cuda:
            batch_neighbours = batch_neighbours.cuda()
            batch_costs = batch_costs.cuda()
            batch_mask = batch_mask.cuda()
            batch_lengths = batch_lengths.cuda()
        return (batch_neighbours, batch_costs, batch_mask), batch_lengths