from utils import *

from preprocess import vocab_dicts

import torch.nn as nn
import numpy as np


class LikelihoodModel(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, c, w, i):
        return


class W2VLikelihood(nn.Module):
    def __init__(self, C, W):
        super().__init__()
        self.W = W
        self.C = C

    def forward(self, c, w, width=5, mask=None, vector_input=False, oov_mask=None):
        # c is bs x l, w is m x n x bs x l x d
        C = self.C[c, :].contiguous()
        l = C.size(-2)
        seq_logits = w.matmul(C.transpose(-2, -1))  # n x bs x l x l
        if mask is not None:
            seq_logits *= mask.unsqueeze(1).unsqueeze(0).expand_as(seq_logits).float()
        if width > 0:
            context_mask = (banded_mask(l, width) - torch.eye(l)).unsqueeze(-1)
        else:
            context_mask = (1-torch.eye(l)).unsqueeze(-1)#
        if use_cuda:
            context_mask = context_mask.cuda()
        seq_logits_masked = seq_logits.unsqueeze(-2).matmul(context_mask).squeeze(-1)  # n x bs x l x 1
        norm = self._logcontext(w).unsqueeze(-1)  # n x bs x l
        return -(l - 1) * norm + seq_logits_masked

    def _logcontext(self, w, subsamples=5000):
        if subsamples <= 0:
            logits = w.matmul(self.C.t())
            out = logits.logsumexp(dim=-1)
        else:
            samples = torch.randint(self.C.size(0), (subsamples,))
            logits = w.matmul(self.C[samples, :].t())
            out = logits.logsumexp(dim=-1) + np.log(self.C.size(0) / subsamples)
        return out

    def maximum_likelihood(self, c, width=10, k=5, return_logprobs=False):
        C = self.C[c, :].contiguous()
        l = C.size(-2)
        if width > 0:
            context_mask = (banded_mask(l, width) - torch.eye(l))#.unsqueeze(-1)
        else:
            context_mask = (1 - torch.eye(l))#.unsqueeze(-1)
        if use_cuda:
            context_mask = context_mask.cuda()
        seq_logits = self.W.unsqueeze(0).matmul(C.transpose(-2, -1))        # 1 x V x l
        seq_logits_masked = seq_logits.matmul(context_mask)                 # 1 x V x l
        norm = self._logcontext(self.W)                                     # V x 1
        logprobs = seq_logits_masked - (l-1)*norm.unsqueeze(-1)
        #repeated = seq_logits.unsqueeze(-1).repeat_interleave(l, dim=-1)
        #seq_logits_masked = repeated.unsqueeze(-2).matmul(context_mask).squeeze(-1).squeeze(-1)
        #dists = cosine_dist(C, self.W)
        v, i = (logprobs.transpose(-2,-1)).topk(k, dim=-1)
        if return_logprobs:
            return v, i
        else:
            return i


class W2VLikelihoodNaive(nn.Module):
    def __init__(self, C, W):
        super().__init__()
        self.W = W
        self.C = C

    def forward(self, c, w, width=5, mask=None, vector_input=False, oov_mask=None):
        # c is bs x l, w is k x bs x l
        C = self.C[c, :].contiguous()
        W = self.W[w,:].contiguous()
        l = C.size(-2)
        seq_logits = W.matmul(C.transpose(-2, -1))  # n x bs x l x l
        if mask is not None:
            seq_logits *= mask.unsqueeze(1).unsqueeze(0).expand_as(seq_logits).float()
        if width > 0:
            context_mask = (banded_mask(l, width) - torch.eye(l)).unsqueeze(-1)
        else:
            context_mask = (1-torch.eye(l)).unsqueeze(-1)#
        if use_cuda:
            context_mask = context_mask.cuda()
        seq_logits_masked = seq_logits.unsqueeze(-2).matmul(context_mask).squeeze(-1)  # n x bs x l x 1
        norm = self._logcontext(W).unsqueeze(-1)  # n x bs x l
        return (-(l - 1) * norm + seq_logits_masked).squeeze(-1)

    def _logcontext(self, w, subsamples=5000):
        if subsamples <= 0:
            logits = w.matmul(self.C.t())
            out = logits.logsumexp(dim=-1)
        else:
            samples = torch.randint(self.C.size(0), (subsamples,))
            logits = w.matmul(self.C[samples, :].t())
            out = logits.logsumexp(dim=-1) + np.log(self.C.size(0) / subsamples)
        return out

    def maximum_likelihood(self, c, width=10, k=5, return_logprobs=False):
        C = self.C[c, :].contiguous()
        l = C.size(-2)
        if width > 0:
            context_mask = (banded_mask(l, width) - torch.eye(l))#.unsqueeze(-1)
        else:
            context_mask = (1 - torch.eye(l))#.unsqueeze(-1)
        if use_cuda:
            context_mask = context_mask.cuda()
        seq_logits = self.W.unsqueeze(0).matmul(C.transpose(-2, -1))        # 1 x V x l
        seq_logits_masked = seq_logits.matmul(context_mask)                 # 1 x V x l
        norm = self._logcontext(self.W)                                     # V x 1
        logprobs = seq_logits_masked - (l-1)*norm.unsqueeze(-1)
        #repeated = seq_logits.unsqueeze(-1).repeat_interleave(l, dim=-1)
        #seq_logits_masked = repeated.unsqueeze(-2).matmul(context_mask).squeeze(-1).squeeze(-1)
        #dists = cosine_dist(C, self.W)
        v, i = (logprobs.transpose(-2,-1)).topk(k, dim=-1)
        if return_logprobs:
            return v, i
        else:
            return i


class XLNetLikelihood(LikelihoodModel):
    def __init__(self, ):
        super().__init__()




class BertLikelihoodNaive(LikelihoodModel):
    def __init__(self, bert, lm_head, tokenizer):
        super().__init__()
        self.bert = bert
        self.lm_head = lm_head
        self.tokenizer = tokenizer
        self.dim = self.bert.config.hidden_size

        for param in self.bert.parameters():
            param.requires_grad = False
        for param in self.lm_head.parameters():
            param.requires_grad = False

        if use_cuda:
            self.bert = self.bert.cuda()
            self.lm_head = self.lm_head.cuda()
            #self.tokenizer = self.tokenizer.cuda()

    def _preprocess_bert(self, c):
        # c is bs x l,
        bs, l = c.size()
        inputs = c.unsqueeze(1).repeat(1, l, 1)     # bs x l x l
        input_mask = torch.eye(l).view(1, l, l).expand(bs, l, l).bool() #
        if use_cuda:
            input_mask = input_mask.cuda()
        inputs.masked_fill_(input_mask, self.tokenizer.mask_token_id)
        inputs_masked = inputs.view(-1, l)
        cls = torch.ones(bs * l, 1).long() * self.tokenizer.cls_token_id
        sep = torch.ones(bs * l, 1).long() * self.tokenizer.sep_token_id
        if use_cuda:
            cls = cls.cuda()
            sep = sep.cuda()
        processed_inputs = torch.cat([cls, inputs_masked, sep], 1)

        return processed_inputs

    def forward(self, c, w, mask=None, subsamples=-1):
        # c is bs x l, w is k x bs x l
        k, bs, l = w.size()
        inputs = self._preprocess_bert(c)
        outputs = self.bert(inputs)[0]
        outputs_trunc = outputs[:, 1:-1, :]  # (bs * l) x l x d
        outputs_unrolled = outputs_trunc.reshape(bs, l, l, -1)  # bs x l x l x d
        outputs_rolled = outputs_unrolled.reshape(bs, l * l, -1)  # bs x (l x l) x d
        inds = torch.eye(l).view(-1).nonzero().squeeze(-1)
        if use_cuda:
            inds = inds.cuda()
        outputs_collapsed = torch.index_select(outputs_rolled, 1, inds)    # bs x l x d
        scores = self.lm_head(outputs_collapsed)                            # bs x l x N
        logprobs = F.log_softmax(scores, dim=-1)                             # bs x l x N
        logprobs_expanded = logprobs.unsqueeze(0).expand(k, *logprobs.size())
        logprobs_collapsed = torch.gather(logprobs_expanded, -1, w.unsqueeze(-1))
        return logprobs_collapsed.squeeze(-1)


class BertLikelihood(LikelihoodModel):
    def __init__(self, bert, tokenizer):
        super().__init__()
        self.bert_model = bert
        self.tokenizer = tokenizer
        self.dim = self.bert_model.config.hidden_size

        for param in self.bert_model.parameters():
            param.requires_grad = False

        if use_cuda:
            self.bert_model = self.bert_model.cuda()
            #self.tokenizer = self.tokenizer.cuda()

    def _preprocess_bert(self, c, vector_input=False, oov_mask=None):
        # c is bs x l,
        if vector_input:
            bs, l, d = c.size()
            inputs = c.unsqueeze(1).repeat(1, l, 1, 1)     # bs x l x l x d
            input_mask = torch.eye(l).view(1, l, l, 1).expand(bs, l, l, d).bool() #
            if use_cuda:
                input_mask = input_mask.cuda()
            inputs.masked_fill_(input_mask, self.tokenizer.mask_token_id)
            inputs_masked = inputs.view(-1, l, d)
            cls_vec = self.bert_model.bert.embeddings.word_embeddings.weight[self.tokenizer.cls_token_id,:]
            sep_vec = self.bert_model.bert.embeddings.word_embeddings.weight[self.tokenizer.sep_token_id,:]
            cls = cls_vec[None,None,:].repeat(bs * l, 1, 1)
            sep = sep_vec[None,None,:].repeat(bs * l, 1, 1)
            if use_cuda:
                cls = cls.cuda()
                sep = sep.cuda()
            processed_inputs = torch.cat([cls, inputs_masked, sep], 1)
        else:
            bs, l = c.size()
            inputs = c.unsqueeze(1).repeat(1, l, 1)  # bs x l x l
            input_mask = torch.eye(l).view(1, l, l).expand(bs, l, l).bool()  #
            if use_cuda:
                input_mask = input_mask.cuda()
            inputs.masked_fill_(input_mask, self.tokenizer.mask_token_id)
            inputs_masked = inputs.view(-1, l)
            cls = torch.ones(bs * l, 1).long() * self.tokenizer.cls_token_id
            sep = torch.ones(bs * l, 1).long() * self.tokenizer.sep_token_id
            if use_cuda:
                cls = cls.cuda()
                sep = sep.cuda()
            processed_inputs = torch.cat([cls, inputs_masked, sep], 1)
        return processed_inputs

    def forward(self, c, w, mask=None, subsamples=5000, vector_input=False, oov_mask=None):
        if vector_input:
            bs, l, d = c.size()
            inputs = self._preprocess_bert(c, vector_input=True, oov_mask=oov_mask)
            outputs = self.bert_model(inputs_embeds=inputs)  #(bs * l) x (l+2) x d
        else:
            bs, l = c.size()
            inputs = self._preprocess_bert(c, oov_mask=oov_mask)
            outputs = self.bert_model(inputs)  # (bs * l) x (l+2) x d
        outputs_trunc = outputs[:,1:-1,:]    #(bs * l) x l x d
        outputs_unrolled = outputs_trunc.reshape(bs, l, l, self.dim)   # bs x l x l x d
        outputs_rolled = outputs_unrolled.reshape(bs, -1, self.dim)    # ba x (l x l) x d

        inds = torch.eye(l).view(-1).nonzero().squeeze(-1)
        if use_cuda:
            inds = inds.cuda()
        output_vectors = torch.index_select(outputs_rolled, 1, inds)    # bs x l x d

        numerator_rolled = output_vectors.view(1, 1, bs, l, self.dim).expand(*w.size()).unsqueeze(-2).matmul(
            w.unsqueeze(-1))
        numerator = numerator_rolled.view(*w.size()[:-1],1)

        '''
        vocab_vecs = self.bert_model.bert.embeddings.word_embeddings.weight    # N x d
        if subsamples <= 0:
            logits = output_vectors.matmul(vocab_vecs.t())          # bs x l x N
            denominator = logits.logsumexp(dim=-1)
        else:
            voc_size = vocab_vecs.size(0)
            samples = torch.randint(voc_size, (subsamples,))
            logits = output_vectors.matmul(vocab_vecs[samples,:].t())
            denominator = logits.logsumexp(dim=-1) + np.log(voc_size/subsamples)
        '''

        return numerator #- denominator.view(1, 1, bs, l, 1)

    def maximum_likelihood(self, c, k=5, mask=None, subsamples=-1, return_logprobs=False):
        bs, l = c.size()
        inputs = self._preprocess_bert(c)
        outputs = self.bert_model(inputs)  # (bs * l) x (l+2) x d
        outputs_trunc = outputs[:, 1:-1, :]  # (bs * l) x l x d
        outputs_unrolled = outputs_trunc.reshape(bs, l, l, self.dim)  # bs x l x l x d
        outputs_rolled = outputs_unrolled.reshape(bs, -1, self.dim)  # ba x (l x l) x d

        inds = torch.eye(l).view(-1).nonzero().squeeze(-1)
        if use_cuda:
            inds = inds.cuda()
        output_vectors = torch.index_select(outputs_rolled, 1, inds)  # bs x l x d


        vocab_vecs = self.bert_model.bert.embeddings.word_embeddings.weight
        dists = output_vectors.matmul(vocab_vecs.t()) #/ vocab_vecs.norm(dim=1)

        v, i = dists.topk(k, dim=-1)
        if return_logprobs:
            denominator = dists.logsumexp(dim=-1, keepdim=True)
            return v-denominator, i
        else:
            return i



class BertLikelihood2(LikelihoodModel):
    def __init__(self, bert, tokenizer):
        super().__init__()
        self.bert_model = bert
        self.tokenizer = tokenizer
        self.dim = self.bert_model.config.hidden_size

        for param in self.bert_model.parameters():
            param.requires_grad = False

        if use_cuda:
            self.bert_model = self.bert_model.cuda()
            #self.tokenizer = self.tokenizer.cuda()

    def _preprocess_bert(self, c, vector_input=False, oov_mask=None):
        # c is bs x l,
        if vector_input:
            bs, l, d = c.size()
            inputs = c.unsqueeze(1).repeat(1, l, 1, 1)     # bs x l x l x d
            input_mask = torch.eye(l).view(1, l, l, 1).expand(bs, l, l, d).bool() #
            if use_cuda:
                input_mask = input_mask.cuda()
            inputs.masked_fill_(input_mask, self.tokenizer.mask_token_id)
            inputs_masked = inputs.view(-1, l, d)
            cls_vec = self.bert_model.bert.embeddings.word_embeddings.weight[self.tokenizer.cls_token_id,:]
            sep_vec = self.bert_model.bert.embeddings.word_embeddings.weight[self.tokenizer.sep_token_id,:]
            cls = cls_vec[None,None,:].repeat(bs * l, 1, 1)
            sep = sep_vec[None,None,:].repeat(bs * l, 1, 1)
            if use_cuda:
                cls = cls.cuda()
                sep = sep.cuda()
            processed_inputs = torch.cat([cls, inputs_masked, sep], 1)
        else:
            bs, l = c.size()
            cls = torch.ones(bs, 1).long() * self.tokenizer.cls_token_id
            sep = torch.ones(bs, 1).long() * self.tokenizer.sep_token_id
            if use_cuda:
                cls = cls.cuda()
                sep = sep.cuda()
            processed_inputs = torch.cat([cls, c, sep], 1)
        return processed_inputs

    def forward(self, c, w, mask=None, subsamples=5000, vector_input=False, oov_mask=None):
        if vector_input:
            bs, l, d = c.size()
            inputs = self._preprocess_bert(c, vector_input=True, oov_mask=oov_mask)
            outputs = self.bert_model(inputs_embeds=inputs)  #(bs * l) x (l+2) x d
        else:
            bs, l = c.size()
            inputs = self._preprocess_bert(c, oov_mask=oov_mask)
            outputs = self.bert_model(inputs)  # (bs * l) x (l+2) x d
        output_vectors = outputs[:,1:-1,:]    #bs x l x d

        numerator_rolled = output_vectors.view(1, 1, bs, l, self.dim).expand(*w.size()).unsqueeze(-2).matmul(
            w.unsqueeze(-1))
        numerator = numerator_rolled.view(*w.size()[:-1],1)

        vocab_vecs = self.bert_model.bert.embeddings.word_embeddings.weight    # N x d
        if subsamples <= 0:
            logits = output_vectors.matmul(vocab_vecs.t())          # bs x l x N
            denominator = logits.logsumexp(dim=-1)
        else:
            voc_size = vocab_vecs.size(0)
            samples = torch.randint(voc_size, (subsamples,))
            logits = output_vectors.matmul(vocab_vecs[samples,:].t())
            denominator = logits.logsumexp(dim=-1) + np.log(voc_size/subsamples)

        return numerator - denominator.view(1, 1, bs, l, 1)

    def maximum_likelihood(self, c, k=5, mask=None, subsamples=-1, return_logprobs=False):
        bs, l = c.size()
        inputs = self._preprocess_bert(c)
        outputs = self.bert_model(inputs)  # (bs * l) x (l+2) x d
        output_vectors = outputs[:, 1:-1, :]  # (bs * l) x l x d

        vocab_vecs = self.bert_model.bert.embeddings.word_embeddings.weight
        dists = output_vectors.matmul(vocab_vecs.t()) #/ vocab_vecs.norm(dim=1)

        v, i = dists.topk(k, dim=-1)
        if return_logprobs:
            denominator = dists.logsumexp(dim=-1, keepdim=True)
            return v-denominator, i
        else:
            return i



class W2VBertLikelihoodNaive(LikelihoodModel):
    def __init__(self, bert, decoder, tokenizer, w2v):
        super().__init__()
        self.bert = bert
        self.decoder = decoder
        self.tokenizer = tokenizer

        self.w2v = w2v
        self.vectors = torch.Tensor(self.w2v.wv.vectors)
        self.dim = self.vectors.size(1)
        w2i, i2w = vocab_dicts(w2v)
        self.index_dict = make_perm_from_dict(i2w, tokenizer.vocab, tokenizer.unk_token_id)

        for param in self.bert.parameters():
            param.requires_grad = False
        for param in self.decoder.parameters():
            param.requires_grad = False

        if use_cuda:
            self.bert = self.bert.cuda()
            self.decoder = self.decoder.cuda()
            self.vectors = self.vectors.cuda()
            self.index_dict = self.index_dict.cuda()

    def _preprocess_bert(self, c):
        bs, l = c.size()
        inputs = c.unsqueeze(1).repeat(1, l, 1)  # bs x l x l
        input_mask = torch.eye(l).view(1, l, l).expand(bs, l, l).bool()  #
        if use_cuda:
            input_mask = input_mask.cuda()
        inputs.masked_fill_(input_mask, self.tokenizer.mask_token_id)
        inputs_masked = inputs.view(-1, l)
        cls = torch.ones(bs * l, 1).long() * self.tokenizer.cls_token_id
        sep = torch.ones(bs * l, 1).long() * self.tokenizer.sep_token_id
        if use_cuda:
            cls = cls.cuda()
            sep = sep.cuda()
        processed_inputs = torch.cat([cls, inputs_masked, sep], 1)
        return processed_inputs

    def forward(self, c, w, mask=None):
        k, bs, l = w.size()
        converted_c = self.index_dict[c]
        #converted_w = self.index_dict[w]
        inputs = self._preprocess_bert(converted_c)
        outputs = self.bert(inputs)
        outputs_trunc = outputs[:, 1:-1, :]  # (bs * l) x l x d
        outputs_unrolled = outputs_trunc.reshape(bs, l, l, -1)  # bs x l x l x d
        outputs_rolled = outputs_unrolled.reshape(bs, l * l, -1)  # bs x (l x l) x d
        inds = torch.eye(l).view(-1).nonzero().squeeze(-1)
        if use_cuda:
            inds = inds.cuda()
        outputs_collapsed = torch.index_select(outputs_rolled, 1, inds)  # bs x l x d

        scores = self.decoder(outputs_collapsed)  # bs x l x N
        #logprobs = F.log_softmax(scores, dim=-1)  # bs x l x N
        #logprobs_expanded = logprobs.unsqueeze(0).expand(k, *logprobs.size())
        #logprobs_collapsed = torch.gather(logprobs_expanded, -1, converted_w.unsqueeze(-1))
        logprobs_collapsed = torch.gather(scores, -1, w.permute(1,2,0)) #bs x l x k
        return logprobs_collapsed.permute(2,0,1)


    def maximum_likelihood(self, c, k=5, mask=None, subsamples=-1, return_logprobs=False):
        bs, l = c.size()
        inputs = self._preprocess_bert(c)
        outputs = self.bert_model(inputs)  # (bs * l) x (l+2) x d
        outputs_trunc = outputs[:, 1:-1, :]  # (bs * l) x l x d
        outputs_unrolled = outputs_trunc.reshape(bs, l, l, self.dim)  # bs x l x l x d
        outputs_rolled = outputs_unrolled.reshape(bs, -1, self.dim)  # ba x (l x l) x d

        inds = torch.eye(l).view(-1).nonzero().squeeze(-1)
        if use_cuda:
            inds = inds.cuda()
        output_vectors = torch.index_select(outputs_rolled, 1, inds)  # bs x l x d

        vocab_vecs = self.vectors
        dists = output_vectors.matmul(vocab_vecs.t()) #/ vocab_vecs.norm(dim=1)

        v, i = dists.topk(k, dim=-1)
        if return_logprobs:
            denominator = dists.logsumexp(dim=-1, keepdim=True)
            return v-denominator, i
        else:
            return i


class W2VBertLikelihood(LikelihoodModel):
    def __init__(self, bert, tokenizer, w2v):
        super().__init__()
        self.bert_model = bert
        self.tokenizer = tokenizer

        self.w2v = w2v
        self.vectors = torch.Tensor(self.w2v.wv.vectors)
        self.dim = self.vectors.size(1)
        w2i, i2w = vocab_dicts(w2v)
        self.index_dict = make_perm_from_dict(i2w, tokenizer.vocab, tokenizer.unk_token_id)

        for param in self.bert_model.parameters():
            param.requires_grad = False

        if use_cuda:
            self.bert_model = self.bert_model.cuda()
            self.vectors = self.vectors.cuda()
            self.index_dict = self.index_dict.cuda()

    def _preprocess_bert(self, c, vector_input=False, oov_mask=None):
        # c is bs x l,

        converted_c = self.index_dict[c]

        bs, l = c.size()
        inputs = converted_c.unsqueeze(1).repeat(1, l, 1)  # bs x l x l
        input_mask = torch.eye(l).view(1, l, l).expand(bs, l, l).bool()  #
        if use_cuda:
            input_mask = input_mask.cuda()
        inputs.masked_fill_(input_mask, self.tokenizer.mask_token_id)
        inputs_masked = inputs.view(-1, l)
        cls = torch.ones(bs * l, 1).long() * self.tokenizer.cls_token_id
        sep = torch.ones(bs * l, 1).long() * self.tokenizer.sep_token_id
        if use_cuda:
            cls = cls.cuda()
            sep = sep.cuda()
        processed_inputs = torch.cat([cls, inputs_masked, sep], 1)
        return processed_inputs

    def forward(self, c, w, mask=None, subsamples=5000, vector_input=False, oov_mask=None):
        if vector_input:
            bs, l, d = c.size()
            inputs = self._preprocess_bert(c, vector_input=True, oov_mask=oov_mask)
            outputs = self.bert_model(inputs_embeds=inputs)  #(bs * l) x (l+2) x d
        else:
            bs, l = c.size()
            inputs = self._preprocess_bert(c, oov_mask=oov_mask)
            outputs = self.bert_model(inputs)  # (bs * l) x (l+2) x d
        outputs_trunc = outputs[:,1:-1,:]    #(bs * l) x l x d
        outputs_unrolled = outputs_trunc.reshape(bs, l, l, self.dim)   # bs x l x l x d
        outputs_rolled = outputs_unrolled.reshape(bs, -1, self.dim)    # ba x (l x l) x d

        inds = torch.eye(l).view(-1).nonzero().squeeze(-1)
        if use_cuda:
            inds = inds.cuda()
        output_vectors = torch.index_select(outputs_rolled, 1, inds)    # bs x l x d

        numerator_rolled = output_vectors.view(1, 1, bs, l, self.dim).expand(*w.size()).unsqueeze(-2).matmul(
            w.unsqueeze(-1))
        numerator = numerator_rolled.view(*w.size()[:-1],1)

        vocab_vecs = self.vectors    # N x d
        if subsamples <= 0:
            logits = output_vectors.matmul(vocab_vecs.t())          # bs x l x N
            denominator = logits.logsumexp(dim=-1)
        else:
            voc_size = vocab_vecs.size(0)
            samples = torch.randint(voc_size, (subsamples,))
            logits = output_vectors.matmul(vocab_vecs[samples,:].t())
            denominator = logits.logsumexp(dim=-1) + np.log(voc_size/subsamples)

        return numerator - denominator.view(1, 1, bs, l, 1)


    def maximum_likelihood(self, c, k=5, mask=None, subsamples=-1, return_logprobs=False):
        bs, l = c.size()
        inputs = self._preprocess_bert(c)
        outputs = self.bert_model(inputs)  # (bs * l) x (l+2) x d
        outputs_trunc = outputs[:, 1:-1, :]  # (bs * l) x l x d
        outputs_unrolled = outputs_trunc.reshape(bs, l, l, self.dim)  # bs x l x l x d
        outputs_rolled = outputs_unrolled.reshape(bs, -1, self.dim)  # ba x (l x l) x d

        inds = torch.eye(l).view(-1).nonzero().squeeze(-1)
        if use_cuda:
            inds = inds.cuda()
        output_vectors = torch.index_select(outputs_rolled, 1, inds)  # bs x l x d

        vocab_vecs = self.vectors
        dists = output_vectors.matmul(vocab_vecs.t()) #/ vocab_vecs.norm(dim=1)

        v, i = dists.topk(k, dim=-1)
        if return_logprobs:
            denominator = dists.logsumexp(dim=-1, keepdim=True)
            return v-denominator, i
        else:
            return i