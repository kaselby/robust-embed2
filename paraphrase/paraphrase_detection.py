import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

import numpy as np
from numpy import dot
from numpy.linalg import norm
import re
from string import punctuation
import math

from sklearn.metrics import roc_auc_score

from utils import *
from preprocessing import synthetic_noise

# preprocess string
def normalize_string(s):
    s = s.lower().strip()
    s = re.sub('\!+', '!', s)
    s = re.sub('\,+', ',', s)
    s = re.sub('\?+', '?', s)
    s = re.sub('\.+', '.', s)    
    #s = re.sub("[^0-9a-zA-Z.!?,'']+", ' ', s)
    for p in punctuation:
      if p != "'":
        s = s.replace(p, " " + p + " ")       
    s = re.sub(' +', ' ', s)
    s = s.strip()
    return s

def split_oov_pairs(pairs, labels, t2i):
    oov_pairs = []
    oov_labels = []
    iv_pairs = []
    iv_labels = []
    for pair, label in zip(pairs, labels):
        oov=False
        for seq in pair:
            for word in seq:
                if not word in t2i:
                    oov = True
        if oov:
            oov_pairs.append(pair)
            oov_labels.append(label)
        else:
            iv_pairs.append(pair)
            iv_labels.append(label)
    return (oov_pairs, oov_labels), (iv_pairs, iv_labels)

'''
# numpy version
def get_pair_sim(s1, s2, sim_fnc):
    s1_embeddings = []
    s2_embeddings = []
    s1_words = s1.split()
    s2_words = s2.split()
    for word in s1_words:
        s1_embeddings.append(sim_fnc(word, s1_words))
    for word in s2_words:
        s2_embeddings.append(sim_fnc(word, s2_words))
    s1_avg_embedding = np.mean(s1_embeddings)
    s2_avg_embedding = np.mean(s2_embeddings)
    cos_sim = dot(s1_avg_embedding, s2_avg_embedding)/(norm(s1_avg_embedding)*norm(s2_avg_embedding))
    return cos_sim
  
  
# torch version 
def get_pair_sim_torch(s1, s2, sim_fnc):
    s1_embeddings = []
    s2_embeddings = []
    s1_words = s1.split()
    s2_words = s2.split()
    for word in s1_words:
        s1_embeddings.append(sim_fnc(word, s1_words))
    for word in s2_words:
        s2_embeddings.append(sim_fnc(word, s2_words))
    s1_avg_embedding = torch.mean(s1_embeddings, 0)
    s2_avg_embedding = torch.mean(s2_embeddings, 0)
    cos_sim = torch.dot(s1_avg_embedding, s2_avg_embedding)/(torch.norm(s1_avg_embedding)*torch.norm(s2_avg_embedding))
    return cos_sim    
    

# get corpus accuracy given pairs_lst which is a list of lists, 
#   with inner lists being of format [sentence1, sentence2, 1 or 0]
def get_corpus_acc(pairs_lst, sim_fnc, preprocess_fnc):
    total_counter = 0
    correct_counter = 0
    for pair in pairs_lst:
        total_counter += 1
        clean_s1 = preprocess_fnc(pair[0])
        clean_s2 = preprocess_fnc(pair[1])
        pair_sim = get_pair_sim_torch(clean_s1, clean_s2, sim_fnc)
        if pair_sim >= 0.5 and pair[2] == 1:
            correct_counter += 1
        elif pair_sim < 0.5 and pair[2] == 0:
            correct_counter += 1
    corpus_acc = correct_counter / total_counter
    return corpus_acc
'''


def load_msr(filename, noise_probability=0):
    pairs = []
    labels = []
    infile = open(filename, 'r')
    next(infile)
    for line in infile:
        label, _, _, s1, s2 = line.split("\t")
        s1_clean = normalize_string(s1).split(" ")
        s2_clean = normalize_string(s2).split(" ")
        # synthetic noise injection
        if noise_probability > 0:
            s1_clean = synthetic_noise.process_sentence(s1_clean, noise_probability)
            s2_clean = synthetic_noise.process_sentence(s2_clean, noise_probability)
        pairs.append((s1_clean,s2_clean))
        labels.append(int(label))
    return pairs, labels


def load_twitter(filename, train=True, noise_probability = 0):
    pairs = []
    labels = []
    infile = open(filename, 'r')
    next(infile)
    for line in infile:
        _, _, s1, s2, score, _, _,  = line.split("\t")
        if train:
            scores = score[1:-1].split(',')
            if int(scores[0]) != 2:
                s1_clean = normalize_string(s1).split(" ")
                s2_clean = normalize_string(s2).split(" ")
                # synthetic noise injection
                if noise_probability > 0:
                    s1_clean = synthetic_noise.process_sentence(s1_clean, noise_probability)
                    s2_clean = synthetic_noise.process_sentence(s2_clean, noise_probability)
                pairs.append((s1_clean, s2_clean))
                label = 1 if int(scores[0]) in [3, 4, 5] else 0
                labels.append(label)
        else:
            if int(score) != 3:
                s1_clean = normalize_string(s1).split(" ")
                s2_clean = normalize_string(s2).split(" ")
                # synthetic noise injection
                if noise_probability > 0:
                    s1_clean = synthetic_noise.process_sentence(s1_clean, noise_probability)
                    s2_clean = synthetic_noise.process_sentence(s2_clean, noise_probability)
                pairs.append((s1_clean,s2_clean))
                label = 1 if int(score) in [4,5] else 0
                labels.append(label)
    return pairs, labels


def batch_seqs(pair_list, labels, batch_size, batch_fct, *args, **kwargs):
    def generate():
        N = len(pair_list)
        n_batches = math.ceil(N / batch_size)
        p = torch.randperm(N)

        for i in range(n_batches):
            min_j = i*batch_size
            max_j = min((i+1)*batch_size, N)
            seqs1, seqs2 = zip(*[pair_list[p[j]] for j in range(min_j, max_j)])
            b1 = batch_fct(seqs1, *args, **kwargs)
            b2 = batch_fct(seqs2, *args, **kwargs)
            targets = torch.FloatTensor([labels[p[j]] for j in range(min_j, max_j)])
            if use_cuda:
                targets = targets.cuda()
            #batches.append(((b1,b2),targets))
            yield (b1,b2), targets
    return generate

def paraphrase_detection(inputs, embed_fct, print_every=20, **kwargs):
    predicted = []
    true = []
    total = 0
    for ((s1, s2), t) in inputs:
        bs = t.size(0)
        e1, e2 = embed_fct(s1, **kwargs), embed_fct(s2, **kwargs)                                   # bs x l x d
        labels = _mean_cosine(e1, e2)
        acc = (labels.long() == t).sum().item()
        predicted += labels.tolist()
        true += t.tolist()
        total += bs
        if int(total/bs) % print_every == 0:
            print("Batches Processed:", total/bs,"\nBatch Accuracy:", acc/bs,"\n")
    return predicted, true

def _mean_cosine(e1, e2):
    e1_avg = e1.mean(1)
    e2_avg = e2.mean(1)
    e1_avg /= e1_avg.norm(dim=-1, keepdim=True)
    e2_avg /= e2_avg.norm(dim=-1, keepdim=True)
    sim = e1_avg.unsqueeze(1).matmul(e2_avg.unsqueeze(-1)).view(-1)
    labels = (torch.abs(sim) > 0.5)
    return labels

def embed_robust(model):
    def embed(batch, **kwargs):
        (neighbours, costs), mask = batch
        return model(neighbours,costs,mask=mask, **kwargs)[0]
    return embed

def embed_w2v(model):
    def embed(batch):
        indices,mask = batch
        #indices,_,mask = batch
        return model.W[indices[:,:],:] * mask.unsqueeze(-1).float()
    return embed

class CosineClassifier(nn.Module):
    def __init__(self, embed, threshold=0.85):
        super().__init__()
        self.embed = embed
        self.threshold=threshold

    def forward(self, b1, b2, l1, l2, eps=1e-8, **kwargs):
        e1 = self.embed(b1, **kwargs)
        e2 = self.embed(b2, **kwargs)

        l1_mask = mask_lengths(l1, max_l=e1.size(1)).unsqueeze(-1)
        l2_mask = mask_lengths(l2, max_l=e2.size(1)).unsqueeze(-1)

        e1_sum = (e1 * l1_mask).sum(dim=1)
        e2_sum = (e2 * l2_mask).sum(dim=1)

        norm1 = e1_sum.norm(dim=-1) + eps
        norm2 = e2_sum.norm(dim=-1) + eps

        cosine = e1_sum.unsqueeze(1).matmul(e2_sum.unsqueeze(-1)).squeeze(-1).squeeze(-1)  / \
                 norm1 / norm2

        return cosine

class LSTMClassifier(nn.Module):
    def __init__(self, input_size, hidden_size, embed, n_layers=1):
        super().__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.embed = embed
        self.n_layers = n_layers
        self.lstm = nn.LSTM(input_size, hidden_size, num_layers=1, batch_first=True)
        self.out = nn.Sequential(
            #nn.Linear(hidden_size * 2, hidden_size),
            #nn.ReLU(),
            nn.Linear(hidden_size * 2, 1)
        )

        if use_cuda:
            self.lstm = self.lstm.cuda()
            self.out = self.out.cuda()

    def forward(self, b1, b2, l1, l2, **kwargs):
        e1 = self.embed(b1, **kwargs)
        e2 = self.embed(b2, **kwargs)

        sorted_l1, perm1 = l1.sort(0, descending=True)
        sorted_l2, perm2 = l2.sort(0, descending=True)
        sorted_e1 = e1[perm1, :, :]
        sorted_e2 = e2[perm2, :, :]

        packed1 = pack_padded_sequence(sorted_e1, sorted_l1, batch_first=True)
        packed2 = pack_padded_sequence(sorted_e2, sorted_l2, batch_first=True)

        lstm_out_1, (h1, c1) = self.lstm(packed1)
        lstm_out_2, (h2, c2) = self.lstm(packed2)
        #unpacked1 = pad_packed_sequence(lstm_out_1, batch_first=True)
        #unpacked2 = pad_packed_sequence(lstm_out_2, batch_first=True)
        #masked_out_1 = (lstm_out_1 * m1.unsqueeze(-1).float()).sum(dim=1) / m1.float().sum(dim=1, keepdim=True)
        #masked_out_2 = (lstm_out_2 * m2.unsqueeze(-1).float()).sum(dim=1) / m2.float().sum(dim=1, keepdim=True)

        _, unperm1 = perm1.sort(0)
        _, unperm2 = perm2.sort(0)
        unsorted1 = h1[0, unperm1, :]
        unsorted2 = h2[0, unperm2, :]

        logits = self.out(torch.cat((unsorted1, unsorted2), dim=-1))

        return logits.squeeze(1)
        #max1 = out1.sum(dim=1)
        #max2 = out2.sum(dim=1)
        #normed1 = max1 / max1.norm(dim=-1, keepdim=True)
        #normed2 = max2 / max2.norm(dim=-1, keepdim=True)
        #F.sigmoid(normed1.unsqueeze(1).matmul(normed2.unsqueeze(-1)).view(-1))


def make_lstm_classifier(input_size, hidden_size, embed, lr):
    model = LSTMClassifier(input_size, hidden_size, embed)
    opt = optim.Adam(model.parameters(), lr=lr)
    return model, opt


def train_paraphrase(model, optim, train_data, test_data, epochs, print_every=50, eval_every=3, clip=50, save_checkpoint=None, **kwargs):
    loss_fct = nn.BCEWithLogitsLoss()
    best_val = float('inf')
    best_metrics = ()
    for i in range(epochs):
        total = 0
        epoch_loss = 0
        #batches =  batch_seqs(pairs, targets, bs, batch_fct, *args, **kwargs)
        for (((b1,l1), (b2,l2)), t) in train_data():
            optim.zero_grad()
            bs = t.size(0)
            #e1, e2 = embed(s1, **kwargs), embed(s2, **kwargs)  # bs x l x d
            probs = model(b1, b2, l1, l2, **kwargs)
            #loss = -(torch.log(probs).dot(t) + torch.log(1-probs).dot(1-t)) / bs
            loss = loss_fct(probs, t)
            loss.backward()
            nn.utils.clip_grad_norm(model.parameters(), clip)
            optim.step()
            epoch_loss += loss.item()
            total += 1
            if total % print_every == 0:
                print("Batches Processed:", total, "\nBatch Loss:", loss.item(), "\n")
        print("Epoch:", i, "\nAverage Loss:", epoch_loss/total, "\n")
        if i % eval_every == 0:
            val_loss, (acc, prec, rec, f1) = eval_paraphrase(model, test_data, **kwargs)
            print("Validation Loss:", val_loss)
            print("Accuracy:", acc, "\tPrecision:", prec, "\tRecall:", rec, "\tF1 Score:", f1, "\n")

            if val_loss < best_val:
                best_val = val_loss
                best_metrics = (acc, prec, rec, f1)
                if save_checkpoint is not None:
                    torch.save(model.state_dict(), save_checkpoint)
    return best_val, best_metrics

def eval_paraphrase(classifier, batch_gen, print_every=-1, **kwargs):
    predicted = []
    true = []
    total = 0
    total_loss = 0
    loss_fct = nn.BCEWithLogitsLoss()
    with torch.no_grad():
        #batches = batch_seqs(pairs, targets, bs, batch_fct, *args, **kwargs)
        for (((b1, l1), (b2, l2)), t) in batch_gen():
            probs = classifier(b1, b2, l1, l2, **kwargs)
            #loss = loss_fct(probs, t)
            #total_loss += loss.item()
            predicted += probs.tolist()
            true += t.tolist()
            total += 1
            if print_every > 0 and total % print_every == 0:
                print("Eval Batches Processed:", total, "\n")
    predicted = torch.FloatTensor(predicted)
    true = torch.LongTensor(true)
    #acc, prec, rec, f1 = get_class_scores(predicted, true)
    auc = roc_auc_score(true, predicted)

    return total_loss / total, auc


