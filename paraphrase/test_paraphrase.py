from bayes_lev_embed import RobustEmbeddings, VectorEmbeddings
from paraphrase.paraphrase_detection import *
from preprocess import *

torch.backends.cudnn.deterministic=True

bs = 1
k = 10
n = 20
hs = 128
lr = 0.0001

w2v_file = "./data/reddit_10m_word_level_processed_3_word2vec_model.w2v"
vocab_file = "./data/reddit_vocab_symspell.txt"
cost_file = "data/dists_matrix.pt"
neighbour_file = "data/neighbours_matrix.pt"
msr_train = "paraphrase/MSRParaphraseCorpus/msr_paraphrase_train.txt"
msr_test = "paraphrase/MSRParaphraseCorpus/msr_paraphrase_test.txt"
twitter_train = "paraphrase/SemEval-PIT2015/train.data"
twitter_test = "paraphrase/SemEval-PIT2015/test.data"

save_folder = "./paraphrase/models"


w2v, ss = load_vocab(vocab_file, w2v_file)
emb = RobustEmbeddings.from_w2v(w2v)
cost_matrix = torch.load(cost_file)
nn_matrix = torch.load(neighbour_file)

train_pairs, train_labels = load_msr(msr_train)
test_pairs, test_labels = load_msr(msr_test)

(oov_pairs, oov_labels), (iv_pairs, iv_labels) = split_oov_pairs(test_pairs, test_labels, emb.voc.t2i)

emb = VectorEmbeddings.from_w2v(w2v, max_vocab=20000)
model = CosineClassifier(emb)

