from transformers import BertModel, BertTokenizer, BertForMaskedLM, BertConfig

from bayes_lev_embed import RobustEmbeddings, RobustEmbeddingsNaive, VectorEmbeddings
from bert_model import BertForContinuousMaskedLM, BertForDensityEstimation, W2VBertForMaskedLM
from preprocess import *
from entailment.entailment_detection import *

import tabulate
def summarize_seq(seq, pivot, emb, ss, m=5, n=20, k=5):
    with torch.no_grad():
        posterior_samples, (prior_knearest, likelihood_knearest, posterior_knearest), (
        (pr_pr, pr_l, pr_po), (l_pr, l_l, l_po), (po_pr, po_l, po_po)) = emb.analyze_seq(seq, ss, pivot, m=m, n=n, k=k,
                                                                                         only_oov=False)
    prior_headers = ["K-Nearest Prior Words", "Prior Probability", "Likelihood Probability", "Posterior Probability"]
    prior_candidates = emb.indices_to_words(prior_knearest)
    prior_table = [[' '.join(prior_candidates[i]), pr_pr[i].item(), pr_l[i].item(), pr_po[i].item()] for i in range(m)]

    likelihood_headers = ["Top-K Likelihood Words", "Prior Probability", "Likelihood Probability", "Posterior Probability"]
    likelihood_candidates = emb.indices_to_words(likelihood_knearest)
    likelihood_table = [[likelihood_candidates[i], l_pr[i].item(), l_l[i].item(), l_po[i].item()] for i in range(m)]

    posterior_headers = ["Top-K Posterior Words", "Prior Probability", "Likelihood Probability", "Posterior Probability"]
    posterior_candidates = emb.indices_to_words(posterior_knearest)
    posterior_table = [[posterior_candidates[i], po_pr[i].item(), po_l[i].item(), po_po[i].item()] for i in range(m)]

    print(tabulate.tabulate(prior_table, prior_headers, tablefmt="fancy_grid"))
    print(tabulate.tabulate(likelihood_table, likelihood_headers, tablefmt="fancy_grid"))
    print(tabulate.tabulate(posterior_table, posterior_headers, tablefmt="fancy_grid"))

    return posterior_knearest

snli_train = "entailment/snli_1.0/snli_1.0_train.jsonl"
snli_test = "entailment/snli_1.0/snli_1.0_test.jsonl"
snli_test_rove = "entailment/snli_1.0/test.txt"

google_w2v_file = "data/GoogleNews-vectors-negative300.bin.gz"
google_w2v_vocab = "data/google_vocab_symspell.txt"

w2v_bert_file='data/w2v-bert'

w2v, ss = load_vocab(google_w2v_vocab, google_w2v_file, keyedvectors=True)

w2v = KeyedVectors.load_word2vec_format(google_w2v_file, binary=True, limit = 50000)
bert_tokenizer = BertTokenizer.from_pretrained(w2v_bert_file)
bert_config = BertConfig.from_pretrained(w2v_bert_file)
bert_saved_model = W2VBertForMaskedLM.from_pretrained(w2v_bert_file, torch.Tensor(w2v.wv.vectors), config=bert_config)
bert_model = BertForDensityEstimation.from_w2vbfmlm(bert_saved_model)
emb = RobustEmbeddings.from_w2v_bert(bert_model, bert_tokenizer, w2v)

test_pairs, test_labels = load_snli(snli_test, noise_probability=0)

