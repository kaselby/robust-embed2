from translation.translate_kira import *
from bayes_lev_embed import *
import argparse

from transformers import BertModel, BertTokenizer, BertForMaskedLM
from bert_model import BertForContinuousMaskedLM, BertForDensityEstimation

mtnt_train = "translation/MTNT/train/train.en-fr.tsv"
mtnt_test = "translation/MTNT/test/test.en-fr.tsv"
wmt_train = "translation/WMT/training/europarl-v7.fr-en"
wmt_test = "translation/WMT/dev/news-test2008"

en_vocab_file = "data/clean_news_vocab_symspell.txt"
en_w2v_file = "data/clean_news_word2vec_model.w2v"
fr_vocab_file = "data/french_vocab_symspell.txt"
fr_w2v_file = "data/french_word2vec_model.w2v"


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--sentence', help='sentence', type=str, default='')
    parser.add_argument('--model_file', type = str, default = 'saved_model.pt')

    parser.add_argument('--bs', help = 'batch size', type = int, default = 64)
    parser.add_argument('--hs', help = 'hidden size', type = int, default = 2048)
    parser.add_argument('--k', help = 'k', type = int, default = 10)
    parser.add_argument('--heads', help='attn heads', type=int, default=5)
    parser.add_argument('--data_type', choices = ('wmt', 'mtnt', 'ted'), type = str, default = 'wmt')
    parser.add_argument('--model_type', choices = ('w2v', 'robust'), type = str, default = 'w2v')
    parser.add_argument('--likelihood', choices = ('w2v', 'bert'), type = str, default = 'w2v')
    parser.add_argument('--noise', help = 'level of synthetic noise to inject', type = float, default = 0)

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    en_w2v, en_ss = load_vocab(en_vocab_file, en_w2v_file)
    fr_w2v, fr_ss = load_vocab(fr_vocab_file, fr_w2v_file)

    if args.likelihood == 'bert':
        en_vocab_file = 'data/bert_vocab.txt'
    en_w2v, en_ss = load_vocab(en_vocab_file, en_w2v_file)
    fr_w2v, fr_ss = load_vocab(fr_vocab_file, fr_w2v_file)

    if args.likelihood == 'w2v':
        if args.model_type == 'w2v':
            print('embedding: w2v')
            en_emb = VectorEmbeddings.from_w2v(en_w2v, trainable_tokens=(SOS, EOS))
        else:
            print('embedding: robust')
            en_emb = RobustEmbeddings.from_w2v(en_w2v, trainable_tokens=(SOS, EOS))
        fr_emb = VectorEmbeddings.from_w2v(fr_w2v, trainable_tokens=(SOS, EOS))

    elif args.likelihood == 'bert':
        pretrained_bert = 'bert-base-uncased' # 'albert-base-v1'
        bert_tokenizer = BertTokenizer.from_pretrained(pretrained_bert)
        bert_saved_model = BertForMaskedLM.from_pretrained(pretrained_bert)
        bert_model = BertForDensityEstimation.from_bfcmlm(bert_saved_model)
        if args.model_type == 'w2v':
            print('embedding: w2v')
            en_emb = VectorEmbeddings.from_bert(bert_model, bert_tokenizer)
        else:
            print('embedding: robust')
            en_emb = RobustEmbeddings.from_bert(bert_model, bert_tokenizer)
        bert_multi_tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-uncased')
        bert_multi_model = BertModel.from_pretrained('bert-base-multilingual-uncased')
        fr_emb = VectorEmbeddings.from_bert(bert_multi_model, bert_multi_tokenizer)

    saved_model = args.model_file
    model, optim = create_model(en_emb, fr_emb, args.hs, 1, attn_heads=args.heads)
    checkpoint = torch.load(saved_model)
    model.load_state_dict(checkpoint['model_state_dict'])

    if len(args.sentence) > 0:
        sentence = ('<sos> ' + normalize_string_en(args.sentence) + ' <eos>').split(' ')
        batch, batch_len = make_batch_w2v([sentence], en_emb.voc.t2i)

        with torch.no_grad():
            output = model(batch, batch_len)

        output_indices = output.argmax(dim = -1)
        prediction = decode_words(fr_emb.voc, output_indices)
        print(prediction)

    else:
        if args.data_type == 'wmt':
            test_pairs = load_WMT(wmt_test, noise_probability = args.noise)
        else:
            test_pairs = load_mtnt(mtnt_test, noise_probability = args.noise)
        if args.model_type == 'w2v':
            batch = batch_seqs(test_pairs, args.bs, make_batch_w2v, en_emb.voc, fr_emb.voc)
            loss, bleu = eval_translate(model, batch)
        else:
            batch = batch_seqs(test_pairs, args.bs, make_batch, en_emb.voc, fr_emb.voc, en_ss, args.k)
            loss, bleu = eval_translate(model, batch)
        print(loss, bleu)
