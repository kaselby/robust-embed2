import argparse
import os
from transformers import BertModel, BertTokenizer, BertForMaskedLM, BertConfig

from bayes_lev_embed import RobustEmbeddings, RobustEmbeddingsNaive, VectorEmbeddings
from entailment.entailment_detection import *
from preprocess import *
from bert_model import BertForContinuousMaskedLM, BertForDensityEstimation, W2VBertForMaskedLM
from preprocessing.synthetic_noise import process_sentence, noise_generator

#torch.backends.cudnn.deterministic=True

#w2v_file = "./data/reddit_10m_word_level_processed_3_word2vec_model.w2v"
#vocab_file = "./data/reddit_vocab_symspell.txt"
base_w2v_file = "data/clean_news_word2vec_model.w2v"
base_vocab_file = "data/clean_news_vocab_symspell.txt"
bert_vocab_file = "data/bert_vocab_symspell.txt"
tinybert_vocab_file = "data/tinybert_vocab_symspell.txt"
tinybert_dir = "data/TinyBERT_4_312"

noisy_snli_dir = "entailment/noisy_snli"

#w2v_file = "data/GoogleNews-vectors-negative300.bin"
#vocab_file = "preprocessing/google_w2v_vocab.txt"
#cost_file = "data/news_dists_matrix.pt"
#neighbour_file = "data/news_neighbours_matrix.pt"
snli_train = "entailment/snli_1.0/snli_1.0_train.jsonl"
snli_test = "entailment/snli_1.0/snli_1.0_test.jsonl"
snli_test_rove = "entailment/snli_1.0/test.txt"
google_w2v_file = "data/GoogleNews-vectors-negative300.bin.gz"
google_w2v_vocab = "data/google_vocab_symspell.txt"

w2v_bert_file='data/w2v-bert'

b2v_path = "data/b2v_model.bin"

save_folder = "./models"

class RobustNaiveModel(nn.Module):
    def __init__(self, emb, ss, k=20):
        super().__init__()
        self.emb = emb
        self.k=k
        self.ss = ss

    def forward(self, seq, posterior_samples=-1, trivial=False, **kwargs):
        b, l = make_batch([seq], self.emb.voc, self.ss, self.k, **kwargs)
        predictions = self.emb(b, return_indices=True, trivial=trivial)
        return [self.emb.voc.i2t[predictions[0,i].item()] for i in range(predictions.size(1))]

class RobustModel(nn.Module):
    def __init__(self, emb, ss, k=20):
        super().__init__()
        self.emb = emb
        self.k = k
        self.ss = ss

    def forward(self, seq, posterior_samples=-1, trivial=False, **kwargs):
        b, l = make_batch([seq], self.emb.voc, self.ss, self.k, **kwargs)
        out = self.emb(b)
        predictions=self.emb.get_nearest(out, k=1)
        return [self.emb.voc.i2t[predictions[0, i].item()] for i in range(predictions.size(1))]

class LevModel(nn.Module):
    def __init__(self, ss):
        super().__init__()
        self.ss = ss

    def forward(self, seq, trivial=False):
        out_seq=[]
        for word in seq:
            topk = ss.lookup(word, Verbosity.ALL)
            top1 = topk[0].term if len(topk) > 0 else ""
            out_seq.append(top1)
        return out_seq

def eval_accuracy(model, seqs, noise_level, **kwargs):
    correct = 0
    total = 0
    for seq in seqs:
        if len(seq) > 0:
            noisy_seq = process_sentence(seq, noise_level)
            predictions = model(noisy_seq, **kwargs)
            for i in range(len(seq)):
                if seq[i] == predictions[i]:
                    correct += 1
                total += 1

    print("Accuracy:", correct / total)
    return correct / total


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--bs', help='batch size', type=int, default=32)
    parser.add_argument('--hs', help='hidden size', type=int, default=256)
    parser.add_argument('--k', help='k', type=int, default=10)
    parser.add_argument('--model_file', type=str, default=None)
    parser.add_argument('--data_type', choices=('snli'), type=str, default='snli')
    parser.add_argument('--model_type', choices=('w2v', 'robust', 'robust-naive', 'fasttext', 'b2v', 'lev'), type=str, default='w2v')
    parser.add_argument('--likelihood', choices=('w2v', 'bert', 'w2v-bert'), type=str, default='w2v')
    parser.add_argument('--bert_model', choices=('base', 'tiny'), type=str, default='base')
    parser.add_argument('--checkpoint_file', type=str, default=None)
    parser.add_argument('--classifier', choices=('cosine', 'lstm'), type=str, default='cosine')
    parser.add_argument('--noise', help = 'level of synthetic noise to inject', type=float, default=0)
    parser.add_argument('--spellcheck', help='level of synthetic noise to inject', choices=('none', 'lev'), type=str, default='none')
    parser.add_argument('--max_vocab', help='max vocab size', type=int, default=-1)
    parser.add_argument('--max_pairs', help='max vocab size', type=int, default=-1)
    parser.add_argument('--iters', help='likelihood context iterations', type=int, default=1)
    parser.add_argument('--tau', help='cost softmax temperature', type=float, default=1)
    parser.add_argument('--variance_file', type=str, default=None)
    parser.add_argument('--train_data', type=str, default=None)
    parser.add_argument('--test_data', type=str, default=None)
    parser.add_argument('--trivial', action='store_true')
    parser.add_argument('--pregen', type=str, default='')
    parser.add_argument('--trials', type=int, default=1)

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    if args.model_type == 'w2v':
        vocab_file = google_w2v_vocab
        w2v_file = google_w2v_file
        kv = True
    else:
        if args.likelihood == 'w2v':
            kv = False
            w2v_file = base_w2v_file
            vocab_file = base_vocab_file
        else: #args.likelihood == "bert" or args.likelihood == "w2v-bert":
            vocab_file = bert_vocab_file
            w2v_file = google_w2v_file
            kv = True



    w2v, ss = load_vocab(vocab_file, w2v_file, keyedvectors=kv)
    #cost_matrix = torch.load(cost_file)
    #nn_matrix = torch.load(neighbour_file)

    Sigma = None
    if args.variance_file is not None:
        saved_param = torch.load(args.variance_file)
        Sigma = saved_param['embed.Sigma'].abs()

    if len(args.pregen) > 0:
        noise_prefix = str(args.noise) if args.noise > 0 else "0"
        snli_test = os.path.join(noisy_snli_dir, args.pregen, noise_prefix + "_snli_1.0_test.jsonl")
        test_pairs, test_labels = load_snli(snli_test, noise_probability=0, max_pairs=args.max_pairs)
    else:
        if args.train_data is not None:
            snli_train = args.train_data
        if args.test_data is not None:
            snli_test = args.test_data
        if args.data_type == 'snli':
            # train_pairs, train_labels = load_snli(snli_train)
            test_pairs, test_labels = load_snli(snli_test, noise_probability=0, max_pairs=args.max_pairs)
            # test_pairs, test_labels = load_snli_rove(snli_test_rove, noise_probability=args.noise)
        else:
            raise NotImplementedError

    seqs1, seqs2 = zip(*test_pairs)
    all_seqs = seqs1 + seqs2
    """
    if args.model_type == 'robust':
        emb = RobustEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab)
    elif args.model_type == 'fasttext':
        vocab_path = 'data/wiki.en.vec'
        from torchtext import vocab
        voc = vocab.Vectors(vocab_path, max_vectors = 50000)
        emb = VectorEmbeddings.from_fasttext(voc)
    else:
        emb = VectorEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab)
    """

    if args.likelihood == 'w2v':
        if args.model_type == 'w2v':
            emb = VectorEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab)
        elif args.model_type == 'fasttext':
            vocab_path = 'data/wiki.en.vec'
            from torchtext import vocab
            voc = vocab.Vectors(vocab_path, max_vectors = args.max_vocab if args.max_vocab > 0 else 30000)
            emb = VectorEmbeddings.from_fasttext(voc)
        elif args.model_type == 'b2v':
            import fasttext
            b2v = fasttext.load_model(b2v_path)
            emb = VectorEmbeddings.from_bridge2vec(b2v, max_vocab=args.max_vocab)
        elif args.model_type == 'robust-naive':
            emb = RobustEmbeddingsNaive.from_w2v(w2v, max_vocab=args.max_vocab, tau=args.tau)
        else:
            emb = RobustEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab, S=Sigma, tau_cost=args.tau)

    elif args.likelihood == 'bert':
        pretrained_bert = tinybert_dir if args.bert_model == 'tiny' else 'bert-base-uncased'
        bert_tokenizer = BertTokenizer.from_pretrained(pretrained_bert)
        if args.model_type == 'w2v':
            bert_model = BertForMaskedLM.from_pretrained(pretrained_bert)
            emb = VectorEmbeddings.from_bert(bert_model, bert_tokenizer)
        if args.model_type == 'robust-naive':
            bert_model = BertForMaskedLM.from_pretrained(pretrained_bert)
            emb = RobustEmbeddingsNaive.from_bert(bert_model, bert_tokenizer, tau=args.tau)
        else:
            bert_saved_model = BertForMaskedLM.from_pretrained(pretrained_bert)
            bert_model = BertForDensityEstimation.from_bfcmlm(bert_saved_model)
            if args.model_type == 'w2v':
                emb = VectorEmbeddings.from_bert(bert_model, bert_tokenizer)
            else:
                emb = RobustEmbeddings.from_bert(bert_model, bert_tokenizer, S=Sigma, tau_cost=args.tau)

    elif args.likelihood == 'w2v-bert':
        '''
        w2v = KeyedVectors.load_word2vec_format(google_w2v_file, binary=True, limit = 50000)
        bert_tokenizer = BertTokenizer.from_pretrained(w2v_bert_file)
        bert_config = BertConfig.from_pretrained(w2v_bert_file)
        bert_saved_model = W2VBertForMaskedLM.from_pretrained(w2v_bert_file, torch.Tensor(w2v.wv.vectors), config=bert_config)
        bert_model = BertForDensityEstimation.from_w2vbfmlm(bert_saved_model)
        '''
        pretrained_bert = tinybert_dir if args.bert_model == 'tiny' else 'bert-base-uncased'
        bert_tokenizer = BertTokenizer.from_pretrained(pretrained_bert)
        bert_model = BertForMaskedLM.from_pretrained(pretrained_bert)
        if args.model_type == 'robust-naive':
            emb = RobustEmbeddingsNaive.from_w2v_bert(bert_model,
                                                      bert_tokenizer, w2v, tau=args.tau)
        else:
            emb = RobustEmbeddings.from_w2v_bert(bert_model, bert_tokenizer, w2v, S=Sigma, tau_cost=args.tau)

    #if args.spellcheck == 'lev':
     #   test_pairs = [(naive_spellcheck(seq, ss, emb.voc.t2i) for seq in pair) for pair in test_pairs]

    if args.model_type == 'robust':
        model = RobustModel(emb, ss)
    elif args.model_type == 'robust-naive':
        model = RobustNaiveModel(emb, ss)
    elif args.model_type == 'lev':
        model = LevModel(ss)
    else:
        raise NotImplementedError()

    voc = voc = ss._words.keys() if args.model_type == "lev" else emb.voc.t2i

    seqs = [[word for word in seq if word in voc] for seq in all_seqs]

    results = []
    for i in range(args.trials):
        acc = eval_accuracy(model, seqs, args.noise, trivial=(args.trivial and args.model_type != "lev"))
        results.append(acc)
    avg = sum(results) / len(results)

    print("Results: ", str(results), "Average:", str(avg))