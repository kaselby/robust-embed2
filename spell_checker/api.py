"""This file is the api module used for hunspell spellchecker.

This class APIWorker is an interface that given an input sentence, send a get request for the correction using hunspell.

    Example of usage:
    worker = APIWorker()
    corrected_sentence = worker.request(sentence)
"""

import requests
import time

class APIWorker:
    def __init__(self):
        self.url = "https://montanaflynn-spellcheck.p.rapidapi.com/check/"
        self.headers = {
            "x-rapidapi-host": "montanaflynn-spellcheck.p.rapidapi.com",
            "x-rapidapi-key" : "2b8352d192mshe20646afbcbc650p1b9878jsn2519ae9f0c80"
        }

    def request(self, sent):
        data = {
            'text': sent
        }
        exit = False
        while not exit:
            response = requests.get(self.url, data, headers = self.headers)
            try:
                response.raise_for_status()
                exit = True
            except requests.exceptions.HTTPError as err:
                print(err)
                time.sleep(300)
        correct_pkg = response.json()
        return correct_pkg['suggestion']
