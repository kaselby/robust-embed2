"""This file is the scrapper module used for google spellchecker.

This class APIWorker is an interface that given an input sentence, google search this sentence and grab the 'did you mean'
sentence that is corrected by the search engine.

    Example of usage:
    google = GoogleSearch()
    corrected_sentence = google.search(sentence)
"""

import requests
from bs4 import BeautifulSoup
from nltk import word_tokenize
from lxml.html import fromstring
import random

def get_proxies():
    url = 'https://free-proxy-list.net/'
    response = requests.get(url)
    parser = fromstring(response.text)
    proxies = set()
    for i in parser.xpath('//tbody/tr')[:300]:
        #if i.xpath('.//td[7][contains(text(),"yes")]'):
        # Grabbing IP and corresponding PORT
        proxy = ":".join([i.xpath('.//td[1]/text()')[0], i.xpath('.//td[2]/text()')[0]])
        proxies.add(proxy)
    return proxies

class GoogleSearch:
    def __init__(self):
        self.headers = {
            "User-Agent" :
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36",
        }
        self.proxies = get_proxies()

    def get_proxy(self):
        proxy_list = list(self.proxies)
        random.shuffle(proxy_list)
        for proxy in proxy_list:
            yield proxy

    def search(self, sent):
        query = '+'.join(word_tokenize(sent))
        url = 'http://google.com/search?h1=en&q=' + query + "&meta=&gws_rd=ssl"
        proxy = next(self.get_proxy())
        page = requests.get(url, headers=self.headers, proxies = {'http': proxy})
        try:
            page.raise_for_status()
        except requests.exceptions.HTTPError as err:
            raise Exception(err)
        soup = BeautifulSoup(page.text, 'html.parser')
        hook_element = soup.body.find('span', text = 'Showing results for')
        if hook_element is None:
            return sent
        target_container = hook_element.parent()
        correction = target_container[1].text
        return correction
