"""This file is a sentence extractor used to extract sentences from data and form a text file for other spell checker.

The intention of this module is to extract pure sentences from the data and use spell checkers like Grammarly or MS Word
to correct the text file.
Obstacles: Grammarly only allow small text files and MS Word must apply corrections manually one by one

    Example of usage:
    python sent_extractor.py
"""

import sys
sys.path.insert(1, '~/robust-embed2/')

from nltk import word_tokenize
import json
from tqdm import tqdm

from preprocessing import synthetic_noise
from spell_checker.hot_correct import SpellCheckers


def entailment(checker):
    data_file = 'entailment/snli_1.0/snli_1.0_train.jsonl'
    out_file = 'entailment/spell_checker/to_be_cleaned_snli_1.0_train.rtf'

    input = open(data_file, 'r')
    output = open(out_file, 'w+')

    for line in tqdm(input.readlines()):
        json_obj = json.loads(line)
        sent1 = checker.add_noise(json_obj['sentence1'])
        sent2 = checker.add_noise(json_obj['sentence2'])
        output.write('{} {}'.format(sent1, sent2))
        output.write('\n')

    input.close()
    output.close()

def sentiment(checker):
    data_file = 'sentiment/stanfordSentimentTreebank/train.txt'
    out_file = 'sentiment/spell_checker/to_be_cleaned_train.rtf'

    input = open(data_file, 'r')
    output = open(out_file, 'w+')

    header = input.readline()
    output.write(header)

    for line in tqdm(input.readlines()):
        sentence, label = line.strip().rsplit(',', 1)
        sentence = checker.add_noise(sentence)
        output.write('{}\n'.format(sentence))

    input.close()
    output.close()

if __name__ == '__main__':
    checker = SpellCheckers(mode = 'google', noise_level = 0.1)
    sentiment(checker)