"""This hot_correct.py file is for one-time noise generation and correction using given spellchecker.

Given a mode ('pyspellchecker', 'hunspell', 'google') and task ('entailment', 'sentiment', 'paraphrase'), this module
generates one instance of data with a certain level of noise injected and correct such data using the spell checker
specified. The corrected data is then saved under a folder 'spell_checker' under each task folder.

    Example of usage:
    python hot_correct.py --noise 0.1 --task entailment --mode hunspell
"""

import sys
sys.path.insert(1, '~/robust-embed2/')

from spellchecker import SpellChecker
from nltk import word_tokenize
from nltk.tokenize.treebank import TreebankWordDetokenizer
import json
from tqdm import tqdm
import time
import random

from preprocessing import synthetic_noise
from preprocess import normalize_string
from spell_checker.google_search import *
from spell_checker.api import *


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--noise', help = 'the level of noise to be evaluated on', type = float, default = 0)
    parser.add_argument('--task', help = 'the tasks to generate data for', choices = ['entailment', 'sentiment', 'paraphrase'], required = True)
    parser.add_argument('--mode', help = 'the mode of spell checker', choices = ['pyspellchecker', 'hunspell', 'google'], required = True)
    return parser.parse_args()

class SpellCheckers:
    def __init__(self, mode, noise_level):
        self.mode = mode
        self.noise_level = noise_level
        if self.mode == 'pyspellchecker':
            self.checker = self.pyspellchecker
        elif self.mode == 'google':
            self.google = GoogleSearch()
            self.checker = self.google_search
        elif self.mode == 'api':
            self.api_client = APIWorker()
            self.checker = self.api
        else:
            raise Exception('unknown checker mode')

    def process(self, sent):
        return self.checker(self.add_noise(sent))

    def add_noise(self, sent):
        words = synthetic_noise.process_sentence(normalize_string(sent).split(' '), self.noise_level)
        sent = TreebankWordDetokenizer().detokenize(words)
        return sent

    def pyspellchecker(self, sent):
        checker = SpellChecker()
        words = normalize_string(sent).split(' ')
        misspelled = checker.unknown(words)
        for word in misspelled:
            correction = checker.correction(word)
            words[words.index(word)] = correction
        correction = TreebankWordDetokenizer().detokenize(words)
        return correction

    def google_search(self, sent):
        if random.random() < 0.1:
            time.sleep(3)
        wait_time = random.randrange(25, 250) / 1000
        time.sleep(wait_time)
        return self.google.search(sent)

    def api(self, sent):
        wait_time = random.randrange(25, 250) / 1000
        time.sleep(wait_time)
        return self.api_client.request(sent)


def entailment(checker):
    data_file = 'entailment/snli_1.0/snli_1.0_test.jsonl'
    out_file = 'entailment/spell_checker/snli_1.0_test_{}_{}.jsonl'.format(checker.mode, checker.noise_level)

    input = open(data_file, 'r')
    output = open(out_file, 'w+')

    for line in tqdm(input.readlines()):
        json_obj = json.loads(line)
        json_obj['sentence1'] = checker.process(json_obj['sentence1'])
        json_obj['sentence2'] = checker.process(json_obj['sentence2'])
        output.write(json.dumps(json_obj))
        output.write('\n')

    input.close()
    output.close()

def sentiment(checker):
    data_file = 'sentiment/stanfordSentimentTreebank/train.txt'
    out_file = 'sentiment/spell_checker/train_{}_{}.txt'.format(checker.mode, checker.noise_level)

    input = open(data_file, 'r')
    output = open(out_file, 'w+')

    header = input.readline()
    output.write(header)

    for line in tqdm(input.readlines()):
        sentence, label = line.strip().rsplit(',', 1)
        sentence = checker.process(sentence)
        output.write('{},{}\n'.format(sentence,label))

    input.close()
    output.close()

    data_file = 'sentiment/stanfordSentimentTreebank/test.txt'
    out_file = 'sentiment/spell_checker/test_{}_{}.txt'.format(checker.mode, checker.noise_level)

    input = open(data_file, 'r')
    output = open(out_file, 'w+')

    header = input.readline()
    output.write(header)

    for line in tqdm(input.readlines()):
        sentence, label = line.strip().rsplit(',', 1)
        sentence = checker.process(sentence)
        output.write('{},{}\n'.format(sentence, label))

    input.close()
    output.close()

def main():
    args = parse_args()
    print('mode: {}  noise-level: {}'.format(args.mode, args.noise_level))
    checker = SpellCheckers(mode = args.mode, noise_level = args.noise_level)
    if args.task == 'entailment':
        entailment(checker)
    elif args.task == 'sentiment':
        sentiment(checker)

if __name__ == '__main__':
    main()
