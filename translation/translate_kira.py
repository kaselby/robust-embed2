import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
from torch.nn import TransformerEncoder, TransformerDecoder, LayerNorm, TransformerEncoderLayer, TransformerDecoderLayer

import numpy as np
from numpy import dot
from numpy.linalg import norm
import re
from string import punctuation
import math
import time

from nltk.translate.bleu_score import sentence_bleu, corpus_bleu, SmoothingFunction

from utils import *
from preprocess import *
from preprocessing import synthetic_noise
from beam_search import BeamIterator

BLEU_WEIGHTS = [
    [1,0,0,0],
    [0.5,0.5,0,0],
    [0.33,0.33,0.33,0],
    [0.25,0.25,0.25,0.25]
]

BEAM_WIDTH = 3
N_BEAMS = 3

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
MAX_LENGTH = 35

def preprocess_src(seq, preprocess_fct=normalize_string_en, noise_probability=0, sos=SOS, eos=EOS):
    out_seq = preprocess_fct(seq).split(" ")
    if noise_probability > 0:
        out_seq = synthetic_noise.process_sentence(out_seq, noise_probability)
    out_seq = [sos] + out_seq + [eos]
    return out_seq

def preprocess_tgt(seq, preprocess_fct=normalize_string_fr, sos=SOS, eos=EOS):
    out_seq = preprocess_fct(seq).split(" ")
    out_seq = [sos] + out_seq + [eos]
    return out_seq


def load_mtnt(filename, en_preprocess=normalize_string_en, fr_preprocess=normalize_string_fr, noise_probability = 0, max_pairs=-1):
    pairs = []
    infile = open(filename, 'r')
    lines = infile.readlines()
    N = len(lines) if max_pairs <= 0 else min(len(lines), max_pairs)
    for i in range(N):
        line = lines[i]
        _, src_line, tgt_line = line.split("\t")
        s1 = preprocess_src(src_line, preprocess_fct=en_preprocess, noise_probability=noise_probability)
        s2 = preprocess_tgt(tgt_line, preprocess_fct=fr_preprocess)
        pairs.append((s1, s2))
    return pairs


def load_WMT(filename, src_lang='en', tgt_lang='fr', src_preprocess=normalize_string_en,
             tgt_preprocess=normalize_string_fr, max_pairs=-1, noise_probability = 0):
    src_file = open(filename + "." + src_lang, 'r')
    tgt_file = open(filename + "." + tgt_lang, 'r')
    src_lines = src_file.readlines()
    tgt_lines = tgt_file.readlines()
    src_file.close()
    tgt_file.close()

    pairs = []
    N = min(len(src_lines), len(tgt_lines)) if max_pairs <= 0 else min(len(src_lines), len(tgt_lines), max_pairs)
    for i in range(N):
        '''
        src_clean = src_preprocess(src_lines[i]).split(" ")
        tgt_clean = tgt_preprocess(tgt_lines[i]).split(" ")
        # synthetic noise injection
        if noise_probability > 0:
            src_clean = synthetic_noise.process_sentence(src_clean, noise_probability)
            tgt_clean = synthetic_noise.process_sentence(tgt_clean, noise_probability)
        src_clean = [SOS] + src_clean + [EOS]
        tgt_clean = tgt_clean + [EOS]
        '''

        s1 = preprocess_src(src_lines[i], preprocess_fct=src_preprocess, noise_probability=noise_probability)
        s2 = preprocess_tgt(tgt_lines[i], preprocess_fct=tgt_preprocess)

        pairs.append((s1, s2))

    return pairs

def load_ted(filename, src_lang='en', tgt_lang='fr', src_preprocess=normalize_string_en,
             tgt_preprocess=normalize_string_fr, max_pairs=-1, noise_probability = 0):
    src_file = open(filename + "." + src_lang, 'r')
    tgt_file = open(filename + "." + tgt_lang, 'r')
    src_lines = src_file.readlines()
    tgt_lines = tgt_file.readlines()
    src_file.close()
    tgt_file.close()

    pairs = []
    N = min(len(src_lines), len(tgt_lines)) if max_pairs <= 0 else min(len(src_lines), len(tgt_lines), max_pairs)
    for i in range(N):
        s1 = src_lines[i][7:]
        s1 = re.sub(" &apos;", "'", s1)
        s1 = preprocess_src(s1, preprocess_fct=src_preprocess, noise_probability=noise_probability)
        s2 = re.sub("&apos; ", "'", tgt_lines[i])
        s2 = preprocess_tgt(s2, preprocess_fct=tgt_preprocess)
        pairs.append((s1, s2))
    return pairs

'''
def make_batch_tgt(seqs, vocab):
    bs = len(seqs)
    #sorted_seqs = sorted(seqs, key=len, reverse=True)
    max_length = max([len(s) for s in seqs])

    pad_index = vocab[PAD]
    unk_index = vocab[UNK]

    batch = pad_index*torch.ones(bs, max_length).long()
    lengths = torch.zeros(bs).long()

    for i in range(bs):
        words = seqs[i]
        l = len(words)
        #oov=0
        for j in range(l):
            word = words[j]
            #
            if word in vocab:
                index = vocab[word]
                batch[i,j] = index
            else:
                batch[i,j] = unk_index
    #mask = mask_lengths(lengths, max_l=max_length)
    if use_cuda:
        batch, lengths = batch.cuda(), lengths.cuda()#, mask.cuda()
    return batch, lengths
'''


def batch_seqs(pair_list, batch_size, batch_fct, src_voc, tgt_voc, *args, tgt_batch_fct=make_batch_w2v, max_len=MAX_LENGTH, **kwargs):
    def generate():
        N = len(pair_list)
        n_batches = math.ceil(N / batch_size)
        p = torch.randperm(N)

        for i in range(n_batches):
            min_j = i*batch_size
            max_j = min((i+1)*batch_size, N)
            seqs1, seqs2 = zip(*[pair_list[p[j]] for j in range(min_j, max_j)])
            src_batch = batch_fct(seqs1, src_voc, max_length=max_len, *args, **kwargs)
            tgt_batch = tgt_batch_fct(seqs2, tgt_voc, max_length=max_len)
            yield src_batch, tgt_batch
    return generate


class Translator(nn.Module):
    def __init__(self, src_emb, tgt_emb, hidden_size, dropout_p=0.1, max_length=MAX_LENGTH, attn=True):
        super().__init__()
        self.src_emb = src_emb
        self.tgt_emb = tgt_emb

        self.hidden_size = hidden_size
        self.max_length = max_length

        self.enc = nn.LSTM(src_emb.dim, hidden_size, num_layers=1, batch_first=True)
        self.dec = nn.LSTM(tgt_emb.dim, hidden_size, num_layers=1, batch_first=True)

        self.attn = attn
        self.concat = nn.Linear(hidden_size * 2, hidden_size) if attn else nn.Linear(hidden_size, hidden_size)

        #self.attn = nn.Linear(self.hidden_size * 2, self.max_length)
        #self.attn_combine = nn.Linear(self.hidden_size * 2, self.hidden_size)
        self.out = nn.Linear(self.hidden_size, tgt_emb.voc.voc_size)

        if use_cuda:
            self.enc = self.enc.cuda()
            self.dec = self.dec.cuda()
            self.concat = self.concat.cuda()
            self.out = self.out.cuda()

    def _encode(self, batch, lengths):
        self.enc.flatten_parameters()

        embedded = self.src_emb(batch)  # bs x l x d

        sorted_lengths, perm_idx = lengths.sort(0, descending=True)
        sorted_embed = embedded[perm_idx, :, :]

        packed = pack_padded_sequence(sorted_embed, sorted_lengths, batch_first=True)
        out, (h, c) = self.enc(packed)
        unpacked, _ = pad_packed_sequence(out, batch_first=True)

        _, unperm_idx = perm_idx.sort(0)
        unsorted_output = unpacked[unperm_idx, :]
        unsorted_h = h[:, unperm_idx, :]
        unsorted_c = c[:, unperm_idx, :]

        return unsorted_output, (unsorted_h, unsorted_c)

    def _attn(self, rnn_output, encoder_outputs, lengths=None):
        attn_energies = torch.bmm(rnn_output, encoder_outputs.transpose(1, 2)).squeeze(1)  # bs x l

        if lengths is not None:
            mask_lengths = torch.LongTensor(lengths)
            if use_cuda:
                mask_lengths = mask_lengths.cuda()
            mask = sequence_mask(mask_lengths)
            attn_weights = masked_softmax(attn_energies, mask, dim=1)       # bs x l
        else:
            attn_weights = F.softmax(attn_energies, 1)                      # bs x l

        context = (attn_weights.unsqueeze(1)).bmm(encoder_outputs)  # bs x 1 x hs

        return context

    def _decode_step(self, dec_input, hidden, encoder_outputs, lengths=None):
        embedded = self.tgt_emb(dec_input)                          # bs x 1 x d
                                                                    # bs x 1 x hs
        #attended_input = self._attn(embedded, h, enc_out)
        rnn_output, (h, c) = self.dec(embedded, hidden)             # output is bs x 1 x hs

        if self.attn:
            context = self._attn(rnn_output, encoder_outputs, lengths=lengths)              # bs x 1 x hs
            concat_input = torch.cat((rnn_output.squeeze(1), context.squeeze(1)), 1)
        else:
            concat_input = rnn_output.squeeze(1)
        concat_output = F.tanh(self.concat(concat_input))
        # Finally predict next token (Luong eq. 6, without softmax)
        output = self.out(concat_output)        # bs x V

        return output, (h,c)

    def _decode(self, enc_hidden, enc_out, tgt_batch=None, tf_ratio=0):
        self.dec.flatten_parameters()
        # Incomplete
        bs = enc_out.size(0)

        #dec_input = self.tgt_emb.voc.W[self.tgt_emb.voc.t2i[SOS]]
        dec_input = torch.ones(bs, 1, dtype = torch.long) * self.tgt_emb.voc.t2i[SOS]
        dec_hidden = enc_hidden
        max_length = self.max_length if tgt_batch is None else min(self.max_length, tgt_batch.size(1))
        dec_outputs = torch.zeros(bs, max_length, self.tgt_emb.voc.voc_size)
        if use_cuda:
            dec_input = dec_input.cuda()
            dec_outputs = dec_outputs.cuda()
        for i in range(max_length):
            dec_out_i, dec_hidden = self._decode_step(dec_input, dec_hidden, enc_out)
            dec_outputs[:, i, :] = dec_out_i

            r = torch.rand(1).item()
            if tgt_batch is not None and r < tf_ratio:
                dec_input = tgt_batch[:,i]
            else:
                _, top_i = dec_out_i.topk(1, dim=-1)
                dec_input = top_i[:,0].unsqueeze(1)

        return dec_outputs

    def forward(self, batch, lengths, tgt_batch=None):
        enc_out, enc_hidden = self._encode(batch, lengths)
        return self._decode(enc_hidden, enc_out, tgt_batch=tgt_batch)

    def evaluate(self, seq):
        with torch.no_grad():
            embedded = self.src_emb(seq)
            encoder_outputs, encoder_hidden = self.enc(embedded)

            iterator = BeamIterator(self, encoder_hidden, encoder_outputs, BEAM_WIDTH, N_BEAMS, self.max_length, self.tgt_emb.voc.t2i[SOS], self.tgt_emb.voc.t2i[EOS])
            decoded_words = iterator.search()
        return torch.Tensor(decoded_words).unsqueeze(0)


def create_model(src_emb, tgt_emb, hidden_size, lr, n_layers=6, **kwargs):
    #translator = Translator(src_emb, tgt_emb, hidden_size)
    translator = TransformerTranslator(src_emb, tgt_emb, hidden_size, n_layers, **kwargs)
    opt = optim.Adam(translator.parameters(), lr=lr)
    return translator, opt


class TransformerTranslator(nn.Module):
    def __init__(self, src_emb, tgt_emb, ff_dim, n_layers, attn_heads=5, dropout_p=0.1, max_length=MAX_LENGTH):
        super().__init__()
        self.src_emb = src_emb
        self.tgt_emb = tgt_emb

        #assert src_emb.dim == tgt_emb.dim
        self.model_dim = src_emb.dim

        self.ff_dim = ff_dim
        self.max_length = max_length

        #self.transformer = nn.Transformer(d_model=self.model_dim, nhead=attn_heads, dim_feedforward=ff_dim, num_encoder_layers=n_layers, num_decoder_layers=n_layers, dropout=dropout_p)

        encoder_layer = TransformerEncoderLayer(src_emb.dim, attn_heads, ff_dim, dropout_p)
        encoder_norm = LayerNorm(src_emb.dim)
        self.encoder = TransformerEncoder(encoder_layer, n_layers, encoder_norm)
        decoder_layer = TransformerDecoderLayer(tgt_emb.dim, attn_heads, ff_dim, dropout_p)
        decoder_norm = LayerNorm(tgt_emb.dim)
        self.decoder = TransformerDecoder(decoder_layer, n_layers, decoder_norm)

        self.positional_encodings = PositionalEncoding(self.model_dim, dropout=dropout_p, max_len=max_length)
        self.out = nn.Linear(self.model_dim, tgt_emb.voc.voc_size)

        if use_cuda:
            self.encoder = self.encoder.cuda()
            self.decoder = self.decoder.cuda()
            self.positional_encodings = self.positional_encodings.cuda()
            self.out = self.out.cuda()


    def forward(self, batch, lengths, tgt_batch, tgt_lengths):
        src_embedded = self.src_emb(batch)  # bs x l x d
        src_encoded = self.positional_encodings(src_embedded.transpose(0,1))
        tgt_embedded = self.tgt_emb(tgt_batch)  # bs x l x d
        tgt_encoded = self.positional_encodings(tgt_embedded.transpose(0,1))

        src_pad_mask = mask_lengths(lengths).logical_not()
        tgt_pad_mask = mask_lengths(tgt_lengths).logical_not()
        tgt_mask = generate_square_subsequent_mask(tgt_lengths.max().item())

        if use_cuda:
            src_pad_mask, tgt_pad_mask, tgt_mask = src_pad_mask.cuda(), tgt_pad_mask.cuda(), tgt_mask.cuda()

        transformer_input_src = src_encoded
        transformer_input_tgt = tgt_encoded

        encoder_outputs = self.encoder(transformer_input_src, src_key_padding_mask=src_pad_mask)
        decoder_outputs = self.decoder(transformer_input_tgt, encoder_outputs, tgt_mask=tgt_mask, tgt_key_padding_mask=tgt_pad_mask)

        #transformer_out = self.transformer(transformer_input_src, transformer_input_tgt, tgt_mask=tgt_mask,
                                           #src_key_padding_mask=src_pad_mask, tgt_key_padding_mask=tgt_pad_mask)
        predicted = self.out(decoder_outputs.transpose(0,1))

        return predicted


    def evaluate(self, batch, lengths):
        src_embedded = self.src_emb(batch)
        bs, l, d = src_embedded.size()
        src_encoded = self.positional_encodings(src_embedded.transpose(0,1))
        src_pad_mask = mask_lengths(lengths).logical_not()
        if use_cuda:
            src_pad_mask = src_pad_mask.cuda()

        encoder_input = src_encoded
        encoder_outputs = self.encoder(encoder_input, src_key_padding_mask=src_pad_mask)

        #max_length = self.max_length if tgt_batch is None else min(self.max_length, tgt_batch.size(1))

        decoder_outputs = torch.zeros(bs, self.max_length, self.tgt_emb.voc.voc_size)
        #generated_tokens = torch.zeros(max_length+1, bs, dtype=torch.long)
        #generated_tokens[0,:] = torch.ones(bs) * self.tgt_emb.voc.sos_index

        decoder_input = torch.ones(1,bs,dtype=torch.long) * self.tgt_emb.voc.sos_index
        if use_cuda:
            decoder_outputs = decoder_outputs.cuda()
            decoder_input = decoder_input.cuda()
            #generated_tokens = generated_tokens.cuda()

        for i in range(self.max_length):
            #decoder_input = generated_tokens[:i+1].clone()
            #tgt_pad_mask = (torch.arange(max_length) > i).unsqueeze(0).expand(bs, -1)
            tgt_mask = generate_square_subsequent_mask(i+1)

            if use_cuda:
                tgt_mask = tgt_mask.cuda()
                #tgt_pad_mask = tgt_pad_mask.cuda()

            tgt_embedded = self.tgt_emb(decoder_input)
            tgt_encoded = self.positional_encodings(tgt_embedded)
            decoder_output = self.decoder(tgt_encoded, encoder_outputs, tgt_mask=tgt_mask)#, tgt_key_padding_mask=tgt_pad_mask)
            token_output = decoder_output[i]
            logits = self.out(token_output)
            decoder_outputs[:,i,:] = logits

            predicted_token = logits.topk(1, dim=-1)[1][:,0]

            decoder_input = torch.cat([decoder_input, predicted_token.unsqueeze(0)], dim=0)
            #generated_tokens[i+1] = predicted_token

        return decoder_outputs



def generate_square_subsequent_mask(sz):
    r"""Generate a square mask for the sequence. The masked positions are filled with float('-inf').
        Unmasked positions are filled with float(0.0).
    """
    mask = (torch.triu(torch.ones(sz, sz)) == 1).transpose(0, 1)
    mask = mask.float().masked_fill(mask == 0, float('-inf')).masked_fill(mask == 1, float(0.0))
    return mask


def train_translate(model, optim, train_data, test_data, epochs, print_every=200, eval_every=1, clip=50, save_checkpoint=None, log_file="translation/logs/log.txt", best_bleu = [0,0,0,0], parallel=False, **kwargs):
    tgt_voc = model.tgt_emb.voc if not parallel else model.module.tgt_emb.voc

    best_val = float('inf')
    loss_lst = []
    for i in range(*epochs):
        print("\nEpoch:", i, "\n")
        log_file.write('Epoch:{}\n'.format(i))
        total = 0
        epoch_loss = 0
        epoch_start = time.time()
        #batches =  batch_seqs(pairs, targets, bs, batch_fct, *args, **kwargs)
        for ((src_batch, src_lengths),(tgt_batch, tgt_lengths)) in train_data():
            optim.zero_grad()
            bs = tgt_batch.size(0)
            #e1, e2 = embed(s1, **kwargs), embed(s2, **kwargs)  # bs x l x d
            outputs = model(src_batch, src_lengths, tgt_batch, tgt_lengths, **kwargs)
            loss = masked_cross_entropy(
                outputs.contiguous(),  # -> batch x seq
                tgt_batch[:,1:].contiguous(),  # -> batch x seq
                tgt_lengths,
                pad_index=tgt_voc.t2i[PAD]
            )
            loss.backward()

            nn.utils.clip_grad_norm_(model.parameters() if not parallel else model.module.parameters(), clip)
            optim.step()
            epoch_loss += loss.item()
            total += 1
            if total % print_every == 0:
                print("Batches Processed:", total, "\nBatch Loss:", loss.item(), "\n")
                log_file.write("  Batches Processed:{}    Batch Loss:{}\n".format(total, loss.item()))
                loss_lst.append(loss.item())

        epoch_end = time.time()
        log_str ='Average Training Loss: {}\nAverage Epoch Time: {:5f} minutes\n'.format(epoch_loss/total, (epoch_end-epoch_start)/60.0)
        print(log_str)
        log_file.write(log_str)
        if i % eval_every == 0:
            val_loss, bleu = eval_translate(model, test_data, parallel = parallel, **kwargs)
            #bleu = eval_translate_beam(model, test_data, **kwargs)
            print("Validation Loss:", val_loss)
            print("BLEU:", str(bleu), '\n')
            log_file.write('Validation Loss:{}\nBLEU Score:{}\n'.format(val_loss, str(bleu)))
            #log_file.write('BLEU Score:{}\n'.format(str(bleu)))

            if best_bleu[3] < bleu[3]:
                #best_val = val_loss
                best_bleu = bleu
                if save_checkpoint is not None:
                    torch.save({
                        'epoch': i+1,
                        'best_bleu': best_bleu,
                        'model_state_dict': model.state_dict() if not parallel else model.module.state_dict()
                    }, save_checkpoint)
                    print("Saved to checkpoint\n")
                    log_file.write('Saved to checkpoint\n')

        log_file.write('\n')
        log_file.flush()

    return best_bleu, loss_lst


def eval_translate(model, eval_data, print_every=-1, parallel=False, **kwargs):
    tgt_voc = model.tgt_emb.voc if not parallel else model.module.tgt_emb.voc
    total = 0
    total_loss = 0
    total_bleu = torch.FloatTensor([0,0,0,0])
    with torch.no_grad():
        #batches = batch_seqs(pairs, targets, bs, batch_fct, *args, **kwargs)
        for ((src_batch, src_lengths),(tgt_batch, tgt_lengths)) in eval_data():
            outputs = model.evaluate(src_batch, src_lengths, **kwargs)
            loss = masked_cross_entropy(
                outputs.contiguous(),  # -> batch x seq
                tgt_batch[:,1:].contiguous(),  # -> batch x seq
                tgt_lengths,
                pad_index=tgt_voc.t2i[PAD]
            )
            total += 1
            total_loss += loss.item()

            output_indices = outputs.argmax(dim=-1)
            predicted_seqs = decode_words(tgt_voc, output_indices)
            target_seqs = decode_words(tgt_voc, tgt_batch)

            bleu = eval_bleu(predicted_seqs, target_seqs)
            total_bleu += bleu

            if print_every > 0 and total % print_every == 0:
                print("Eval Batches Processed:", total, "\n")

    return total_loss / total, total_bleu / total


def eval_translate_beam(model, eval_pairs, parallel=False, all_weights=BLEU_WEIGHTS, **kwargs):
    all_hypotheses = []
    all_references = []

    for (src_seq, tgt_seq) in eval_pairs:
        if isinstance(model.src_emb, W2VEmbeddings):
            src_seq, src_len = make_batch_w2v([src_seq], model.src_emb.voc.t2i)
            tgt_seq, _ = make_batch_w2v([tgt_seq], model.tgt_emb.voc.t2i)
        else:
            src_seq, _ = make_batch([src_seq], model.src_emb.voc.t2i)
            tgt_seq, _ = make_batch([tgt_seq], model.tgt_emb.voc.t2i)
        out = model.evaluate(src_seq)
        all_hypotheses.append(decode_words(model.tgt_emb.voc, out)[0])
        #out2 = model(src_seq, src_len, **kwargs)
        #output_indices = out2.argmax(dim = -1)
        #all_hypotheses.append(output_indices)
        all_references.append(decode_words(model.tgt_emb.voc, tgt_seq))
        #predicted_seqs = decode_words(model.tgt_emb.voc, out)
        #predicted_seqs2 = decode_words(model.tgt_emb.voc, output_indices)
        #bleu = eval_bleu(predicted_seqs2, tgt_seq)

    scores = []
    for weights in all_weights:
        scores_i = corpus_bleu(all_references, all_hypotheses, weights=weights) * 100.0
        scores.append(scores_i)

    return torch.FloatTensor(scores)


def decode_words(vocab, outputs):
    #print(outputs.size())
    bs, l = outputs.size()
    seqs = []

    #indices = outputs.argmax(dim=-1)
    for i in range(bs):
        seq = []
        for j in range(l):
            word = vocab.i2t[outputs[i,j].item()]
            seq.append(word)
            if word == EOS:
                break
        seqs.append(seq)
    return seqs

def eval_bleu(predicted, true, all_weights=BLEU_WEIGHTS):
    all_hypotheses = []
    all_references = []

    smooth = SmoothingFunction(epsilon=1e-8).method1

    for i in range(len(predicted)):
        all_hypotheses.append(predicted[i])
        all_references.append([true[i]])

    scores = []
    for weights in all_weights:
        scores_i = corpus_bleu(all_references, all_hypotheses, weights=weights, smoothing_function = smooth) * 100.0
        scores.append(scores_i)

    return torch.FloatTensor(scores)


def test_w2v_translate(model, seq, target=None, all_weights=BLEU_WEIGHTS):
    batch, lengths = make_batch_w2v([preprocess_src(seq)], model.src_emb.voc)

    with torch.no_grad():
        output = model(batch, lengths)

    output_indices = output.argmax(dim=-1)
    prediction = decode_words(model.tgt_emb.voc, output_indices)

    if target is not None:
        scores = []
        for weights in all_weights:
            scores_i = sentence_bleu([target], prediction, weights=weights) * 100.0
            scores.append(scores_i)
        return prediction, scores
    else:
        return prediction

