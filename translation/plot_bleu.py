import matplotlib.pyplot as plt

def plot_from_log(log_file):
    file = open(log_file, 'r')
    scores = []
    cur_epoch = 0
    for line in file:
        if line[:5] == 'Epoch':
            cur_epoch = int(line.split(':')[-1])
        elif line[:4] == 'BLEU':
            score = float(line.split(',')[-1].strip()[:-2])
            try:
                scores[cur_epoch] = score
            except:
                scores.append(score)
        else:
            continue
    file.close()

    plt.plot(scores)
    plt.xlabel('epoch')
    plt.ylabel('bleu score')
    plt.savefig('logs/bleu_score_plot.png')

log_file = 'logs/log_robust_ted0_64_2048_0.0003.txt'
plot_from_log(log_file)
