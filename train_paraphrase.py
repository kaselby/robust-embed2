import argparse
import os

from bayes_lev_embed import RobustEmbeddings, VectorEmbeddings
from paraphrase.paraphrase_detection import *
from preprocess import *

#torch.backends.cudnn.deterministic=True

#w2v_file = "./data/reddit_10m_word_level_processed_3_word2vec_model.w2v"
#vocab_file = "./data/reddit_vocab_symspell.txt"
#cost_file = "data/dists_matrix.pt"
#neighbour_file = "data/neighbours_matrix.pt"
w2v_file = "./data/news_word2vec_model.w2v"
vocab_file = "./data/news_vocab_symspell_20000.txt"
cost_file = "data/news_dists_matrix.pt"
neighbour_file = "data/news_neighbours_matrix.pt"
msr_train = "paraphrase/MSRParaphraseCorpus/msr_paraphrase_train.txt"
msr_test = "paraphrase/MSRParaphraseCorpus/msr_paraphrase_test.txt"
twitter_train = "paraphrase/SemEval-PIT2015/train.data"
twitter_test = "paraphrase/SemEval-PIT2015/test.data"

save_folder = "./models/"


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--bs', help='batch size', type=int, default=32)
    parser.add_argument('--hs', help='hidden size', type=int, default=256)
    parser.add_argument('--n', help='n', type=int, default=20)
    parser.add_argument('--k', help='k', type=int, default=10)
    parser.add_argument('--tau', help='initial temperature', type=int, default=0.6)
    parser.add_argument('--lr', help='learning rate', type=float, default=0.0005)
    parser.add_argument('--epochs', help='num epochs', type=int, default=20)
    parser.add_argument('--noise', help='synthetic noise fraction', type=float, default=0)
    parser.add_argument('--data_type', choices=('twitter', 'msr'), type=str, default='msr')
    parser.add_argument('--model_type', choices=('w2v', 'robust'), type=str, default='w2v')
    parser.add_argument('--classifier', choices=('cosine', 'lstm'), type=str, default='cosine')
    parser.add_argument('--seed', help='random seed', type=int, default=None)
    parser.add_argument('--save', help='save file', type=str, default=None)
    parser.add_argument('--max_vocab', help='max vocab size', type=int, default=20000)

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    if args.seed is not None:
        torch.manual_seed(args.seed)
        np.random.seed(args.seed)

    w2v, ss = load_vocab(vocab_file, w2v_file)
    robust_emb = RobustEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab, tau=args.tau)
    w2v_emb = VectorEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab)
    emb = robust_emb if args.model_type == 'robust' else w2v_emb

    cost_matrix = torch.load(cost_file)
    nn_matrix = torch.load(neighbour_file)
    train_pairs, train_labels = [], []
    test_pairs, test_labels = [], []

    if args.data_type == 'twitter':
        train_pairs, train_labels = load_twitter(twitter_train, train=True, noise_probability=args.noise)
        test_pairs, test_labels = load_twitter(twitter_test, train=False, noise_probability=args.noise)
    elif args.data_type == 'msr':
        train_pairs, train_labels = load_msr(msr_train, noise_probability=args.noise)
        test_pairs, test_labels = load_msr(msr_test, noise_probability=args.noise)
    else:
        raise NotImplementedError

    (oov_pairs, oov_labels), (iv_pairs, iv_labels) = split_oov_pairs(test_pairs, test_labels, emb.voc.t2i)
    print("%d OOV test pairs, %d total test pairs" % (len(oov_pairs),len(test_pairs)))

    if args.classifier == 'cosine':
        model, optim = make_cosine_classifier(emb, args.lr)
    else:
        model, optim = make_lstm_classifier(emb.dim, args.hs, emb, args.lr)

    name = args.save if args.save is not None else "paraphrase"
    outfile = os.path.join(save_folder, name + ".pt")

    print("Training...")
    if args.model_type == 'w2v':
        train_batches = batch_seqs(train_pairs, train_labels, args.bs, make_batch_w2v, emb.voc.t2i)
        test_batches = batch_seqs(test_pairs, test_labels, args.bs, make_batch_w2v, emb.voc.t2i)
        best_val, best_metrics = train_paraphrase(model, optim, train_batches, test_batches, args.epochs, save_checkpoint=outfile)
    else:
        train_batches = batch_seqs(train_pairs, train_labels, args.bs, make_batch, emb.voc.t2i, ss, args.k, vocab_tensors=(nn_matrix, cost_matrix))
        test_batches = batch_seqs(test_pairs, test_labels, args.bs, make_batch, emb.voc.t2i, ss, args.k,
                               vocab_tensors=(nn_matrix, cost_matrix))
        best_val, best_metrics = train_paraphrase(model, optim, train_batches, test_batches, args.epochs, n=args.n, save_checkpoint=outfile)

    model.load_state_dict(torch.load(outfile))
    if args.model_type == 'w2v':
        oov_batches = batch_seqs(oov_pairs, oov_labels, args.bs, make_batch_w2v, emb.voc.t2i)
        iv_batches = batch_seqs(iv_pairs, iv_labels, args.bs, make_batch_w2v, emb.voc.t2i)
    else:
        oov_batches = batch_seqs(oov_pairs, oov_labels, args.bs, make_batch, emb.voc.t2i, ss, args.k,
                                 vocab_tensors=(nn_matrix, cost_matrix))
        iv_batches = batch_seqs(iv_pairs, iv_labels, args.bs, make_batch, emb.voc.t2i, ss, args.k,
                                 vocab_tensors=(nn_matrix, cost_matrix))
    oov_val, oov_metrics = eval_paraphrase(model, oov_batches)
    iv_val, iv_metrics = eval_paraphrase(model, iv_batches)

    acc, prec, rec, f1 = best_metrics
    oov_acc, oov_prec, oov_rec, oov_f1 = oov_metrics
    iv_acc, iv_prec, iv_rec, iv_f1 = iv_metrics
    print("Best Validation Loss:", best_val)
    print("Best Accuracy:", acc, "\tBest Precision:", prec, "\tBest Recall:", rec, "Best F1:", f1)
    print("OOV Validation Loss:", oov_val)
    print("OOV Accuracy:", oov_acc, "\tOOV Precision:", oov_prec, "\tOOV Recall:", oov_rec, "OOV F1:", oov_f1)
    print("IV Validation Loss:", iv_val)
    print("IV Accuracy:", iv_acc, "\tIV Precision:", iv_prec, "\tIV Recall:", iv_rec, "IV F1:", iv_f1)

    if args.save is not None:
        score_file = os.path.join(save_folder, name + ".scores.txt")
        f = open(score_file, 'w')
        f.write("Best Validation Loss:" + str(best_val) +"\n")
        f.write("Best Accuracy:" + str(acc) + "\tBest Precision:" + str(prec) + "\tBest Recall:" + str(rec) + "Best F1:" + str(f1))
        f.write("OOV Validation Loss:" + str(oov_val) +"\n")
        f.write("OOV Accuracy:" + str(oov_acc) + "\tOOV Precision:" + str(oov_prec) + "\tOOV Recall:" + str(
            oov_rec) + "OOV F1:" + str(oov_f1) +"\n")
        f.write("IV Validation Loss:" + str(iv_val))
        f.write("IV Accuracy:" + str(iv_acc) + "\tIV Precision:" + str(iv_prec) + "\tIV Recall:" + str(
            iv_rec) + "IV F1:" + str(iv_f1) + "\n")
        f.close()

    '''
    if args.save is not None:
        emb_file = os.path.join(save_folder, args.save, ".emb.pt")
        model_file = os.path.join(save_folder, args.save, ".model.pt")
        torch.save(emb, emb_file)
        torch.save(model, model_file)
    '''


