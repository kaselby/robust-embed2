import torch
import torch.nn as nn

import sys
import os
import json

use_cuda = torch.cuda.is_available()

def mask_lengths(lengths, max_l=None):
    n = lengths.size(0)
    l = lengths.max().item() if max_l is None else max_l
    mask = torch.arange(l).unsqueeze(1).expand(l,n)
    if use_cuda:
        mask = mask.cuda()
    return (mask < lengths).t()


def masked_softmax(logits, mask, dim=-1, tau=1, eps=1e-8):
    energies = logits/tau
    normed = energies - energies.max(dim=dim,keepdim=True)[0]
    exps = torch.exp(normed) * mask.float()
    sums = exps.sum(dim=dim, keepdim=True) + eps
    return exps/sums


def negative_mask(values):
    return 1 - (values < 0).long()


def get_class_scores(p,t):
    #import pdb;pdb.set_trace()
    true_positives = ((p==1) & (t==1)).float().sum()
    true_negatives = ((p==0) & (t==0)).float().sum()
    false_positives = ((p==1) & (t==0)).float().sum()
    false_negatives = ((p==0) & (t==1)).float().sum()
    print(true_positives, true_negatives, false_positives, false_negatives)
    accuracy = (true_positives + true_negatives) / t.size(0)
    precision = true_positives / (true_positives + false_positives) if \
        (true_positives + false_positives) > 0 else 0
    recall = true_positives / (true_positives + false_negatives) if \
        (true_positives + false_negatives) > 0 else 0
    f1_score = (2 * precision * recall) / (precision + recall) if (precision+recall) > 0 else 0

    return accuracy, precision, recall, f1_score

def compare_classes(p, t):
    true_positives = (p == 1) == (t == 1)
    true_negatives = (p == 0) == (t == 0)
    false_positives = (p == 1) == (t == 0)
    false_negatives = (p == 0) == (t == 1)
    return true_positives, true_negatives, false_positives, false_negatives


def cosine_dist(a, b, eps=1e-8):
    if len(b.size()) == 1:
        return a.matmul(b) / (a.norm(dim=-1) + eps) / (b.norm() + eps)
    elif len(b.size()) == 2:
        a_normed = a/(a.norm(dim=-1, keepdim=True)+eps)
        b_normed = b/(b.norm(dim=-1, keepdim=True)+eps)
        return (a_normed.unsqueeze(0).matmul(b_normed.t())).squeeze(0) if len(a.size()) == 1 else a_normed.matmul(b_normed.t())
    else:
        raise NotImplementedError

    '''
    elif len(b.size()) > 2:
        a_dims = len(a.size()) - 1
        b_dims = len(b.size()) - 1
        a_exp_dims = a.size()[:-1] + torch.Size([1] * b_dims) + torch.Size([1,]) + a.size()[-1:]
        b_exp_dims = torch.Size([1] * (a_dims)) + b.size()[:-1] + torch.Size([1,]) + b.size()[-1:]
        expanded_dims = a.size()[:-1] + b.size()[:-1]
        a_expanded = a.view(*a_exp_dims).expand(*expanded_dims, 1, a.size()[-1])
        b_expanded = b.view(*b_exp_dims).expand(*expanded_dims, 1, b.size()[-1])
        a_normed = a_expanded / a_expanded.norm(dim=-1, keepdim=True)
        b_normed = b_expanded / b_expanded.norm(dim=-1, keepdim=True)
        return a_normed.matmul(b_normed.transpose(-1,-2)).squeeze(-1).squeeze(-1)
    '''


def truncate_vocab_file(filename, max_vocab):
    infile_loc = os.path.join("data", filename + ".txt")
    outfile_loc = os.path.join("data", filename + "_%d.txt" % max_vocab)
    infile = open(infile_loc, 'r')
    lines = infile.readlines()
    infile.close()
    outfile = open(outfile_loc, 'w')
    for line in lines[:max_vocab]:
        outfile.write(line)
    outfile.close()

#
#   Gumbel Softmax
#   Implementation from https://gist.github.com/yzh119/fd2146d2aeb329d067568a493b20172f
#


import torch.nn.functional as F

def sample_gumbel(shape, eps=1e-20):
    U = torch.rand(shape)
    if use_cuda:
        U = U.cuda()
    return -torch.log(-torch.log(U + eps) + eps)

def gumbel_softmax_sample(logits, temperature):
    y = logits + sample_gumbel(logits.size())
    return F.softmax(y / temperature, dim=-1)

def gumbel_softmax(logits, temperature):
    """
    input: [*, n_class]
    return: [*, n_class] an one-hot vector
    """
    y = gumbel_softmax_sample(logits, temperature)
    shape = y.size()
    _, ind = y.max(dim=-1)
    y_hard = torch.zeros_like(y).view(-1, shape[-1])
    y_hard.scatter_(1, ind.view(-1, 1), 1)
    y_hard = y_hard.view(*shape)
    return (y_hard - y).detach() + y



def sequence_mask(sequence_length, max_len=None):
    if max_len is None:
        max_len = sequence_length.data.max()
    batch_size = sequence_length.size(0)
    seq_range = torch.arange(0, max_len).long()
    seq_range_expand = seq_range.unsqueeze(0).expand(batch_size, max_len)
    if sequence_length.is_cuda:
        seq_range_expand = seq_range_expand.cuda()
    seq_length_expand = (sequence_length.unsqueeze(1)
                         .expand_as(seq_range_expand))
    return seq_range_expand < seq_length_expand

def masked_cross_entropy(logits, target, length, pad_index=0):
    '''if use_cuda:
        length = (torch.LongTensor(length)).cuda()
    else:
        length = torch.LongTensor(length)'''
    """
    Args:
        logits: A Variable containing a FloatTensor of size
            (batch, max_len, num_classes) which contains the
            unnormalized probability for each class.
        target: A Variable containing a LongTensor of size
            (batch, max_len) which contains the index of the true
            class for each corresponding step.
        length: A Variable containing a LongTensor of size (batch,)
            which contains the length of each data in a batch.

    Returns:
        loss: An average loss value masked by the length.
    """
    max_length = logits.size(1)
    if target.size(1) > max_length:
        target = target[:,:max_length].contiguous()
    elif target.size(1) < max_length:
        target = torch.nn.functional.pad(target, (0,max_length-target.size(1)), value=pad_index)
    if use_cuda:
        length = torch.min(length, torch.LongTensor([max_length]).cuda())
    else:
        length = torch.min(length, torch.LongTensor([max_length]))

    # logits_flat: (batch * max_len, num_classes)
    logits_flat = logits.view(-1, logits.size(-1))
    # log_probs_flat: (batch * max_len, num_classes)
    log_probs_flat = F.log_softmax(logits_flat, dim=1)
    # target_flat: (batch * max_len, 1)
    target_flat = target.view(-1, 1)
    # losses_flat: (batch * max_len, 1)
    losses_flat = -torch.gather(log_probs_flat, dim=1, index = target_flat)
    # losses: (batch, max_len)
    losses = losses_flat.view(*target.size())
    # mask: (batch, max_len)
    mask = sequence_mask(sequence_length=length, max_len=max_length)
    losses = losses * mask.float()

    loss = losses.sum() / length.float().sum()
    return loss


import math
class PositionalEncoding(nn.Module):

    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x):
        x = x + self.pe[:x.size(0), :]
        return self.dropout(x)

import re
from string import punctuation
from unidecode import unidecode
def normalize_string_en(s):
    s = unidecode(s)
    s = s.lower().strip()
    s = re.sub('\!+', '!', s)
    s = re.sub('\,+', ',', s)
    s = re.sub('\?+', '?', s)
    s = re.sub('\.+', '.', s)
    #s = re.sub("[^0-9a-zA-Z.!?,'']+", ' ', s)
    for p in punctuation:
      if p != "'":
        s = s.replace(p, " " + p + " ")
    s = re.sub(' +', ' ', s)
    s = s.strip()
    return s
from preprocessing.synthetic_noise import process_sentence
import random
# general mtnt loader for printing purpose
def load_sentences(dataset='mtnt', preprocess=normalize_string_en, noise_probability = 0, num_sample = 5, context_len = 10, center_noise=False):
    sentences = []
    if dataset == 'snli':
        filename = 'entailment/snli_1.0/snli_1.0_dev.jsonl'
        infile = open(filename, 'r')
        next(infile)
        data = []
        for line in infile:
            line_obj = json.loads(line)
            data.append(line_obj['sentence1'])
            data.append(line_obj['sentence2'])
    else:
        filename = 'translation/MTNT/monolingual/dev.en'
        infile = open(filename, 'r')
        data = infile.readlines()
    infloop_preventer = 0
    while num_sample > len(sentences):
        sent_i = random.randrange(len(data))
        line = preprocess(data[sent_i]).split(' ')
        word_i = random.randrange(len(line))
        while (not any(c.isalpha() for c in line[word_i])) or (infloop_preventer > 100):
            word_i = random.randrange(len(line))
            infloop_preventer += 1
        noisy_line = process_sentence(line, noise_probability, noise_loc=word_i) if center_noise else process_sentence(line, noise_probability)
        line = line[max(word_i-context_len, 0):min(word_i+1+context_len, len(line))]
        noisy_line = noisy_line[max(word_i-context_len, 0):min(word_i+1+context_len, len(noisy_line))]
        if context_len < word_i:
            word_i = context_len
        sentences.append((word_i, line, noisy_line))
    return sentences


def banded_mask(d, width):
    ones = torch.ones(d,d)
    mask = torch.tril(ones, diagonal=width) - torch.tril(ones, diagonal=-width-1)
    return mask

def weighted_logsumexp(x, weights):
    # tensor is * x k, weights are * x k
    sizes = x.size()
    x_max = x.max(dim=-1, keepdim=True)[0]    # * x 1
    out = x_max.squeeze(-1) + torch.log(torch.exp((x - x_max).unsqueeze(-2)).matmul(weights.unsqueeze(-1))).view(sizes[:-1])
    return out


def make_perm_from_dict(input_i2t, output_t2i, unk_index):
    n = len(input_i2t.items())
    lst = [0] * n
    for i, t in input_i2t.items():
        if t in output_t2i:
            lst[i] = output_t2i[t]
        else:
            lst[i] = unk_index
    return torch.LongTensor(lst)