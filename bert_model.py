from transformers.modeling_bert import BertPredictionHeadTransform
from transformers import BertPreTrainedModel, BertModel
from torch.nn import CrossEntropyLoss
import torch.nn as nn
import torch

class BiasModule(nn.Module):
    def __init__(self, config):
        super().__init__()
        '''
        self.network = nn.Sequential(
            nn.Linear(config.hidden_size, 200),
            nn.Linear(200, 1)
        )
        '''
        self.network = nn.Linear(config.hidden_size, 1)

    def forward(self, vector):
        return self.network(vector).squeeze(-1)

class BertContinuousLMHead(nn.Module):
    def __init__(self, config):
        super(BertContinuousLMHead, self).__init__()
        self.transform = BertPredictionHeadTransform(config)

        # The output weights are the same as the input embeddings, but there is
        # an output-only bias for each token.
        self.decoder = nn.Linear(config.hidden_size,
                                 config.vocab_size,
                                 bias=False)

        #self.bias = BiasModule(config)
        #self.vocab_vectors = vocab_vectors
        #self.stored_biases = None

    def forward(self, hidden_states, update_biases=False):
        #if self.stored_biases is None or update_biases:
            #self.stored_biases = self.bias(self.vocab_vectors)
        hidden_states = self.transform(hidden_states)
        hidden_states = self.decoder(hidden_states) #+ self.bias(self.vocab_vectors)#self.stored_biases
        return hidden_states

class BertContinuousLMHeadOuter(nn.Module):
    def __init__(self, config):
        super(BertContinuousLMHeadOuter, self).__init__()
        self.predictions = BertContinuousLMHead(config)
    def forward(self, hidden_states, update_biases=False):
        return self.predictions(hidden_states, update_biases)

class BertForContinuousMaskedLM(BertPreTrainedModel):
    def __init__(self, config):
        super(BertForContinuousMaskedLM, self).__init__(config)
        self.bert = BertModel(config)
        self.cls = BertContinuousLMHeadOuter(config)

        self.init_weights()

    def get_output_embeddings(self):
        return self.cls.predictions.decoder

    def forward(self, input_ids=None, attention_mask=None, token_type_ids=None, position_ids=None, head_mask=None,
                inputs_embeds=None,
                masked_lm_labels=None, encoder_hidden_states=None, encoder_attention_mask=None, lm_labels=None, ):
        outputs = self.bert(input_ids,
                            attention_mask=attention_mask,
                            token_type_ids=token_type_ids,
                            position_ids=position_ids,
                            head_mask=head_mask,
                            inputs_embeds=inputs_embeds,
                            encoder_hidden_states=encoder_hidden_states,
                            encoder_attention_mask=encoder_attention_mask)

        sequence_output = outputs[0]
        prediction_scores = self.cls(sequence_output)

        outputs = (prediction_scores,) + outputs[2:]  # Add hidden states and attention if they are here

        # Although this may seem awkward, BertForMaskedLM supports two scenarios:
        # 1. If a tensor that contains the indices of masked labels is provided,
        #    the cross-entropy is the MLM cross-entropy that measures the likelihood
        #    of predictions for masked words.
        # 2. If `lm_labels` is provided we are in a causal scenario where we
        #    try to predict the next token for each input in the decoder.
        if masked_lm_labels is not None:
            loss_fct = CrossEntropyLoss()  # -1 index = padding token
            masked_lm_loss = loss_fct(prediction_scores.view(-1, self.config.vocab_size), masked_lm_labels.view(-1))
            outputs = (masked_lm_loss,) + outputs

        if lm_labels is not None:
            # we are doing next-token prediction; shift prediction scores and input ids by one
            prediction_scores = prediction_scores[:, :-1, :].contiguous()
            lm_labels = lm_labels[:, 1:].contiguous()
            loss_fct = CrossEntropyLoss(ignore_index=-1)
            ltr_lm_loss = loss_fct(prediction_scores.view(-1, self.config.vocab_size), lm_labels.view(-1))
            outputs = (ltr_lm_loss,) + outputs

        return outputs


class BertForDensityEstimation(BertPreTrainedModel):
    @classmethod
    def from_bfcmlm(cls, model):
        return cls(model.bert, model.cls.predictions.transform, model.config)

    @classmethod
    def from_dbfcmlm(cls, model):
        return cls(model.distilbert, model.vocab_transform, model.config)

    @classmethod
    def from_w2vbfmlm(cls, model):
        return cls(model.bert, model.cls.transform, model.config)

    def __init__(self, bert, transform, config):
        super(BertForDensityEstimation, self).__init__(config)
        self.bert = bert
        self.transform = transform

    def get_output_embeddings(self):
        return self.cls.predictions.decoder

    def forward(self, input_ids=None, **kwargs):
        outputs = self.bert(input_ids, **kwargs)

        sequence_output = outputs[0]
        return self.transform(sequence_output)


from transformers.modeling_bert import BertOnlyMLMHead, BertForMaskedLM, BertLayerNorm, ACT2FN
class W2VBertForMaskedLM(BertForMaskedLM):
    def __init__(self, config, vectors):
        super().__init__(config)

        self.vocab_size = vectors.size(0)
        self.hidden_size = vectors.size(1)
        self.cls = W2VBertLMPredictionHead(config, self.vocab_size, self.hidden_size)

        self.init_weights()

        with torch.no_grad():
            self.cls.decoder.weight.copy_(vectors)
        self.cls.decoder.weight.requires_grad_(False)

    def get_output_embeddings(self):
        return None

    def forward(
            self,
            input_ids=None,
            attention_mask=None,
            token_type_ids=None,
            position_ids=None,
            head_mask=None,
            inputs_embeds=None,
            masked_lm_labels=None,
            encoder_hidden_states=None,
            encoder_attention_mask=None,
            lm_labels=None,
    ):
        outputs = self.bert(
            input_ids,
            attention_mask=attention_mask,
            token_type_ids=token_type_ids,
            position_ids=position_ids,
            head_mask=head_mask,
            inputs_embeds=inputs_embeds,
            encoder_hidden_states=encoder_hidden_states,
            encoder_attention_mask=encoder_attention_mask,
        )

        sequence_output = outputs[0]
        prediction_scores = self.cls(sequence_output)

        outputs = (prediction_scores,) + outputs[2:]  # Add hidden states and attention if they are here

        # Although this may seem awkward, BertForMaskedLM supports two scenarios:
        # 1. If a tensor that contains the indices of masked labels is provided,
        #    the cross-entropy is the MLM cross-entropy that measures the likelihood
        #    of predictions for masked words.
        # 2. If `lm_labels` is provided we are in a causal scenario where we
        #    try to predict the next token for each input in the decoder.
        if masked_lm_labels is not None:
            loss_fct = CrossEntropyLoss()  # -100 index = padding token
            masked_lm_loss = loss_fct(prediction_scores.view(-1, self.vocab_size), masked_lm_labels.view(-1))
            outputs = (masked_lm_loss,) + outputs

        if lm_labels is not None:
            # we are doing next-token prediction; shift prediction scores and input ids by one
            prediction_scores = prediction_scores[:, :-1, :].contiguous()
            lm_labels = lm_labels[:, 1:].contiguous()
            loss_fct = CrossEntropyLoss()
            ltr_lm_loss = loss_fct(prediction_scores.view(-1, self.config.vocab_size), lm_labels.view(-1))
            outputs = (ltr_lm_loss,) + outputs

        return outputs


class W2VBertLMPredictionHead(nn.Module):
    def __init__(self, config, vocab_size, emb_size):
        super().__init__()
        self.transform = W2VBertPredictionHeadTransform(config, emb_size)

        # The output weights are the same as the input embeddings, but there is
        # an output-only bias for each token.
        self.decoder = nn.Linear(emb_size, vocab_size, bias=False)

        #self.bias = nn.Parameter(torch.zeros(config.vocab_size))

        # Need a link between the two variables so that the bias is correctly resized with `resize_token_embeddings`
        #self.decoder.bias = self.bias

    def forward(self, hidden_states):
        hidden_states = self.transform(hidden_states)
        hidden_states = self.decoder(hidden_states)
        return hidden_states

class W2VBertPredictionHeadTransform(nn.Module):
    def __init__(self, config, output_size):
        super().__init__()
        self.dense = nn.Linear(config.hidden_size, output_size)
        if isinstance(config.hidden_act, str):
            self.transform_act_fn = ACT2FN[config.hidden_act]
        else:
            self.transform_act_fn = config.hidden_act
        self.LayerNorm = BertLayerNorm(output_size, eps=config.layer_norm_eps)

    def forward(self, hidden_states):
        hidden_states = self.dense(hidden_states)
        hidden_states = self.transform_act_fn(hidden_states)
        hidden_states = self.LayerNorm(hidden_states)
        return hidden_states