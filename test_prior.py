import argparse
import os
from transformers import BertModel, BertTokenizer, BertForMaskedLM, DistilBertModel, DistilBertTokenizer, DistilBertForMaskedLM, BertConfig
from tqdm import tqdm
import torch
import numpy as np

from bayes_lev_embed import RobustEmbeddings, RobustEmbeddingsNaive, VectorEmbeddings
from entailment.entailment_detection import *
from preprocess import *
from bert_model import BertForDensityEstimation, BertForContinuousMaskedLM, W2VBertForMaskedLM
from preprocessing.synthetic_noise import process_sentence

from utils import load_sentences

#w2v_file = "./data/reddit_10m_word_level_processed_3_word2vec_model.w2v"
#vocab_file = "./data/reddit_vocab_symspell.txt"
base_w2v_file = "data/clean_news_word2vec_model.w2v"
base_vocab_file = "data/clean_news_vocab_symspell.txt"
bert_vocab_file = "data/bert_vocab_symspell.txt"
tinybert_vocab_file = "data/tinybert_vocab_symspell.txt"
tinybert_dir = "data/TinyBERT_4_312"
#w2v_file = "preprocessing/GoogleNews-vectors-negative300.bin"
#vocab_file = "preprocessing/google_w2v_vocab.txt"
#cost_file = "data/news_dists_matrix.pt"
#neighbour_file = "data/news_neighbours_matrix.pt"

google_w2v_file = "data/GoogleNews-vectors-negative300.bin.gz"
google_w2v_vocab = "data/google_vocab_symspell.txt"

w2v_bert_file='data/w2v-bert'

save_folder = "./models"


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--m', help='m', type=int, default=5)
    parser.add_argument('--n', help='n', type=int, default=20)
    parser.add_argument('--k', help='k', type=int, default=10)
    parser.add_argument('--sentence', help='sentence to debug', type=str, default='')
    parser.add_argument('--word', help='a wrong word in the input sentence', type=str, default='')
    parser.add_argument('--seed', help='the seed for random generation', type=int, default=-1)
    parser.add_argument('--model_type', choices=('w2v', 'robust', 'robust-naive', 'fasttext'), type=str, default='w2v')
    parser.add_argument('--likelihood', choices=('w2v', 'bert', 'w2v-bert'), type=str, default='w2v')
    parser.add_argument('--bert_model', choices=('base', 'tiny', 'distill'), type=str, default='base')
    parser.add_argument('--checkpoint_file', type=str, default=None)
    parser.add_argument('--noise', help = 'level of synthetic noise to inject', type=float, default=0)
    parser.add_argument('--max_vocab', help='max vocab size', type=int, default=-1)
    parser.add_argument('--N_samples', help='N', type=int, default=10)
    parser.add_argument('--print_all', help='whether to print correct predictions as well', action='store_true')
    parser.add_argument('--dataset', choices=('mtnt', 'snli'), type=str, default='snli')
    parser.add_argument('--variance_file', type=str, default=None)

    return parser.parse_args()

import tabulate
def summarize_seq(seq, pivot, emb, ss, m=5, n=20, k=5, **kwargs):
    with torch.no_grad():
        posterior_samples, (prior_knearest, likelihood_knearest, posterior_knearest), (
        (pr_pr, pr_l, pr_po), (l_pr, l_l, l_po), (po_pr, po_l, po_po)) = emb.analyze_seq(seq, ss, pivot, m=m, n=n, k=k,
                                                                                         only_oov=False, **kwargs)
    prior_headers = ["K-Nearest Prior Words", "Prior Probability", "Likelihood Probability", "Posterior Probability"]
    prior_candidates = emb.indices_to_words(prior_knearest)
    prior_table = [[' '.join(prior_candidates[i]), pr_pr[i].item(), pr_l[i].item(), pr_po[i].item()] for i in range(m)]

    likelihood_headers = ["Top-K Likelihood Words", "Prior Probability", "Likelihood Probability", "Posterior Probability"]
    likelihood_candidates = emb.indices_to_words(likelihood_knearest)
    likelihood_table = [[likelihood_candidates[i], l_pr[i].item(), l_l[i].item(), l_po[i].item()] for i in range(m)]

    posterior_headers = ["Top-K Posterior Words", "Prior Probability", "Likelihood Probability", "Posterior Probability"]
    posterior_candidates = emb.indices_to_words(posterior_knearest)
    posterior_table = [[posterior_candidates[i], po_pr[i].item(), po_l[i].item(), po_po[i].item()] for i in range(m)]

    print(tabulate.tabulate(prior_table, prior_headers, tablefmt="fancy_grid"))
    print(tabulate.tabulate(likelihood_table, likelihood_headers, tablefmt="fancy_grid"))
    print(tabulate.tabulate(posterior_table, posterior_headers, tablefmt="fancy_grid"))

    return posterior_knearest


if __name__ == '__main__':
    args = parse_args()

    Sigma = None
    tau = 0.6
    if args.variance_file is not None:
        saved_param = torch.load(args.variance_file)
        Sigma = saved_param['embed.Sigma'].abs()
        tau = float(saved_param['embed.tau'])

    if args.likelihood == "w2v-bert":
        vocab_file = google_w2v_vocab
        w2v_file = google_w2v_file
        kv = True
    else:
        kv = False
        w2v_file = base_w2v_file
        if args.likelihood == "bert":
            if args.bert_model == 'tiny':
                vocab_file = tinybert_vocab_file
            else:
                vocab_file = bert_vocab_file
        else:
            vocab_file = base_vocab_file

    w2v, ss = load_vocab(vocab_file, w2v_file, keyedvectors=kv)
    #cost_matrix = torch.load(cost_file)
    #nn_matrix = torch.load(neighbour_file)

    if args.likelihood == 'w2v':
        if args.model_type == 'w2v':
            emb = VectorEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab)
        elif args.model_type == 'fasttext':
            vocab_path = 'data/wiki.en.vec'
            from torchtext import vocab
            voc = vocab.Vectors(vocab_path, max_vectors = args.max_vocab)
            emb = VectorEmbeddings.from_fasttext(voc)
        elif args.model_type == 'robust-naive':
            emb = RobustEmbeddingsNaive.from_w2v(w2v, max_vocab=args.max_vocab, S=Sigma, tau=tau)
        else:
            emb = RobustEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab, S=Sigma, tau=tau)

    elif args.likelihood == 'bert':
        if args.bert_model == 'distill':
            bert_path = 'distilbert-base-uncased'
            bert_tokenizer = DistilBertTokenizer.from_pretrained(bert_path)
            if args.model_type == 'robust-naive':
                bert_model = DistilBertForMaskedLM.from_pretrained(bert_path)
                emb = RobustEmbeddingsNaive.from_bert(bert_model, bert_tokenizer, S=Sigma, tau=tau)
            else:
                bert_saved_model = DistilBertForMaskedLM.from_pretrained(bert_path)
                bert_model = BertForDensityEstimation.from_dbfcmlm(bert_saved_model)
                if args.model_type == 'w2v':
                    emb = VectorEmbeddings.from_bert(bert_model, bert_tokenizer)
                else:
                    emb = RobustEmbeddings.from_bert(bert_model, bert_tokenizer, S=Sigma, tau=tau)
        else:
            bert_path = tinybert_dir if args.bert_model == 'tiny' else 'bert-base-uncased'
            bert_tokenizer = BertTokenizer.from_pretrained(bert_path)
            if args.model_type == 'robust-naive':
                bert_model = BertForMaskedLM.from_pretrained(bert_path)
                emb = RobustEmbeddingsNaive.from_bert(bert_model, bert_tokenizer, S=Sigma, tau=tau)
            else:
                bert_saved_model = BertForContinuousMaskedLM.from_pretrained(bert_path)
                bert_model = BertForDensityEstimation.from_bfcmlm(bert_saved_model)
                if args.model_type == 'w2v':
                    emb = VectorEmbeddings.from_bert(bert_model, bert_tokenizer)
                else:
                    emb = RobustEmbeddings.from_bert(bert_model, bert_tokenizer, S=Sigma, tau=tau)

    elif args.likelihood == 'w2v-bert':
        print("3")
        w2v = KeyedVectors.load_word2vec_format(google_w2v_file, binary=True, limit = 50000)
        bert_tokenizer = BertTokenizer.from_pretrained(w2v_bert_file)
        bert_config = BertConfig.from_pretrained(w2v_bert_file)
        bert_saved_model = W2VBertForMaskedLM.from_pretrained(w2v_bert_file, torch.Tensor(w2v.wv.vectors), config=bert_config)
        bert_model = BertForDensityEstimation.from_w2vbfmlm(bert_saved_model)
        emb = RobustEmbeddings.from_w2v_bert(bert_model, bert_tokenizer, w2v, S=Sigma)

    seed = args.seed
    if seed == -1:
        seed = random.randrange(1000000)
    random.seed(seed)
    torch.manual_seed(seed)
    np.random.seed(seed)
    print('seed used is {}\n'.format(seed))

    k = args.k
    n = args.n
    m = args.m
    if len(args.sentence) > 0:
        seqs = args.sentence.split(' ')
        if len(args.word) > 0:
            seqs = [(seqs.index(args.word), seqs, process_sentence(seqs, args.noise))]
        else:
            seqs = [(random.randrange(len(seqs)), seqs, process_sentence(seqs, args.noise))]
    else:
        seqs = load_sentences(dataset = args.dataset, noise_probability = args.noise, num_sample = args.N_samples, context_len = 20, center_noise=False)
    print('processing {} sentences\n'.format(len(seqs)))

    base_examples=[]
    trivial_examples=[]
    base_error_probs=[]
    trivial_error_probs=[]
    base_count=0
    trivial_count=0
    for (i, seq, noisy_seq) in tqdm.tqdm(seqs):
        highlight_orig = list(seq)
        highlight_orig[i] = '{' + highlight_orig[i] + '}'
        print('\nOriginal Sentence: {}'.format(' '.join(highlight_orig)))
        highlight_noisy = list(noisy_seq)
        highlight_noisy[i] = '{' + highlight_noisy[i] + '}'
        print('   Noisy Sentence: {}'.format(' '.join(highlight_noisy)))
        print('\nBase Samples:\n')
        top1_posteriors = summarize_seq(noisy_seq, i, emb, ss, trivial=False)[:, 0]
        print('\nTrivial Samples:\n')
        top1_posteriors2 = summarize_seq(noisy_seq, i, emb, ss, trivial=True)[:, 0]

        base_wrong_posterior = False
        trivial_wrong_posterior = False
        base_errors = 0
        trivial_errors = 0
        for top_posterior in emb.indices_to_words(top1_posteriors):
            if top_posterior != seq[i]:
                base_wrong_posterior = True
                base_errors += 1
        for top_posterior in emb.indices_to_words(top1_posteriors2):
            if top_posterior != seq[i]:
                trivial_wrong_posterior = True
                trivial_errors += 1
        if base_wrong_posterior:
            base_count += 1
            base_error_probs.append(base_errors / len(top1_posteriors))
        if trivial_wrong_posterior:
            trivial_count += 1
            trivial_error_probs.append(trivial_errors / len(top1_posteriors2))

        if (base_wrong_posterior and not trivial_wrong_posterior):
            trivial_examples.append((i, seq, noisy_seq))
        elif (not base_wrong_posterior and trivial_wrong_posterior):
            base_examples.append((i, seq, noisy_seq))

    print('\nthe probability of wrong posterior for the base model is {}'.format(base_count / len(seqs)))
    print('\naverage incorrect posterior percentage for the base model is {}'.format(
        sum(base_error_probs) / len(base_error_probs) if len(base_error_probs) > 0 else 'N/A'))
    print('\nthe probability of wrong posterior for the trivial model is {}'.format(trivial_count / len(seqs)))
    print('\naverage incorrect posterior percentage for the trivial model is {}'.format(
        sum(trivial_error_probs) / len(trivial_error_probs) if len(trivial_error_probs) > 0 else 'N/A'))

    print('\nthere were {} examples where the base model performed better than the trivial model'.format(
        len(base_examples)))
    print('\nthere were {} examples where the trivial model performed better than the base model'.format(
        len(trivial_examples)))

    '''
    M = 5

    print('\nbase examples:')
    for(i, seq, noisy_seq) in base_examples[:M]:
        highlight_orig = list(seq)
        highlight_orig[i] = '{' + highlight_orig[i] + '}'
        print('\nOriginal Sentence: {}'.format(' '.join(highlight_orig)))
        highlight_noisy = list(noisy_seq)
        highlight_noisy[i] = '{' + highlight_noisy[i] + '}'
        print('   Noisy Sentence: {}'.format(' '.join(highlight_noisy)))

        top1_posteriors = summarize_seq(noisy_seq, i, emb, ss)[:, 0]

    print('\ntrivial examples:')
    for (i, seq, noisy_seq) in trivial_examples[:M]:
        highlight_orig = list(seq)
        highlight_orig[i] = '{' + highlight_orig[i] + '}'
        print('\nOriginal Sentence: {}'.format(' '.join(highlight_orig)))
        highlight_noisy = list(noisy_seq)
        highlight_noisy[i] = '{' + highlight_noisy[i] + '}'
        print('   Noisy Sentence: {}'.format(' '.join(highlight_noisy)))

        top1_posteriors = summarize_seq(noisy_seq, i, emb, ss)[:, 0]
    '''