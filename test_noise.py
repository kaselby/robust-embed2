from preprocess import *
from preprocessing.synthetic_noise import process_sentence, noise_generator
from utils import load_sentences

import tqdm

from entailment.entailment_detection import load_snli

snli_train = "entailment/snli_1.0/snli_1.0_train.jsonl"
snli_test = "entailment/snli_1.0/snli_1.0_test.jsonl"
google_w2v_vocab = "data/google_vocab_symspell.txt"

MAX_EDIT_DIST=2
NOISE_LEVELS = [0,0.02,0.05,0.1,0.2]

test_pairs, test_labels = load_snli(snli_test, noise_probability = 0, max_pairs=10000)
ss = SymSpell(MAX_EDIT_DIST)
ss.load_dictionary(google_w2v_vocab, 0, 1)
voc = ss._words.keys()

results={noise_level:{'overall':0, 'in-vocab':0} for noise_level in NOISE_LEVELS}

for noise_level in NOISE_LEVELS:
    total = 0
    total_iv = 0
    for pair in tqdm.tqdm(test_pairs):
        for seq in pair:
            for word in seq:
                if word in voc:
                    noisy_word = noise_generator(word, noise_level)
                    topk = ss.lookup(noisy_word, Verbosity.ALL)
                    top1 = topk[0].term if len(topk) > 0 else ""
                    if top1 == word:
                        results[noise_level]['overall'] += 1
                        results[noise_level]['in-vocab'] += 1
                    total_iv += 1
                total += 1
    results[noise_level]['overall'] /= total
    results[noise_level]['in-vocab'] /= total_iv
    print("Noise Level:", noise_level*100, "%\tTop 1 Accuracy (Overall):",
          str(results[noise_level]['overall']), "\tTop 1 Accuracy (In-Vocab):", str(results[noise_level]['in-vocab']))

print("Final Results:")
for noise_level, acc in results.items():
    print("Noise Level:", noise_level * 100, "%\tTop 1 Accuracy (Overall):",
          str(results[noise_level]['overall']), "\tTop 1 Accuracy (In-Vocab):", str(results[noise_level]['in-vocab']))
