import argparse
import os

from bayes_lev_embed import RobustEmbeddings, VectorEmbeddings
from paraphrase.paraphrase_detection import *
from preprocess import *

from transformers import BertModel, BertTokenizer

#torch.backends.cudnn.deterministic=True

bert_vocab_file = "data/bert_vocab.txt"
w2v_file = "data/clean_news_word2vec_model.w2v"
vocab_file = "data/clean_news_vocab_symspell.txt"
cost_file = "data/dists_matrix.pt"
neighbour_file = "data/neighbours_matrix.pt"
msr_train = "paraphrase/MSRParaphraseCorpus/msr_paraphrase_train.txt"
msr_test = "paraphrase/MSRParaphraseCorpus/msr_paraphrase_test.txt"
twitter_train = "paraphrase/SemEval-PIT2015/train.data"
twitter_test = "paraphrase/SemEval-PIT2015/test.data"

save_folder = "./models"

bs=32
k=10

bert_tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
bert_model = BertModel.from_pretrained('bert-base-uncased')
emb = RobustEmbeddings.from_bert(bert_model, bert_tokenizer)
w2v, ss = load_vocab(bert_vocab_file, w2v_file)

#w2v, ss = load_vocab(vocab_file, w2v_file)
#emb = VectorEmbeddings.from_w2v(w2v)
#emb = RobustEmbeddings.from_w2v(w2v)

model = CosineClassifier(emb)

test_pairs, test_labels = load_msr(msr_test)
#all_batches = batch_seqs(test_pairs, test_labels, bs, make_batch_w2v, emb.voc, exclude_oov=True)
all_batches = batch_seqs(test_pairs, test_labels, bs, make_batch, emb.voc, ss, k)

_, all_metrics = eval_paraphrase(model, all_batches)

print(all_metrics)