import argparse
from collections import Counter
import csv
import re
from sklearn.metrics import roc_auc_score
from sklearn.naive_bayes import BernoulliNB
from sentiment.sentiment_analysis import *
import numpy as np
import torch
from nltk.tokenize import word_tokenize
from transformers import BertModel, BertTokenizer, BertForMaskedLM, BertConfig
from bert_model import BertForContinuousMaskedLM, BertForDensityEstimation, W2VBertForMaskedLM
import tqdm

from preprocess import *
from bayes_lev_embed import *

# Read in the training data.
base_w2v_file = "data/clean_news_word2vec_model.w2v"
base_vocab_file = "data/clean_news_vocab_symspell.txt"
bert_vocab_file = "data/bert_vocab_symspell.txt"
#w2v_file = "preprocessing/GoogleNews-vectors-negative300.bin"
#vocab_file = "preprocessing/google_w2v_vocab.txt"
cost_file = "data/news_dists_matrix.pt"
neighbour_file = "data/news_neighbours_matrix.pt"
"""
sst_data = 'sentiment/stanfordSentimentTreebank/dictionary.txt'
sst_label = 'sentiment/stanfordSentimentTreebank/sentiment_labels.txt'
train_data = load_sst1(sst_data, sst_label)
"""
#sst_train = 'sentiment/sst/stsa.binary.phrases.train'
#sst_test = 'sentiment/sst/stsa.binary.test'
#sst_train = 'sentiment/stanfordSentimentTreebank/train.txt'
#sst_test = 'sentiment/stanfordSentimentTreebank/test.txt'
sst_train = 'sentiment/stanfordSentimentTreebank/sst_train_data.txt'
sst_test = 'sentiment/stanfordSentimentTreebank/sst_test_data.txt'

google_w2v_file = "data/GoogleNews-vectors-negative300.bin"
google_w2v_vocab = "data/google_vocab_symspell.txt"

w2v_bert_file='data/w2v-bert'


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--bs', help='batch size', type=int, default=32)
    parser.add_argument('--hs', help='hidden size', type=int, default=256)
    parser.add_argument('--k', help='k', type=int, default=10)
    parser.add_argument('--model_file', type=str, default=None)
    parser.add_argument('--data_type', choices=('sst'), type=str, default='sst')
    parser.add_argument('--model_type', choices = ('w2v', 'robust', 'robust-naive', 'fasttext'), type = str, default = 'w2v')
    parser.add_argument('--likelihood', choices = ('w2v', 'bert', 'w2v-bert'), type = str, default = 'w2v')
    parser.add_argument('--classifier', choices=('nb', 'lstm'), type=str, default='nb')
    parser.add_argument('--spellcheck', help='level of synthetic noise to inject', choices=('none', 'lev'), type=str,
                        default='none')
    parser.add_argument('--noise', help = 'level of synthetic noise to inject', type=float, default=0)
    parser.add_argument('--max_vocab', help='max vocab size', type=int, default=-1)
    parser.add_argument('--train_data', type=str, default=None)
    parser.add_argument('--test_data', type=str, default=None)

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    if args.likelihood == "w2v-bert" or args.model_type == 'w2v':
        vocab_file = google_w2v_vocab
        w2v_file = google_w2v_file
        kv = True
    else:
        kv = False
        w2v_file = base_w2v_file
        if args.likelihood == "bert":
            vocab_file = bert_vocab_file
        else:
            vocab_file = base_vocab_file
    w2v, ss = load_vocab(vocab_file, w2v_file, keyedvectors=kv)

    with torch.no_grad():

        cost_matrix = torch.load(cost_file)
        nn_matrix = torch.load(neighbour_file)

        if args.train_data is not None:
            sst_train = args.train_data
        if args.test_data is not None:
            sst_test = args.test_data
        if args.data_type == 'sst':
            train_data, train_target = load_sst(sst_train, args.noise)
            test_data, test_target = load_sst(sst_test, args.noise)
        else:
            raise NotImplementedError


        if args.likelihood == 'w2v':
            if args.model_type == 'w2v':
                emb = VectorEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab)
            elif args.model_type == 'fasttext':
                vocab_path = 'data/wiki.en.vec'
                from torchtext import vocab
                voc = vocab.Vectors(vocab_path, max_vectors = args.max_vocab if args.max_vocab > 0 else 30000)
                emb = VectorEmbeddings.from_fasttext(voc)
            elif args.model_type == 'robust-naive':
                emb = RobustEmbeddingsNaive.from_w2v(w2v, max_vocab=args.max_vocab)
            else:
                emb = RobustEmbeddings.from_w2v(w2v, max_vocab=args.max_vocab)

        elif args.likelihood == 'bert':
            bert_tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
            if args.model_type == 'w2v':
                bert_model = BertForMaskedLM.from_pretrained('bert-base-uncased')
                emb = VectorEmbeddings.from_bert(bert_model, bert_tokenizer)
            if args.model_type == 'robust-naive':
                bert_model = BertForMaskedLM.from_pretrained('bert-base-uncased')
                emb = RobustEmbeddingsNaive.from_bert(bert_model, bert_tokenizer)
            else:
                bert_saved_model = BertForMaskedLM.from_pretrained('bert-base-uncased')
                bert_model = BertForDensityEstimation.from_bfcmlm(bert_saved_model)
                if args.model_type == 'w2v':
                    emb = VectorEmbeddings.from_bert(bert_model, bert_tokenizer)
                else:
                    emb = RobustEmbeddings.from_bert(bert_model, bert_tokenizer)

        elif args.likelihood == 'w2v-bert':
            w2v = KeyedVectors.load_word2vec_format(google_w2v_file, binary=True, limit=50000)
            bert_tokenizer = BertTokenizer.from_pretrained(w2v_bert_file)
            bert_config = BertConfig.from_pretrained(w2v_bert_file)
            bert_saved_model = W2VBertForMaskedLM.from_pretrained(w2v_bert_file, torch.Tensor(w2v.wv.vectors),
                                                                  config=bert_config)
            bert_model = BertForDensityEstimation.from_w2vbfmlm(bert_saved_model)
            emb = RobustEmbeddings.from_w2v_bert(bert_model, bert_tokenizer, w2v)

        if args.spellcheck == 'lev':
            train_data = [naive_spellcheck(seq, ss, emb.voc.t2i) for seq in train_data]
            test_data = [naive_spellcheck(seq, ss, emb.voc.t2i) for seq in test_data]

        if args.model_type == 'robust' or args.model_type == 'robust-naive':
            train_batches = batch_seqs(train_data, train_target, args.bs, make_batch, emb.voc, ss, args.k)
            test_batches = batch_seqs(test_data, test_target, args.bs, make_batch, emb.voc, ss, args.k)
        else:
            train_batches = batch_seqs(train_data, train_target, args.bs, make_batch_w2v, emb.voc)
            test_batches = batch_seqs(test_data, test_target, args.bs, make_batch_w2v, emb.voc)



        train_vec = []
        train_gt = []
        print('training')
        for ((batch, lengths), targets) in tqdm.tqdm(train_batches()):
            e = emb(batch) * mask_lengths(lengths).unsqueeze(2)
            mean_vec = torch.sum(e, dim = 1) / lengths.unsqueeze(1)
            train_vec.extend(mean_vec.detach().cpu().numpy())
            train_gt.extend(targets.cpu().numpy())

        train_vec = np.array(train_vec)
        #train_vec = np.array(make_inputs3(train_data, emb.voc))  # N x l x d
        clf_linear = BernoulliNB()
        clf_linear.fit(train_vec, train_gt)

        test_vec = []
        test_gt = []
        print('testing')
        for ((batch, lengths), targets) in tqdm.tqdm(test_batches()):
            e = emb(batch) * mask_lengths(lengths).unsqueeze(2)
            mean_vec = torch.sum(e, dim = 1) / lengths.unsqueeze(1)
            test_vec.extend(mean_vec.detach().cpu().numpy())
            test_gt.extend(targets.cpu().numpy())
        #test_vec = np.array(make_inputs3(test_data, emb.voc))
        #predict_test = clf_linear.predict(test_vec)
        #print(list(zip(*[predict_test[:100], test_target[:100]])))
        test_vec = np.array(test_vec)
        pred = clf_linear.predict(test_vec)
        metrics = roc_auc_score(test_gt, pred)

        print("mean accuracy:{}".format(clf_linear.score(test_vec, test_gt)))
        print("AUC:", metrics)

